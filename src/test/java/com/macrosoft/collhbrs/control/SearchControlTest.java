package com.macrosoft.collhbrs.control;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.daos.CompanyDAO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.DTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SearchControlTest {

    private SearchControl control;

    @BeforeEach
    void setUp() {
        control = new SearchControl();
    }

    @Test
    void getCompanyVacancies() throws DatabaseException {
        CompanyDTO starkAG = new CompanyDAO().getOne(18);
        List<VacancyDTO> vacancyDTOList = control.getCompanyVacancies(starkAG);
        assertEquals(2, vacancyDTOList.size());
    }

    @Test
    void filterCompanies() throws DatabaseException {
        List<CompanyDTO> companies = control.filterCompanies("stark");
        assertEquals(1, companies.size());
    }

    @Test
    void filterVacancies() throws DatabaseException {
        List<VacancyDTO> vacancies = control.filterVacancies("datenschutz");
        assertEquals(1, vacancies.size());
    }

    @Test
    void liveFilterCombobox() throws DatabaseException {
        List<DTO> dtos = control.liveFilterCombobox("wall");
        assertEquals(1, dtos.size());
    }
}