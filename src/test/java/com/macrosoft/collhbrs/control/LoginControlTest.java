package com.macrosoft.collhbrs.control;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.InvalidCredentialsException;
import com.macrosoft.collhbrs.model.dtos.UserLoginDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LoginControlTest {

    @Test
    void checkAuthentication() {
        UserLoginDTO dto = new UserLoginDTO("", "");
        assertThrows(InvalidCredentialsException.class, () -> LoginControl.checkAuthentication(dto));
    }

    @Test
    void checkRole() throws DatabaseException {
        // this user is a student
        UserLoginDTO studentDto = new UserLoginDTO("malte", "abc123");
        // this user is a company
        UserLoginDTO companyDto = new UserLoginDTO("thewall", "abc123");
        assertEquals(LoginControl.checkRole(studentDto), Config.Role.STUDENT);
        assertEquals(LoginControl.checkRole(companyDto), Config.Role.COMPANY);
    }
}