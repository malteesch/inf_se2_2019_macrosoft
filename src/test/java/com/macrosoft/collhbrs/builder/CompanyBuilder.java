package com.macrosoft.collhbrs.builder;

import com.macrosoft.collhbrs.model.dtos.CompanyDTO;

public class CompanyBuilder extends AbstractUserBuilder {

    protected String name, description, website;

    public CompanyBuilder createDefaultCompany() {
       createDefaultUser();

        name = "Musterfirma GmbH";
        description = "Wir sind eine Test Firma";
        website = "musterfirma.de";
        return this;
    }

    @Override
    public CompanyDTO done() {
        CompanyDTO dto = new CompanyDTO();
        dto.setUsername(username);
        dto.setEmail(email);
        dto.setPassword(password);
        dto.setAddress(address);
        dto.setUserid(userid);
        dto.setImage(image);
        dto.setRegistrationdate(registrationdate);
        dto.setName(name);
        dto.setDescription(description);
        dto.setWebsite(website);
        return dto;
    }
}
