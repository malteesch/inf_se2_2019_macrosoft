package com.macrosoft.collhbrs.builder;

import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class VacancyBuilder {

    private String title, description;
    private CompanyDTO company;
    private int vacancyId;
    private LocalDate date;
    private List<String> tags, tasks, requirements, advantages;

    public VacancyBuilder createDefaultVacancy() {
        title = "Entwickler (w/m/d)";
        description = "Lorem ipsum";
        company = new CompanyDTO();
        List<String> tags = new ArrayList<>();
        tags.add("tag1");
        tags.add("tag2");
        tags.add("tag3");
        List<String> tasks = new ArrayList<>();
        tasks.add("task1");
        tasks.add("task2");
        tasks.add("task3");
        List<String> requirements = new ArrayList<>();
        requirements.add("requirement1");
        requirements.add("requirement2");
        requirements.add("requirement3");
        List<String> advantages = new ArrayList<>();
        advantages.add("advantage1");
        advantages.add("advantage2");
        advantages.add("advantage3");
        this.tags = tags;
        this.tasks = tasks;
        this.requirements = requirements;
        this.advantages = advantages;
        return this;
    }

    public VacancyBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public VacancyBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public VacancyBuilder withCompany(CompanyDTO company) {
        this.company = company;
        return this;
    }

    public VacancyBuilder withVacancyId(int vacancyId) {
        this.vacancyId = vacancyId;
        return this;
    }

    public VacancyBuilder withDate(LocalDate date) {
        this.date = date;
        return this;
    }

    public VacancyBuilder withTags(List<String> tags) {
        this.tags = tags;
        return this;
    }

    public VacancyBuilder withTasks(List<String> tasks) {
        this.tasks = tasks;
        return this;
    }

    public VacancyBuilder withRequirements(List<String> requirements) {
        this.requirements = requirements;
        return this;
    }

    public VacancyBuilder withAdvantages(List<String> advantages) {
        this.advantages = advantages;
        return this;
    }

    public VacancyDTO get() {
        return done();
    }

    public VacancyDTO done() {
        VacancyDTO dto = new VacancyDTO();
        dto.setTitle(title);
        dto.setDescription(description);
        dto.setCompany(company);
        dto.setTags(tags);
        dto.setTasks(tasks);
        dto.setRequirements(requirements);
        dto.setAdvantages(advantages);
        dto.setVacancyId(vacancyId);
        dto.setDate(date);
        return dto;
    }
}
