package com.macrosoft.collhbrs.builder;

import com.macrosoft.collhbrs.model.dtos.UserDTO;

public class UserBuilder extends AbstractUserBuilder {
    @Override
    public UserDTO done() {
        UserDTO dto = new UserDTO();
        dto.setUsername(username);
        dto.setEmail(email);
        dto.setRegistrationdate(registrationdate);
        dto.setAddress(address);
        dto.setImage(image);
        dto.setPassword(password);
        return dto;
    }
}
