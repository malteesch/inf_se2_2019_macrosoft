package com.macrosoft.collhbrs.builder;

import com.macrosoft.collhbrs.model.dtos.AddressDTO;

public class AddressBuilder {

    private String street, housenumber, postalcode, city, country;

    public AddressBuilder createDefaultAddress() {
        street = "Teststraße";
        housenumber = "1A";
        postalcode = "12345";
        city = "Teststadt";
        country = "Testland";
        return this;
    }

    public AddressBuilder withStreet(String street) {
        this.street = street;
        return this;
    }

    public AddressBuilder withHousenumber(String housenumber) {
        this.housenumber = housenumber;
        return this;
    }

    public AddressBuilder withPostalcode(String postalcode) {
        this.postalcode = postalcode;
        return this;
    }

    public AddressBuilder withCity(String city) {
        this.city = city;
        return this;
    }

    public AddressBuilder withCountry(String country) {
        this.country = country;
        return this;
    }

    public AddressBuilder withoutStreet() {
        return withStreet(null);
    }

    public AddressBuilder withoutHousenumber() {
        return withHousenumber(null);
    }

    public AddressBuilder withoutPostalcode() {
        return withPostalcode(null);
    }

    public AddressBuilder withoutCity() {
        return withCity(null);
    }

    public AddressBuilder withouCountry() {
        return withCountry(null);
    }

    public AddressDTO get() {
        return done();
    }

    public AddressDTO done() {
        AddressDTO dto = new AddressDTO();
        dto.setStreet(street);
        dto.setHousenumber(housenumber);
        dto.setPostalcode(postalcode);
        dto.setCity(city);
        dto.setCountry(country);
        return dto;
    }
}
