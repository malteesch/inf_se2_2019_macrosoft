package com.macrosoft.collhbrs.builder;

import com.macrosoft.collhbrs.model.dtos.AddressDTO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;

import java.time.LocalDate;

public abstract class AbstractUserBuilder {

    protected String username, email, password;
    protected int userid;
    protected byte[] image;
    protected AddressDTO address;
    protected LocalDate registrationdate;

    public AbstractUserBuilder createDefaultUser() {
        username = "MaxMustermann";
        email = "m.mustermann@test.de";
        password = "abc123";
        address = new AddressBuilder().createDefaultAddress().get();
        image = new byte[0];
        registrationdate = LocalDate.now();
        return this;
    }

    public AbstractUserBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public AbstractUserBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public AbstractUserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public AbstractUserBuilder withUserid(int userid) {
        this.userid = userid;
        return this;
    }

    public AbstractUserBuilder withImage(byte[] image) {
        this.image = image.clone();
        return this;
    }

    public AbstractUserBuilder withAddress(AddressDTO address) {
        this.address = address;
        return this;
    }

    public AbstractUserBuilder withRegistrationdate(LocalDate registrationdate) {
        this.registrationdate = LocalDate.from(registrationdate);
        return this;
    }

    public abstract UserDTO done();
}
