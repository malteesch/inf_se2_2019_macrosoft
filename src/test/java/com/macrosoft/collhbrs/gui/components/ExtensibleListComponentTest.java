package com.macrosoft.collhbrs.gui.components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExtensibleListComponentTest {

    @Test
    void getEntries() {
        ExtensibleListComponent component = new ExtensibleListComponent("Test");
        assertEquals(0, component.getEntries().size());
    }
}