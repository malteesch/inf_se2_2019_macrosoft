package com.macrosoft.collhbrs.gui.components;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreateVacancyComponentTest {

    private CreateVacancyComponent component;

    @BeforeEach
    void setUp() {
        component = new CreateVacancyComponent();
    }

    @Test
    void getTitleValue() {
        assertEquals("", component.getTitleValue());
    }

    @Test
    void getDescriptionValue() {
        assertEquals("", component.getDescriptionValue());

    }
}