package com.macrosoft.collhbrs.gui.components;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class NavbarComponentTest {

    @Test
    void test() throws DatabaseException {
        assertThrows(NullPointerException.class, () -> {
            NavbarComponent component = new NavbarComponent();
            component.addStyleName("test");

        });
    }
}