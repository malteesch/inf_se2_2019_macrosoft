package com.macrosoft.collhbrs.model.dtos;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EducationDTOTest {
    private LocalDate start = LocalDate.of(2000, 1, 1);
    private LocalDate finish = LocalDate.of(2001, 1, 1);
    private EducationDTO educationdto1 = new EducationDTO(start, finish, "degree", "institution", 1);

    @Test
    void test() {
        educationdto1.setDegree("degree1");
        assertEquals(educationdto1.getDegree(), "degree1");
        educationdto1.setInstitution("institution1");
        assertEquals(educationdto1.getInstitution(), "institution1");
        educationdto1.setEducationid(2);
        assertEquals(educationdto1.getEducationid(), 2);
        LocalDate startest = LocalDate.of(2000, 01, 02);
        educationdto1.setStart(startest);
        assertEquals(educationdto1.getStart(), startest);
        LocalDate finishtest = LocalDate.of(2001, 01, 02);
        educationdto1.setFinish(finishtest);
        assertEquals(educationdto1.getFinish(), finishtest);
    }
/*
       @Test
    void toStringTest() {
           assertEquals(educationdto1.toString(), Utils.convertDateToString(start) + " bis " + Utils.convertDateToString(finish) + "<br> Abschluss: degree" + "<br> Institution: institution");
       }
 */
}