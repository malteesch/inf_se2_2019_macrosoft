package com.macrosoft.collhbrs.model.dtos;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VacancyDTOTest {
    private VacancyDTO vacancydto1 = new VacancyDTO();

    @Test
    void test() {
        vacancydto1.setTitle("title");
        assertEquals(vacancydto1.getTitle(), "title");
        vacancydto1.setDescription("description");
        assertEquals(vacancydto1.getDescription(), "description");
        UserDTO userdto1 = new UserDTO("username", "mail", "password");
        CompanyDTO companytest = new CompanyDTO(userdto1);
        vacancydto1.setCompany(companytest);
        assertEquals(vacancydto1.getCompany(), companytest);
        ArrayList<String> tags = new ArrayList<>();
        vacancydto1.setTags(tags);
        assertEquals(vacancydto1.getTags(), tags);
        ArrayList<String> task = new ArrayList<>();
        vacancydto1.setTasks(task);
        assertEquals(vacancydto1.getTasks(), task);
        ArrayList<String> advertages = new ArrayList<>();
        vacancydto1.setAdvantages(advertages);
        assertEquals(vacancydto1.getAdvantages(), advertages);
        ArrayList<String> requirments = new ArrayList<>();
        vacancydto1.setRequirements(requirments);
        assertEquals(vacancydto1.getRequirements(), requirments);
        vacancydto1.setVacancyId(1);
        assertEquals(vacancydto1.getVacancyId(), 1);
    }

    /*
    @Test
    void setgetDate() {
        LocalDate date = new LocalDate(1,1,1);
        vacancydto1.setDate(date);
        assertEquals(vacancydto1.getDate(), "documentdescription1");
    }
*/
}