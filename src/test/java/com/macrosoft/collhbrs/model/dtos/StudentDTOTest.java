package com.macrosoft.collhbrs.model.dtos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StudentDTOTest {
    private StudentDTO studentdto1;

    @BeforeEach
    void setUp() {
        studentdto1 = new StudentDTO();
    }

    @Test
    void test() {
        studentdto1.setName("name1");
        assertEquals(studentdto1.getName(), "name1");
        studentdto1.setFamilyname("familyname1");
        assertEquals(studentdto1.getFamilyname(), "familyname1");
        studentdto1.setSpecalization("specalization1");
        assertEquals(studentdto1.getSpecalization(), "specalization1");
        studentdto1.setAnrede("Herr");
        assertEquals(studentdto1.getAnrede(), "Herr");
        studentdto1.setUniversitylocation("unversitylocation1");
        assertEquals(studentdto1.getUniversitylocation(), "unversitylocation1");
        studentdto1.setJob("job1");
        assertEquals(studentdto1.getJob(), "job1");
        studentdto1.setEmployer("employer1");
        assertEquals(studentdto1.getEmployer(), "employer1");
        studentdto1.setMajor("major1");
        assertEquals(studentdto1.getMajor(), "major1");
        studentdto1.setStudentid(2);
        assertEquals(studentdto1.getStudentid(), 2);
        studentdto1.setSemester(7);
        assertEquals(studentdto1.getSemester(), 7);
        LocalDate birthdate1 = LocalDate.of(2000, 1, 1);
        studentdto1.setBirthdate(birthdate1);
        assertEquals(studentdto1.getBirthdate(), birthdate1);
        ArrayList<WorkDTO> worktest = new ArrayList<>();
        studentdto1.setWork(worktest);
        assertEquals(studentdto1.getWork(), worktest);
        ArrayList<AwardDTO> awardtest = new ArrayList<>();
        studentdto1.setAward(awardtest);
        assertEquals(studentdto1.getAward(), awardtest);
        ArrayList<EducationDTO> educationtest = new ArrayList<>();
        studentdto1.setEducation(educationtest);
        assertEquals(studentdto1.getEducation(), educationtest);
        ArrayList<String> interesttest = new ArrayList<>();
        studentdto1.setInterests(interesttest);
        assertEquals(studentdto1.getInterests(), interesttest);
        ArrayList<String> skilltest = new ArrayList<>();
        studentdto1.setSkills(skilltest);
        assertEquals(studentdto1.getSkills(), skilltest);
        DocumentDTO documenttest = new DocumentDTO();
        studentdto1.setDocuments(documenttest);
        assertEquals(studentdto1.getDocuments(), documenttest);
    }
/*
    Woher die Adresse??
   @Test
    void testtoString() {
        assertEquals(studentdto1.toString(), "Name: name\" +\n" +
                "                \"\\n Nachname: nachname\" + \n" +
                "                \"\\n Major: major\" +\n" +
                "                \"\\n universitylocation: universitylocation\" +\n" +
                "                \"\\n job: job\" +\n" +
                "                \"\\n employer: employer\" +\n" +
                "                \"\\n specalization: specialization\" +\n" +
                "                \"\\n userid: userid\" +\n" +
                "                \"\\n studentid: studentid\" +\n" +
                "                \"\\n email: email\" +\n" +
                "                \"\\n Adresse: \" " + this.getAddress().toString(););
    }
*/
}