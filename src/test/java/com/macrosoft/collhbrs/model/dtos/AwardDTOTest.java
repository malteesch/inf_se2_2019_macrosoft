package com.macrosoft.collhbrs.model.dtos;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AwardDTOTest {
    private LocalDate date = LocalDate.of(2000, Calendar.MARCH, 1);
    private AwardDTO awarddto1 = new AwardDTO(date, "title", 1, 2);

    @Test
    void test() {
        awarddto1.setTitle("title1");
        assertEquals(awarddto1.getTitle(), "title1");
        LocalDate date1 = LocalDate.of(2000, Calendar.MARCH, 2);
        awarddto1.setDate(date1);
        assertEquals(awarddto1.getDate(), date1);
        awarddto1.setStudentid(3);
        assertEquals(awarddto1.getStudentid(), 3);
        awarddto1.setAwardid(4);
        assertEquals(awarddto1.getAwardid(), 4);
    }
}