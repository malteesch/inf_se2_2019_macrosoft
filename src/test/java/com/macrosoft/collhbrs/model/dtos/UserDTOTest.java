package com.macrosoft.collhbrs.model.dtos;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserDTOTest {
    private UserDTO userdto = new UserDTO();

    @Test
    void test() {
        userdto.setEmail("Email");
        assertEquals(userdto.getEmail(), "Email");
        userdto.setPasswordAlreadyHashed("abc123");
        assertEquals(userdto.getPassword(), "abc123");
        userdto.setUsername("Username");
        assertEquals(userdto.getUsername(), "Username");
        userdto.setAddressid(1);
        assertEquals(userdto.getAddressid(), 1);

        UserDTO userdto2 = new UserDTO("zwei", "zwei@gmail.com", "zwei22");
        UserDTO userdto3 = new UserDTO("zwei", "zwei@gmail.com", "zwei22");
        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.setCompanyId(1);
        companyDTO.setUsername("zwei");
        companyDTO.setEmail("zwei@gmail.com");


        assertEquals(userdto2, userdto3);
        //Check for equals symmetry
        assertEquals(userdto2, companyDTO);
        assertEquals(companyDTO, userdto2);
    }

    /*
    @Test
    void setgetAddress() {
        AddressDTO adressdto1 = new AddressDTO("street1","housenumber1","plz1","city1","country1");
        userdto.setAddress(adressdto1);
        assertEquals(userdto.getAddress(), adressdto1);
    }
    */
}