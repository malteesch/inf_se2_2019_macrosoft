package com.macrosoft.collhbrs.model.dtos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class DocumentDTOTest {
    private DocumentDTO documentdto1;

    @BeforeEach
    void setUp() {
        documentdto1 = new DocumentDTO(1, 2, true, "documenttitle", "documentdescription", LocalDate.of(2000, 1, 1), new byte[100]);
    }

    @Test
    void test() {
        documentdto1.setUserid(2);
        assertEquals(documentdto1.getUserid(), 2);
        documentdto1.setVisibility(false);
        assertFalse(documentdto1.getVisibility());
        documentdto1.setDocumenttitle("documenttitle1");
        assertEquals(documentdto1.getDocumenttitle(), "documenttitle1");
        documentdto1.setDocumentdescription("documentdescription1");
        assertEquals(documentdto1.getDocumentdescription(), "documentdescription1");
        documentdto1.setDocumentid(2);
        assertEquals(documentdto1.getDocumentid(), 2);
        LocalDate updatetest = LocalDate.of(2001, 1, 1);
        documentdto1.setDocumentupdate(updatetest);
        assertEquals(documentdto1.getDocumentupdate(), updatetest);
        byte[] file1 = {1, 2, 3, 4};
        documentdto1.setFile(file1);
        assertEquals(documentdto1.getFile().length, file1.length);
    }
}