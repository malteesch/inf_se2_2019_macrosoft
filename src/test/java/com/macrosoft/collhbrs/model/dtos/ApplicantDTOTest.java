package com.macrosoft.collhbrs.model.dtos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ApplicantDTOTest {
    private LocalDate datetest;
    private ApplicantDTO applicantdto1;

    @BeforeEach
    void setUp() {
        applicantdto1 = new ApplicantDTO("studentName", "studentFamilyname", "vacancyName", 1, LocalDate.of(2019, 1, 1));
        datetest = LocalDate.of(2019, 3, 5);
    }

    @Test
    void test() {
        applicantdto1.setStudentName("studentName");
        assertEquals(applicantdto1.getStudentName(), "studentName");
        applicantdto1.setStudentFamilyname("studentFamilyname");
        assertEquals(applicantdto1.getStudentFamilyname(), "studentFamilyname");
        applicantdto1.setVacancyTitle("vacancyName");
        assertEquals(applicantdto1.getVacancyTitle(), "vacancyName");
        applicantdto1.setStudentId(2);
        assertEquals(applicantdto1.getStudentId(), 2);
        applicantdto1.setApplicationDate(datetest);
        assertEquals(applicantdto1.getApplicationDate(), datetest);
        assertEquals(applicantdto1.toString(), "Studentname: studentNamestudentFamilyname, Vacancytitle: vacancyName , ApplicationDate: " + applicantdto1.getApplicationDate());
    }
}