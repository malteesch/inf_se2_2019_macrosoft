package com.macrosoft.collhbrs.model.dtos;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WorkDTOTest {
    private LocalDate start = LocalDate.of(2000, 1, 1);
    private LocalDate finish = LocalDate.of(2000, 1, 1);
    private WorkDTO workdto1 = new WorkDTO("username", "email", "password", start, finish,
            "description", "company", 1, 2);

    @Test
    void test() {
        workdto1.setStudentid(22);
        assertEquals(workdto1.getStudentid(), 22);
        workdto1.setWorkid(11);
        assertEquals(workdto1.getWorkid(), 11);
        workdto1.setCompany("company1");
        assertEquals(workdto1.getCompany(), "company1");
        LocalDate startest = LocalDate.of(2000, 1, 1);
        workdto1.setStart(startest);
        assertEquals(workdto1.getStart(), startest);
        LocalDate finishtest = LocalDate.of(2000, 1, 1);
        workdto1.setFinish(finishtest);
        assertEquals(workdto1.getFinish(), finishtest);
        workdto1.setDescription("description1");
        assertEquals(workdto1.getDescription(), "description1");
    }
}