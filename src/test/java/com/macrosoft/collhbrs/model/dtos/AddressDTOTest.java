package com.macrosoft.collhbrs.model.dtos;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddressDTOTest {
    private AddressDTO a1 = new AddressDTO();

    @Test
    void test() {
        a1.setStreet("Musterstrasse");
        assertEquals(a1.getStreet(), "Musterstrasse");
        a1.setHousenumber("123");
        assertEquals(a1.getHousenumber(), "123");
        a1.setPostalcode("53225");
        assertEquals(a1.getPostalcode(), "53225");
        a1.setCity("Musterstadt");
        assertEquals(a1.getCity(), "Musterstadt");
        a1.setCountry("Deutschland");
        assertEquals(a1.getCountry(), "Deutschland");
    }
}