package com.macrosoft.collhbrs.model.dtos;

import com.macrosoft.collhbrs.model.daos.CompanyDAO;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.daos.VacancyDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertThrows;

class Stepstone2CandidatureDTOTest {

    private CandidatureDTO dto;

    @BeforeEach
    void setUp() {

        try {
            dto = new Stepstone2DTOFactory().createCandidatureDTO();
            dto.setStudent(new StudentDAO().getOne("franzi"));
            dto.setCanditaturedate(new Date(2019, Calendar.OCTOBER, 12));
            dto.setCompany(new CompanyDAO().getOne(32));
            dto.setVacancy(new VacancyDAO().getOne(55));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void toString1() {
        System.out.println(dto.toString());
    }

    @Test
    void cloneException() {
        assertThrows(CloneNotSupportedException.class, () -> {
            Stepstone2CandidatureDTO s2dto = new Stepstone2DTOFactory().createCandidatureDTO();
            s2dto.clone();
        });
    }
}