package com.macrosoft.collhbrs.model.dtos;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserLoginDTOTest {

    private UserLoginDTO userlogindto1 = new UserLoginDTO("login", "password");

    @Test
    void test() {
        assertEquals(userlogindto1.getLogin(), "login");
        assertEquals(userlogindto1.getPassword(), "password");
    }
}
