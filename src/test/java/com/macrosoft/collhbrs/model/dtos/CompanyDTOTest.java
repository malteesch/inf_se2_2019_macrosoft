package com.macrosoft.collhbrs.model.dtos;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CompanyDTOTest {
    private CompanyDTO companydto1 = new CompanyDTO("name", "description", "website", 2);

    @Test
    void test() {
        companydto1.setName("name");
        assertEquals(companydto1.getName(), "name");
        companydto1.setDescription("description");
        assertEquals(companydto1.getDescription(), "description");
        companydto1.setWebsite("website");
        assertEquals(companydto1.getWebsite(), "website");
        companydto1.setCompanyId(2);
        assertEquals(companydto1.getCompanyId(), 2);
        assertEquals(companydto1.toString(), "2: name, description");
    }
}