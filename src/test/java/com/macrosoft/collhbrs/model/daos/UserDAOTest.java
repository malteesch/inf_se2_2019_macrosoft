package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.builder.AddressBuilder;
import com.macrosoft.collhbrs.builder.UserBuilder;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.model.dtos.UserDTO;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserDAOTest {

    private static UserDTO newUser;
    private UserDTO testUser, updatedUser;
    private static UserDAO dao;
    private static int testUserId;

    static int getTestUserId() {
        return testUserId;
    }

    static void setTestUserId(int testUserId) {
        UserDAOTest.testUserId = testUserId;
    }

    @BeforeAll
    static void beforeAll() throws DatabaseException {
        dao = new UserDAO();
        newUser = new UserBuilder().createDefaultUser().done();
    }

    @BeforeEach
    void setUp() {
        testUser = null;
        updatedUser = null;
    }

    @Test
    @Order(1)
    void getAll() throws DatabaseException {
        List<UserDTO> dtos = dao.getAll();
        assertTrue(dtos.size() > 0);
    }

    @Test
    @Order(2)
    void createOne() throws DatabaseException {
        assertNull(testUser);
        testUser = dao.createOne(newUser);
        assertNotEquals(0, testUser.getUserid());
        setTestUserId(testUser.getUserid());
        testDTO(testUser);
    }

    @Test
    @Order(3)
    void getOne() throws DatabaseException, UserException {
        assertNull(testUser);
        testUser = dao.getOne(getTestUserId());
        testDTO(testUser);
    }

    @Test
    @Order(4)
    void updateOne() throws DatabaseException, UserException {
        assertNull(testUser);
        testUser = dao.getOne(getTestUserId());
        testUser.setUsername("username");
        testUser.setEmail("email@email.de");
        updatedUser = dao.updateOne(testUser);
        assertEquals(testUserId, updatedUser.getUserid());
        assertEquals("username", updatedUser.getUsername());
        assertEquals("email@email.de", updatedUser.getEmail());
    }

    @Test
    @Order(5)
    void deleteOne() throws DatabaseException, UserException {
        assertNull(testUser);
        testUser = dao.getOne(getTestUserId());
        dao.deleteOne(testUser);
        assertThrows(UserException.class, () -> dao.getOne(getTestUserId()));
    }

    private void testDTO(UserDTO dto) {
        assertNotEquals(0, dto.getUserid());
        assertEquals("MaxMustermann", dto.getUsername());
        assertEquals("m.mustermann@test.de", dto.getEmail());
        assertEquals(new AddressBuilder().createDefaultAddress().get(), dto.getAddress());
    }
}