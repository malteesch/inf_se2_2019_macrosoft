package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.builder.AddressBuilder;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.AddressDTO;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AddressDAOTest {

    private static AddressDTO newAddress;
    private AddressDTO testAddress, updatedAddress;
    private static AddressDAO dao;
    private static int testAddressId;

    public static int getTestAddressId() {
        return testAddressId;
    }

    public static void setTestAddressId(int testAddressId) {
        AddressDAOTest.testAddressId = testAddressId;
    }

    @BeforeAll
    static void beforeAll() throws DatabaseException {
        dao = new AddressDAO();
        newAddress = new AddressBuilder()
                .createDefaultAddress()
                .withStreet("Straße")
                .withHousenumber("1")
                .withPostalcode("12345")
                .withCity("Stadt")
                .withCountry("Land")
                .get();
    }

    @BeforeEach
    void setUp() {
        testAddress = null;
        updatedAddress = null;
    }

    @Test
    @Order(1)
    void getAll() throws DatabaseException {
        List<AddressDTO> dtos = dao.getAll();
        assertTrue(dtos.size() > 0);
    }

    @Test
    @Order(2)
    void createOne() throws DatabaseException {
        assertNull(testAddress);
        testAddress = dao.createOne(newAddress);
        assertNotEquals(0, testAddress.getAddressid());
        setTestAddressId(testAddress.getAddressid());
        testDTO(testAddress);
    }

    @Test
    @Order(3)
    void getOne() throws DatabaseException {
        assertNull(testAddress);
        testAddress = dao.getOne(getTestAddressId());
        testDTO(testAddress);
    }

    @Test
    @Order(4)
    void updateOne() throws DatabaseException {
        assertNull(testAddress);
        testAddress = dao.getOne(getTestAddressId());
        testAddress.setStreet("Andere Straße");
        testAddress.setHousenumber("2B");
        testAddress.setPostalcode("54321");
        updatedAddress = dao.updateOne(testAddress);
        assertEquals("Andere Straße", updatedAddress.getStreet());
        assertEquals("2B", updatedAddress.getHousenumber());
        assertEquals("54321", updatedAddress.getPostalcode());
    }

    @Test
    @Order(5)
    void deleteOne() throws DatabaseException {
        assertNull(testAddress);
        testAddress = dao.getOne(getTestAddressId());
        dao.deleteOne(testAddress);
        assertThrows(DatabaseException.class, () -> dao.getOne(getTestAddressId()));
    }

    private void testDTO(AddressDTO dto) {
        assertNotEquals(0, dto.getAddressid());
        assertEquals("Straße", dto.getStreet());
        assertEquals("1", dto.getHousenumber());
        assertEquals("12345", dto.getPostalcode());
        assertEquals("Stadt", dto.getCity());
        assertEquals("Land", dto.getCountry());
    }
}