package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.AwardDTO;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AwardDAOTest {

    private static LocalDate date = LocalDate.of(2000, 1, 1);
    private static AwardDTO dto1;
    private static AwardDAO awardDAO;


    @BeforeAll
    static void beforeAll() {
        try {
            awardDAO = new AwardDAO();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        dto1 = new AwardDTO(date, "title", 1, 1);
    }

    @Test
    @Order(1)
    void getAllTest() throws Exception{
        List<AwardDTO> dtos = new ArrayList<>();

        try {
            dtos = awardDAO.getAll();
        } catch (DatabaseException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue(dtos.size() > 0);
    }

    @Test
    @Order(2)
    void createDTOTest() {

    }
}