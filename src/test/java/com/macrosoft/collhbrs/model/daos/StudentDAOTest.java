package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StudentDAOTest {

    private StudentDTO testStudent;
    private static StudentDAO dao;
    private static int teststudentid = 1;


    public static int getTeststudentid() {
        return teststudentid;
    }

    public static void setTestStudentId(int teststudentid) {
        StudentDAOTest.teststudentid = teststudentid;
    }


    @BeforeAll
    static void beforeAll() {
        try {
            dao = new StudentDAO();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        StudentDTO newStudent = new StudentDTO();
        try {
            newStudent.setAnrede("Herr");
            newStudent.setUsername("einAnderer122");
            newStudent.setEmail("wenjuckt122@esschon.de");
            newStudent.setName("Gerd");
            newStudent.setBirthdate(LocalDate.of(2000, 1, 1));
            newStudent.setEmployer("Emp");
            newStudent.setSpecalization("BIS");
            newStudent.setFamilyname("Müller");
            newStudent.setUniversitylocation("Hennef");
            newStudent.setJob("Java Entwickler");
            newStudent.setMajor("Major");
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @BeforeEach
    void setUp() {
        testStudent = null;
    }

    @Test
    @Order(1)
    void getAll() {
        List<StudentDTO> dtos = new ArrayList<>();
        System.out.println(dtos.size());

        try {
            dtos = new StudentDAO().getAll();
        } catch (DatabaseException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue(dtos.size() > 0);
    }
/*
    @Test
    @Order(2)
    void createOne() {
        try {
            assertNull(testStudent);
            testStudent = dao.createOne(newStudent);
            assertNotEquals(0, testStudent.getStudentid());
            teststudentid = 134;
            assertEquals(testStudent.getName(), "Gerd");
            assertEquals(testStudent.getAnrede(), "Herr");
            /*
            assertEquals(testStudent.getWork(), new WorkDTO("username", "email", "password", LocalDate.of(2000, 1, 1),
                    LocalDate.of(2001, 1, 1), "Beschreibung", "company", 1, 2));
            assertEquals(testStudent.getEducation(), new EducationDTO(LocalDate.of(2000, 1, 1), LocalDate.of(2001, 1, 1),
                    "degree", "institution", 1));
            assertEquals(testStudent.getAward(), new AwardDTO(LocalDate.of(2000, 1, 1), "Titel", 1, 1));

            assertEquals(testStudent.getBirthdate(), LocalDate.of(2000, 1, 1));
            assertEquals(testStudent.getEmployer(), "Emp");
            assertEquals(testStudent.getSpecalization(), "BIS");
            assertEquals(testStudent.getFamilyname(), "Müller");
            assertEquals(testStudent.getUniversitylocation(), "Hennef");
            assertEquals(testStudent.getJob(), "Java Entwickler");
            assertEquals(testStudent.getMajor(), "Major");
        } catch (Exception e) {
            System.out.println("failed bro");
            e.printStackTrace();
            fail();
        }
    }

 */

    @Test
    @Order(3)
    void getOne() {
        try {
            assertNull(testStudent);

            testStudent = dao.getOne(2);
            testDTO(testStudent);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    /*
    @Test
    @Order(4)
    void updateOne() {
        assertNull(testStudent);
        try {
            testStudent = dao.getOne(1);
            testStudent.setName("Gerhard");
            testStudent.setFamilyname("Heinrichs");
            testStudent.setJob("Java Entwickler");
            testStudent.setAnrede( "Herr");
            testStudent.setMajor("neuer Major");
            testStudent.setUniversitylocation("Hennef");
            testStudent.setSpecalization( "BIS");
            testStudent.setEmployer( "Emp");
            testStudent.setBirthdate(LocalDate.of(2000, 1, 1));
            testStudent.setAnrede( "Herr");
            testStudent.setDocuments(new DocumentDTO(1, 2, true, "Titel",
                    "Beschreibung", LocalDate.of(2000, 1, 1), new byte[60]));
            updatedStudent = dao.updateOne(testStudent);
            assertEquals(teststudentid, updatedStudent.getStudentid());
            assertEquals(testStudent.getName(), "Gerhard");
            assertEquals(testStudent.getFamilyname(), "Heinrichs");
            assertEquals(testStudent.getMajor(), "neuer Major");
            assertEquals(testStudent.getDocuments(), new DocumentDTO(1, 2, true, "Titel",
                    "Beschreibung", LocalDate.of(2000, 1, 1), new byte[60]));
            assertEquals(testStudent.getAnrede(), "Herr");
            assertEquals(testStudent.getBirthdate(), LocalDate.of(2000, 1, 1));
            assertEquals(testStudent.getEmployer(), "Emp");
            assertEquals(testStudent.getSpecalization(), "BIS");
            assertEquals(testStudent.getUniversitylocation(), "Hennef");
            assertEquals(testStudent.getJob(), "Java Entwickler");
            assertEquals(testStudent.getAnrede(), "Herr");
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

     */

    @Test
    @Order(5)
    void deleteOne() {
        /*
        Garnicht Implementiert

        assertNull(testStudent);
        try {
            testStudent = dao.getOne(getTeststudentid());
            dao.deleteOne(testStudent);
            assertThrows(Exception.class, () -> dao.getOne(getTeststudentid()));
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

         */
    }


    private void testDTO(StudentDTO dto) {
        /*
        assertEquals(dto.getName(), "Gerd");
        assertEquals(dto.getBirthdate(), LocalDate.of(2000, 1, 1));
        assertEquals(dto.getEmployer(), "Emp");
        assertEquals(dto.getSpecalization(), "BIS");
        assertEquals(dto.getFamilyname(), "Müller");
        assertEquals(dto.getUniversitylocation(), "Hennef");
        assertEquals(dto.getJob(), "Java Entwickler");
        assertEquals(dto.getMajor(), "Major");
        assertEquals(dto.getAnrede(), "Herr");

         */

        assertEquals(dto.getName(), "Franzi");
        assertEquals(dto.getBirthdate(), LocalDate.of(1970, 1, 1));
        assertEquals(dto.getEmployer(), "NeonSheep GmbH");
        assertEquals(dto.getSpecalization(), "Komplexe Softwaresysteme");
        assertEquals(dto.getFamilyname(), "Major");
        assertEquals(dto.getUniversitylocation(), "Sankt Augustin");
        assertEquals(dto.getJob(), "Teilzeitnerd");
        assertEquals(dto.getMajor(), "Wirtschaftsinformatik B.Sc.");
        assertNull(dto.getAnrede());
    }
}