package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.EducationDTO;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EducationDAOTest {
    private static LocalDate start = LocalDate.of(2000, 1, 1);
    private static LocalDate finish = LocalDate.of(2001, 1, 1);
    private static EducationDTO dto1;
    private static EducationDAO educationD;


    @BeforeAll
    static void beforeAll() {
        try {
            educationD = new EducationDAO();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        dto1 = new EducationDTO(start, finish, "degree", "institution", 1);
    }

    @Test
    @Order(1)
    void getAllTest() throws Exception {
        List<EducationDTO> dtos = new ArrayList<>();

        try {
            dtos = educationD.getAll();
        } catch (DatabaseException e) {
            fail();
        }
        assertTrue(dtos.size() > 0);
    }
}