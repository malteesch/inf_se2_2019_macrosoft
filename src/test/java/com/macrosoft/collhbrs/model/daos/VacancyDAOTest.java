package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.builder.VacancyBuilder;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class VacancyDAOTest {

    private static VacancyDTO newVacancy;
    private VacancyDTO testVacancy, updatedVacancy;
    private static VacancyDAO dao;
    private static int testVacancyId;

    static int getTestVacancyId() {
        return testVacancyId;
    }

    static void setTestVacancyId(int testVacancyId) {
        VacancyDAOTest.testVacancyId = testVacancyId;
    }

    @BeforeAll
    static void beforeAll() throws DatabaseException {
        dao = new VacancyDAO();
        newVacancy = new VacancyDTO();
        CompanyDTO company = new CompanyDAO().getOne(32);

        newVacancy = new VacancyBuilder()
                .createDefaultVacancy()
                .withTitle("Datenbankheini (m/w/d)")
                .withDescription("Cappuccino blue mountain as, half and half, medium strong caramelization spoon americano saucer plunger pot sugar. Java decaffeinated, latte, con panna iced robust lungo foam cinnamon sweet. Galão aged milk single origin grounds irish crema.")
                .withCompany(company)
                .get();
    }

    @BeforeEach
    void setUp() {
        testVacancy = null;
        updatedVacancy = null;
    }

    @Test
    @Order(1)
    void getAll() throws DatabaseException {
        List<VacancyDTO> dtos = new VacancyDAO().getAll();
        assertTrue(dtos.size() > 0);
    }


    @Test
    @Order(2)
    void createOne() throws DatabaseException {
        assertNull(testVacancy);
        testVacancy = dao.createOne(newVacancy);
        assertNotEquals(0, testVacancy.getVacancyId());
        setTestVacancyId(testVacancy.getVacancyId());
        testDTO(testVacancy);
    }

    @Test
    @Order(3)
    void getOne() throws DatabaseException {
        assertNull(testVacancy);
        testVacancy = dao.getOne(getTestVacancyId());
        testDTO(testVacancy);
    }

    @Test
    @Order(4)
    void updateOne() throws DatabaseException {
        assertNull(testVacancy);
        testVacancy = dao.getOne(getTestVacancyId());
        testVacancy.setTitle("Werkstudent Java Entwickler (w/m/d)");
        testVacancy.setDescription("Description");
        List<String> tags = new ArrayList<>();
        tags.add("newTag1");
        testVacancy.setTags(tags);
        updatedVacancy = dao.updateOne(testVacancy);
        assertEquals(testVacancyId, updatedVacancy.getVacancyId());
        assertEquals("Werkstudent Java Entwickler (w/m/d)", updatedVacancy.getTitle());
        assertEquals("Description", updatedVacancy.getDescription());
        assertEquals(1, updatedVacancy.getTags().size());
    }

    @Test
    @Order(5)
    void deleteOne() throws DatabaseException {
        assertNull(testVacancy);
        testVacancy = dao.getOne(getTestVacancyId());
        dao.deleteOne(testVacancy);
        assertThrows(DatabaseException.class, () -> dao.getOne(testVacancyId));
    }

    private void testDTO(VacancyDTO dto) {
        assertNotEquals(0, dto.getVacancyId());
        assertEquals("Datenbankheini (m/w/d)", dto.getTitle());
        assertEquals("Cappuccino blue mountain as, half and half, medium strong caramelization spoon americano saucer plunger pot sugar. Java decaffeinated, latte, con panna iced robust lungo foam cinnamon sweet. Galão aged milk single origin grounds irish crema.", dto.getDescription());
        assertEquals(32, dto.getCompany().getCompanyId());
        assertEquals(3, dto.getTags().size());
        assertEquals(3, dto.getTasks().size());
        assertEquals(3, dto.getRequirements().size());
        assertEquals(3, dto.getAdvantages().size());
    }
}
