package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.builder.CompanyBuilder;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CompanyDAOTest {

    private static CompanyDTO newCompany;
    private CompanyDTO testCompany, updatedCompany;
    private static CompanyDAO dao;
    private static int testCompanyId;


    public static int getTestCompanyid() {
        return testCompanyId;
    }

    public static void setTestCompanyId(int testCompanyId) {
        CompanyDAOTest.testCompanyId = testCompanyId;
    }


    @BeforeAll
    static void beforeAll() throws DatabaseException {
        dao = new CompanyDAO();
        newCompany = new CompanyBuilder().createDefaultCompany().done();
    }

    @BeforeEach
    void setUp() {
        testCompany = null;
        updatedCompany = null;
    }

    @Test
    @Order(1)
    void getAll() throws DatabaseException {
        List<CompanyDTO> dtos = new CompanyDAO().getAll();
        assertTrue(dtos.size() > 0);
    }

    @Test
    @Order(2)
    void createOne() throws Exception {
        assertNull(testCompany);
        testCompany = dao.createOne(newCompany);
        assertNotEquals(0, testCompany.getCompanyId());
        setTestCompanyId(testCompany.getCompanyId());
        assertEquals(testCompany.getName(), "Musterfirma GmbH");
        assertEquals(testCompany.getDescription(), "Wir sind eine Test Firma");
        assertEquals(testCompany.getWebsite(), "musterfirma.de");
    }


    @Test
    @Order(3)
    void getOne() throws DatabaseException {
        assertNull(testCompany);
        testCompany = dao.getOne(getTestCompanyid());
        testDTO(testCompany);
    }

    @Test
    @Order(4)
    void updateOne() throws DatabaseException {
        assertNull(testCompany);
        testCompany = dao.getOne(getTestCompanyid());
        testCompany.setName("H-brs");
        testCompany.setDescription("Hochschule");
        testCompany.setWebsite("www.h-brs.de");

        updatedCompany = dao.updateOne(testCompany);

        assertEquals(testCompanyId, updatedCompany.getCompanyId());
        assertEquals(testCompany.getName(), "H-brs");
        assertEquals(testCompany.getDescription(), "Hochschule");
        assertEquals(testCompany.getWebsite(), "www.h-brs.de");
    }

    @Test
    @Order(5)
    void deleteOne() throws Exception {
        assertNull(testCompany);
        testCompany = dao.getOne(getTestCompanyid());
        dao.deleteOne(testCompany);
        assertThrows(Exception.class, () -> dao.getOne(testCompanyId));
    }

    private void testDTO(CompanyDTO dto) {
        assertNotEquals(0, dto.getCompanyId());
        assertEquals(dto.getName(), "Musterfirma GmbH");
        assertEquals(dto.getDescription(), "Wir sind eine Test Firma");
        assertEquals(dto.getWebsite(), "musterfirma.de");
    }
}