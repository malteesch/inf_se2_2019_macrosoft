package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.DocumentDTO;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DocumentDAOTest {

    private static DocumentDTO newDocument;
    private DocumentDTO testDocument, updatedDocument;
    private static DocumentDAO dao;
    private static int testDocumentId;

    static int getTestDocumentId() {
        return testDocumentId;
    }

    static void setTestDocumentId(int testDocumentId) {
        DocumentDAOTest.testDocumentId = testDocumentId;
    }

    @BeforeAll
    static void beforeAll() {
        try {
            dao = new DocumentDAO();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        newDocument = new DocumentDTO();
        newDocument.setFile(new byte[100]);
        newDocument.setDocumenttitle("Lebenslauf");
        newDocument.setDocumentdescription("Das ist ein Lebenslauf");
        newDocument.setVisibility(false);
        newDocument.setUserid(1);
    }

    @BeforeEach
    void setUp() {
        testDocument = null;
        updatedDocument = null;
    }

    @Test
    @Order(1)
    void getAll() {
        List<DocumentDTO> dtos = new ArrayList<>();
        try {
            dtos = dao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        assertTrue(dtos.size() > 0);
    }

    @Test
    @Order(2)
    void createOne() {
        try {
            assertNull(testDocument);
            testDocument = dao.createOne(newDocument);
            assertNotEquals(0, testDocument.getDocumentid());
            setTestDocumentId(testDocument.getDocumentid());
            testDTO(testDocument);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    @Order(3)
    void getOne() {
        try {
            assertNull(testDocument);
            testDocument = dao.getOne(getTestDocumentId());
            testDTO(testDocument);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    @Order(4)
    void updateOne() {
        try {
            assertNull(testDocument);
            testDocument = dao.getOne(getTestDocumentId());
            testDocument.setDocumenttitle("Anschreiben");
            testDocument.setDocumentdescription("Das ist ein Anschreiben");
            testDocument.setVisibility(true);
            updatedDocument = dao.updateOne(testDocument);
            assertEquals("Anschreiben", updatedDocument.getDocumenttitle());
            assertEquals("Das ist ein Anschreiben", updatedDocument.getDocumentdescription());
            assertTrue(updatedDocument.getVisibility());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    @Order(5)
    void deleteOne() {
        assertNull(testDocument);
        try {
            testDocument = dao.getOne(getTestDocumentId());
            dao.deleteOne(testDocument);
            assertThrows(DatabaseException.class, () -> dao.getOne(getTestDocumentId()));
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    private void testDTO(DocumentDTO dto) {
        assertNotEquals(0, dto.getDocumentid());
        assertEquals("Lebenslauf", dto.getDocumenttitle());
        assertEquals("Das ist ein Lebenslauf", dto.getDocumentdescription());
        assertFalse(dto.getVisibility());
        assertEquals(new byte[100].length, dto.getFile().length);
    }

}

