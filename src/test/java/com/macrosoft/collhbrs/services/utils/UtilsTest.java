package com.macrosoft.collhbrs.services.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class UtilsTest {

    @Test
    void getFeatureStatus() {
        assertTrue(Utils.getFeatureStatus(Config.Features.APPLY));
    }
}