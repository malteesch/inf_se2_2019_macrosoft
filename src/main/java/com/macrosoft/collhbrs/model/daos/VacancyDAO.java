/**
 * @author Platzhalter
 * @version 1.0
 * Weist die Klasse dem DAO-Package zu
 */

package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Diese Klasse bildet das VacancynDAO ab. Hierzu wird das @see AbstractDAO spezialisiert und das @see DAOInterface implementiert
 */
public class VacancyDAO extends AbstractDAO<VacancyDTO> implements DAOInterface<VacancyDTO> {

    public VacancyDAO() throws DatabaseException {
    }

    @Override
    public List<VacancyDTO> getAll() throws DatabaseException {
        //language=PostgreSQL
        String query = "SELECT vacancytitle       AS title,\n" +
                "       vacancydescription AS description,\n" +
                "       vacancy.companyid  AS companyid,\n" +
                "       vacancyid          AS id,\n" +
                "       creationdate       AS date,\n" +
                "       tags,\n" +
                "       tasks,\n" +
                "       requirements,\n" +
                "       advantages\n" +
                "FROM \"macrosoft_collHBRS\".vacancy\n" +
                "         JOIN \"macrosoft_collHBRS\".company ON vacancy.companyid = company.companyid\n" +
                "         JOIN \"macrosoft_collHBRS\".\"user\" ON company.userid = \"user\".userid\n" +
                "         JOIN \"macrosoft_collHBRS\".address ON \"user\".address = address.addressid;";
        return executeQuery(query);
    }

    /**
     * @param identifier please use getOne(int id)
     * @return VacancyDTO identified by its id passed as a String
     * @throws DatabaseException with the error message from JDBC
     */
    @Override
    @Deprecated
    public VacancyDTO getOne(String identifier) throws DatabaseException {
        return getOne(Integer.parseInt(identifier));
    }

    @Override
    public VacancyDTO getOne(int id) throws DatabaseException {
        //language=PostgreSQL
        String query = "SELECT vacancytitle       AS title,\n" +
                "       vacancydescription AS description,\n" +
                "       vacancy.companyid  AS companyid,\n" +
                "       vacancyid          AS id,\n" +
                "       creationdate       AS date,\n" +
                "       tags,\n" +
                "       tasks,\n" +
                "       requirements,\n" +
                "       advantages\n" +
                "FROM \"macrosoft_collHBRS\".vacancy\n" +
                "         JOIN \"macrosoft_collHBRS\".company ON vacancy.companyid = company.companyid\n" +
                "         JOIN \"macrosoft_collHBRS\".\"user\" ON company.userid = \"user\".userid\n" +
                "         JOIN \"macrosoft_collHBRS\".address ON \"user\".address = address.addressid\n" +
                "WHERE vacancyid = ?;";
        List<VacancyDTO> list = executePreparedQuery(query, id);
        if (list.size() < 1) {
            throw new DatabaseException("[" + VacancyDAO.class.toString() + "] VacancyDAO.getOne(" + id + ") did not return a DTO");
        }
        return list.get(0);
    }

    @Override
    public VacancyDTO deleteOne(VacancyDTO dto) throws DatabaseException {
        //language=PostgreSQL
        final String query = "DELETE\n" +
                "FROM \"macrosoft_collHBRS\".vacancy\n" +
                "WHERE vacancyid = ? RETURNING vacancytitle AS title\n" +
                "    , vacancydescription AS description\n" +
                "    , vacancy.companyid AS companyid\n" +
                "    , vacancyid AS id\n" +
                "    , creationdate AS date\n" +
                "    , tags\n" +
                "    , tasks\n" +
                "    , requirements\n" +
                "    , advantages;";
        List<VacancyDTO> list = executePreparedQuery(query, dto.getVacancyId());
        if (list.size() == 0)
            throw new DatabaseException("[" + VacancyDAO.class.toString() + "] VacancyDAO.deleteOne() did not delete the vacancy");
        return list.get(0);
    }

    @Override
    public VacancyDTO createOne(VacancyDTO dto) throws DatabaseException {
        //language=PostgreSQL
        final String query = "INSERT INTO \"macrosoft_collHBRS\".vacancy\n" +
                "VALUES (DEFAULT, ?, ?, ?, DEFAULT, ?, ?, ?, ?, ?) RETURNING vacancytitle AS title, vacancydescription AS description,\n" +
                "                                                  vacancy.companyid AS companyid, vacancyid AS id,\n" +
                "                                                  creationdate AS date, tags, tasks, requirements, advantages;";
        List<VacancyDTO> list = executePreparedQuery(query,
                dto.getTitle(),
                dto.getTags(),
                dto.getDescription(),
                dto.getCompany().getCompanyId(),
                dto.getCompany().getAddress().getAddressid(),
                dto.getTasks(),
                dto.getRequirements(),
                dto.getAdvantages()
        );
        if (list.size() < 1)
            throw new DatabaseException("[" + VacancyDAO.class.toString() + "] VacancyDAO.createOne() did not return a DTO");
        return list.get(0);
    }

    @Override
    public VacancyDTO updateOne(VacancyDTO dto) throws DatabaseException {
        //language=PostgreSQL
        final String query = "UPDATE \"macrosoft_collHBRS\".vacancy\n" +
                "SET (vacancytitle, vacancydescription, companyid, tags, tasks, requirements, advantages) = (?, ?, ?, ?, ?, ?, ?)\n" +
                "WHERE vacancyid = ? RETURNING vacancytitle AS title, vacancydescription AS description, companyid AS companyid, vacancyid AS id, creationdate AS date, tags, tasks, requirements, advantages;";
        List<VacancyDTO> list = executePreparedQuery(query,
                dto.getTitle(),
                dto.getDescription(),
                dto.getCompany().getCompanyId(),
                dto.getTags(),
                dto.getTasks(),
                dto.getRequirements(),
                dto.getAdvantages(),
                dto.getVacancyId()
        );
        if (list.size() == 0)
            throw new DatabaseException("[" + VacancyDAO.class.toString() + "] Error updating the vacancy in the Database");
        return list.get(0);
    }

    @Override
    protected VacancyDTO createDTO(ResultSet set) throws DatabaseException {
        VacancyDTO dto;
        try {
            dto = new VacancyDTO();
            dto.setTitle(set.getString("title"));
            dto.setDescription(set.getString("description"));
            dto.setVacancyId(set.getInt("id"));
            dto.setDate(set.getDate("date").toLocalDate());
            if (set.getArray("tags") != null) {
                dto.setTags(Utils.transform((String[]) (set.getArray("tags").getArray())));
            } else {
                dto.setTags(new ArrayList<>());
            }
            if (set.getArray("tasks") != null) {
                dto.setTasks(Utils.transform((String[]) (set.getArray("tasks").getArray())));
            } else {
                dto.setTasks(new ArrayList<>());

            }
            if (set.getArray("requirements") != null) {
                dto.setRequirements(Utils.transform((String[]) (set.getArray("requirements").getArray())));
            } else {
                dto.setRequirements(new ArrayList<>());

            }
            if (set.getArray("advantages") != null) {
                dto.setAdvantages(Utils.transform((String[]) (set.getArray("advantages").getArray())));
            } else {
                dto.setAdvantages(new ArrayList<>());
            }
            CompanyDTO company = new CompanyDAO().getOne(set.getInt("companyid"));
            dto.setCompany(company);
            return dto;
        } catch (SQLException e) {
            throw new DatabaseException("couldn't create VacancyDTO from resultset" + e.getMessage());
        }
    }
}
