/**
 * @author Platzhalter
 * @version 1.0
 * <p>
 * Weist die Klasse dem Daos-Package zu
 * <p>
 * Weist die Klasse dem Daos-Package zu
 * <p>
 * Weist die Klasse dem Daos-Package zu
 * <p>
 * Weist die Klasse dem Daos-Package zu
 * <p>
 * Weist die Klasse dem Daos-Package zu
 * <p>
 * Weist die Klasse dem Daos-Package zu
 */

/**
 * Weist die Klasse dem Daos-Package zu
 */
package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.AddressDTO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Diese Klasse übernimmt das @see AbstractDAO und spezialisiert es zum CompanyDAO. Hierzu wird auch die Funktionalität
 * des @see DAOInterface implementiert
 */
public class CompanyDAO extends AbstractDAO<CompanyDTO> implements DAOInterface<CompanyDTO> {

    /**
     * Diese Methode dient zur Instanzierung von CompanyDAO Objekten
     */
    public CompanyDAO() throws DatabaseException {
    }

    /**
     * Diese Methode dient zur Einspeisung der Companydaten aus den Datenbanktabellen
     */
    @Override
    public List<CompanyDTO> getAll() throws DatabaseException {
        //language=PostgreSQL
        String query = "SELECT *\n" +
                "FROM \"macrosoft_collHBRS\".company\n" +
                "JOIN \"macrosoft_collHBRS\".\"user\" ON company.userid = \"user\".userid\n" +
                "JOIN \"macrosoft_collHBRS\".address ON \"user\".address = address.addressid;";
        return executeQuery(query);
    }

    /**
     * Diese Methode dient zur Einspeisung einer gezielt gesuchten Companydata anhand eines Strings
     */
    @Override
    public CompanyDTO getOne(String identifier) throws DatabaseException {
        //language=PostgreSQL
        String query = "SELECT *\n" +
                "FROM \"macrosoft_collHBRS\".company\n" +
                "JOIN \"macrosoft_collHBRS\".user ON company.userid = \"user\".userid\n" +
                "JOIN \"macrosoft_collHBRS\".address ON \"user\".address = address.addressid\n" +
                "WHERE username = '" + identifier + "'\n" +
                "OR email = '" + identifier + "'\n" +
                "OR companyname = '" + identifier + "';";
        List<CompanyDTO> list = executeQuery(query);
        if (list.size() < 1) {
            throw new DatabaseException("CompanyDAO.getOne(" + identifier + ") no company found");
        }
        return list.get(0);
    }

    /**
     * Diese Methode dient zur Einspeisung eines gezielt gesuchten Companydata anhand eines Integers(ID)
     */
    @Override
    public CompanyDTO getOne(int id) throws DatabaseException {
        //language=PostgreSQL
        String selectQuery = "SELECT *\n" +
                "FROM \"macrosoft_collHBRS\".\"user\"\n" +
                "         JOIN \"macrosoft_collHBRS\".company ON \"user\".userid = company.userid\n" +
                "         JOIN \"macrosoft_collHBRS\".address on \"user\".address = address.addressid\n" +
                "WHERE companyid = " + id + "\n" +
                "OR company.userid = " + id + ";";
        List<CompanyDTO> queryResult = executeQuery(selectQuery);
        if (queryResult.size() < 1) {
            throw new DatabaseException("CompanyDAO.getOne(" + id + ") no company found");
        }
        return queryResult.get(0);
    }


    /**
     * Diese Methode dient zur Löschung eines gezielten Companyobjektes
     */
    @Override
    public CompanyDTO deleteOne(CompanyDTO itemToDelete) throws Exception {
        //language=PostgreSQL
        String queryAddress = "DELETE FROM \"macrosoft_collHBRS\".address\n" +
                "WHERE \"macrosoft_collHBRS\".address.addressid = '" + itemToDelete.getAddressid() + "';";

        String queryUser = "DELETE FROM \"macrosoft_collHBRS\".\"user\"\n" +
                "WHERE \"macrosoft_collHBRS\".\"user\".userid = '" + itemToDelete.getUserid() + "';";

        String queryCompany = "DELETE FROM \"macrosoft_collHBRS\".company\n" +
                "WHERE \"macrosoft_collHBRS\".company.companyid = '" + itemToDelete.getCompanyId() + "';";

        try {
            PreparedStatement pst1 = this.getPreparedStatement(queryCompany);
            PreparedStatement pst2 = this.getPreparedStatement(queryUser);
            PreparedStatement pst3 = this.getPreparedStatement(queryAddress);
            pst1.executeUpdate();
            pst2.executeUpdate();
            pst3.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException();
        }
        return itemToDelete;
    }

    /**
     * Diese Methode dient zur Erstellung eines bisher nicht existierenden Companydatensatzes(?)
     */
    @Override
    public CompanyDTO createOne(CompanyDTO dto) throws Exception {
        UserDAO uDAO = new UserDAO();
        UserDTO uDTO = uDAO.createOne(dto);
        //language=PostgreSQL
        String query = "INSERT INTO \"macrosoft_collHBRS\".company (companyname, companydescription, companyid, userid, website)\n" +
                "VALUES ('" + dto.getName() + "','" + dto.getDescription() + "', DEFAULT, '" + uDTO.getUserid() + "', '" + dto.getWebsite() + "')\n" +
                "RETURNING *";
        PreparedStatement pst = this.getPreparedStatement(query);
        ResultSet set = pst.executeQuery();
        if (set.next()) {
            CompanyDTO company = new CompanyDTO();
            company.setUserid(set.getInt(1));
            company.setName(set.getString("companyname"));
            company.setDescription(set.getString("companydescription"));
            company.setUserid(set.getInt("userid"));
            company.setCompanyId(set.getInt("companyid"));
            company.setWebsite(set.getString("website"));
            System.out.println("Company erfolgreich gespeichert!");
            return company;
        } else {
            System.out.println("Company-Objekt konnte nicht richtig gespeichert werden!");
            return null;
        }

    }


    @Override
    public CompanyDTO updateOne(CompanyDTO updatedItem) throws DatabaseException {

        CompanyDTO companyNewDto = new CompanyDTO();
        AddressDTO address = updatedItem.getAddress();
        companyNewDto.setAddress(address);

        // update user
        String queryUser = null;
        ResultSet set = null;
        if (updatedItem.getPassword() == null) {
            //language=PostgreSQL
            queryUser = "UPDATE \"macrosoft_collHBRS\".\"user\"\n" +
                    "SET (username, email, photo) = (?, ?, ?)\n" +
                    "WHERE userid = '" + updatedItem.getUserid() + "';";

            try {
                PreparedStatement pst = this.getPreparedStatement(queryUser);
                pst.setString(1, updatedItem.getUsername());
                pst.setString(2, updatedItem.getEmail());
                pst.setBytes(3, updatedItem.getImage());
                pst.executeUpdate();
            } catch (SQLException e) {
                throw new DatabaseException(e.getMessage());
            }
        } else {
            //language=PostgreSQL
            queryUser = "UPDATE \"macrosoft_collHBRS\".\"user\"\n" +
                    "SET (username, email, password, photo) = (?, ?, ?, ?)\n" +
                    "WHERE userid = '" + updatedItem.getUserid() + "';";

            try {
                PreparedStatement pst = this.getPreparedStatement(queryUser);
                pst.setString(1, updatedItem.getUsername());
                pst.setString(2, updatedItem.getEmail());
                pst.setString(3, updatedItem.getPassword());
                pst.setBytes(4, updatedItem.getImage());
                pst.executeUpdate();
            } catch (SQLException e) {
                throw new DatabaseException();
            }

        }

        // update address
        AddressDAO dao = new AddressDAO();
        dao.updateOne(updatedItem.getAddress());

        // update company
        //language=PostgreSQL
        String queryCompany = "UPDATE \"macrosoft_collHBRS\".company\n" +
                "SET (companyname, companydescription, website) = (?, ?, ?)\n" +
                "WHERE userid = ?;";

        try {
            PreparedStatement pst = this.getPreparedStatement(queryCompany);
            pst.setString(1, updatedItem.getName());
            pst.setString(2, updatedItem.getDescription());
            pst.setString(3, updatedItem.getWebsite());
            pst.setInt(4, updatedItem.getUserid());
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException();
        } finally {
            connection.closeConnection();
        }

        return getOne(updatedItem.getUserid());
    }

    @Override
    public CompanyDTO createDTO(ResultSet set) throws DatabaseException {
        CompanyDTO dto = new CompanyDTO();
        try {
            dto.setName(set.getString("companyname"));
            dto.setDescription(set.getString("companydescription"));
            dto.setCompanyId(set.getInt("companyid"));
            dto.setImage(set.getBytes("photo"));
            dto.setWebsite(set.getString("website"));
            dto.setUserid(set.getInt("userid"));
            dto.setUsername(set.getString("username"));
            dto.setEmail(set.getString("email"));
            AddressDTO address = new AddressDAO().getOne(set.getInt("addressid"));
            dto.setAddress(address);
        } catch (SQLException e) {
            throw new DatabaseException("[" + CompanyDAO.class.toString() + "]There was an error creating the companyDTO! " + e);
        }
        return dto;
    }
}