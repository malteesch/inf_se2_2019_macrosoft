package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.AwardDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AwardDAO extends AbstractDAO<AwardDTO> {
    public AwardDAO() throws DatabaseException {
    }

    public List<AwardDTO> getAll(int id) throws DatabaseException {
        //language=PostgreSQL
        String selectQuery = "SELECT * from \"macrosoft_collHBRS\".award where studentid = " + id + ";";
        return executeQuery(selectQuery);
    }

    public List<AwardDTO> getAll() throws Exception {
        //language=PostgreSQL
        String selectQuery = "SELECT * FROM \"macrosoft_collHBRS\".award;";
        return executeQuery(selectQuery);
    }

    @Override
    public AwardDTO createDTO(ResultSet set) throws DatabaseException {
        AwardDTO dto = new AwardDTO();
        try {
            dto.setDate(set.getDate("date").toLocalDate());
        } catch (SQLException e) {
            //TODO
        }
        try {
            dto.setTitle(set.getString("title"));
        } catch (SQLException e) {
            //TODO
        }
        return dto;
    }
}