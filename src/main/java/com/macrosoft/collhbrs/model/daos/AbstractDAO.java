/*
 * @author Platzhalter
 * @version 1.0
 */

/*
 * Weist die Klasse dem Daos-Package zu
 */
package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.services.db.JDBCConnection;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Diese Klasse dient zur Definition eines abstrakten DAO`s, welches in anderen Klassen als Grundlage weitergenutzt wird
 */
public abstract class AbstractDAO<T> {

    /**
     * Diese Variable dient zum Verbndungsaufbau mit der Datenbank
     */
    protected JDBCConnection connection = JDBCConnection.getInstance();

    /**
     * Diese Methode ist der Klassenkonstruktor
     */
    protected AbstractDAO() throws DatabaseException {
    }

    protected Statement getStatement() throws DatabaseException {
        return connection.getStatement();
    }

    protected PreparedStatement getPreparedStatement(String sql) throws DatabaseException {
        return connection.getPreparedStatement(sql);
    }

    protected List<T> executeQuery(String query) throws DatabaseException {
        ArrayList<T> resultList = new ArrayList<>();
        try {
            Statement st = this.getStatement();
            ResultSet set = st.executeQuery(query);
            while (set.next()) {
                resultList.add(createDTO(set));
            }
            return resultList;
        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        } finally {
            connection.closeConnection();
        }
    }

    /**
     * @param values only use String, Integer, List<String || Integer>, byte[]
     * @return list of T created with createDTO method implemented in subclass
     * @throws DatabaseException if there is an error regarding the database
     */
    protected List<T> executePreparedQuery(String query, Object... values) throws DatabaseException {
        List<T> resultList = new ArrayList<>();
        PreparedStatement pst = this.getPreparedStatement(query);
        try {
            for (int i = 0; i < values.length; i++) {
                if (values[i] instanceof String) {
                    pst.setString(i + 1, (String) values[i]);
                } else if (values[i] instanceof Integer) {
                    pst.setInt(i + 1, (Integer) values[i]);
                } else if (values[i] instanceof ArrayList) {
                    Object[] items = ((ArrayList) values[i]).toArray();
                    if (items.length > 0 && items[0] instanceof String)
                        pst.setArray(i + 1, pst.getConnection().createArrayOf("text", items));
                    else if (items.length > 0)
                        pst.setArray(i + 1, pst.getConnection().createArrayOf("integer", items));
                    else pst.setArray(i + 1, null);
                } else if (values[i] instanceof byte[]) {
                    pst.setBytes(i + 1, (byte[]) values[i]);
                } else if (values[i] instanceof LocalDate) {
                    pst.setDate(i + 1, Date.valueOf((LocalDate) values[i]));
                } else if (values[i] instanceof Boolean) {
                    pst.setBoolean(i + 1, (Boolean) values[i]);
                }
            }
            ResultSet set = pst.executeQuery();
            while (set.next()) {
                resultList.add(createDTO(set));
            }
        } catch (SQLException e) {
            throw new DatabaseException("[" + AbstractDAO.class.toString() + "] Error executing prepared statement: " + e.getMessage());
        } finally {
            connection.closeConnection();
        }
        return resultList;
    }

    abstract protected T createDTO(ResultSet set) throws DatabaseException;
}
