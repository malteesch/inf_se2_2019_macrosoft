/**
 * @author Platzhalter
 * @version 1.0
 */

/**
 * Weist die Klasse dem DAO-Package zu
 */

package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.InvalidCredentialsException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.model.dtos.AddressDTO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


/**
 * Diese Klasse bildet das UserDAO ab. Hierzu wird das @see AbstractDAO spezialisiert und das @see DAOInterface implementiert
 */
public class UserDAO extends AbstractDAO<UserDTO> implements DAOInterface<UserDTO> {

    public UserDAO() throws DatabaseException {
        super();
    }

    /*private String username, email, password;
    private int userid, addressid;
    private byte[] image;
    private AddressDTO address;
    private LocalDate registrationdate;*/

    @Override
    public List<UserDTO> getAll() throws DatabaseException {
        //language=PostgreSQL
        final String query = "SELECT *\n" +
                "FROM \"macrosoft_collHBRS\".\"user\"\n" +
                "JOIN \"macrosoft_collHBRS\".address ON \"user\".address = address.addressid;";
        return executeQuery(query);
    }

    @Override
    public UserDTO getOne(String login) throws InvalidCredentialsException, DatabaseException {
        //language=PostgreSQL
        String selectQuery = "SELECT *\n" +
                "FROM \"macrosoft_collHBRS\".\"user\"\n" +
                "         FULL OUTER JOIN \"macrosoft_collHBRS\".address ON \"user\".address = address.addressid\n" +
                "WHERE username = ?\n" +
                "OR email = ?;";
        List<UserDTO> queryResult = executePreparedQuery(selectQuery, login, login);
        if (queryResult.size() < 1) {
            throw new InvalidCredentialsException();
        }
        return queryResult.get(0);
    }

    @Override
    public UserDTO getOne(int id) throws UserException, DatabaseException {
        //language=PostgreSQL
        final String selectQuery = "SELECT *\n" +
                "FROM \"macrosoft_collHBRS\".\"user\"\n" +
                "         FULL OUTER JOIN \"macrosoft_collHBRS\".address ON \"user\".address = address.addressid\n" +
                "WHERE userid = ?;";
        List<UserDTO> queryResult = executePreparedQuery(selectQuery, id);
        if (queryResult.size() < 1) {
            throw new InvalidCredentialsException();
        }
        return queryResult.get(0);
    }

    @Override
    public UserDTO deleteOne(UserDTO user) throws DatabaseException {
        //language=PostgreSQL
        final String deleteQuery = "DELETE FROM \"macrosoft_collHBRS\".user\n" +
                "WHERE username = ?\n" +
                "RETURNING *;";

        List<UserDTO> result = executePreparedQuery(deleteQuery, user.getUsername());
        if (result.size() < 1) {
            throw new DatabaseException("[" + UserDAO.class.toString() + "] deleteOne() did not return a DTO");
        }
        return result.get(0);
    }

    @Override
    public UserDTO createOne(UserDTO user) throws DatabaseException {
        AddressDTO address = new AddressDTO();
        try {
            address = new AddressDAO().createOne(user.getAddress());
        }
        catch (Exception e){
            e.printStackTrace();
        }

        //language=PostgreSQL
        final String insertQuery = "INSERT INTO \"macrosoft_collHBRS\".user (username, email, password, address, photo)\n" +
                "VALUES (?, ?, ?, ?, ?)\n" +
                "RETURNING *";

        List<UserDTO> result = executePreparedQuery(insertQuery, user.getUsername(), user.getEmail(), user.getPassword(), address.getAddressid(), user.getImage() != null ? user.getImage() : new byte[0]);
        if (result.size() < 1) {
            throw new DatabaseException("[" + UserDAO.class.toString() + "] createOne() did not return a DTO");
        }
        return result.get(0);
    }

    @Override
    public UserDTO updateOne(UserDTO user) throws DatabaseException {
        //language=PostgreSQL
        final String updateQuery = "UPDATE \"macrosoft_collHBRS\".user\n" +
                "SET (username, email, password, photo) = (?, ?, ?, ?)\n" +
                "WHERE userid = ?" +
                "RETURNING *;";
        List<UserDTO> result = executePreparedQuery(updateQuery, user.getUsername(), user.getEmail(), user.getPassword(), user.getImage(), user.getUserid());
        if (result.size() < 1) {
            throw new DatabaseException("[" + UserDAO.class.toString() + "] updateOne() did not return a DTO");
        }
        return result.get(0);
    }

    @Override
    public UserDTO createDTO(ResultSet set) throws DatabaseException {
        UserDTO dto;
        try {
            dto = new UserDTO(set.getString("username"), set.getString("email"), set.getString("password"));
            dto.setUserid(set.getInt("userid"));
            dto.setAddressid(set.getInt("address"));
            dto.setImage(set.getBytes("photo"));
            dto.setRegistrationdate(set.getDate("registrationdate").toLocalDate());

            AddressDTO address = new AddressDAO().getOne(dto.getAddressid());
            dto.setAddress(address);
            return dto;
        } catch (SQLException e) {
            throw new DatabaseException("couldn't create UserDTO from resultset: " + e.getMessage());
        }
    }
}
