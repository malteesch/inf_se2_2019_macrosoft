/**
 * @author Platzhalter
 * @version 1.0
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 */

/**
 * Weist die Klasse dem DAO-Package zu
 */
package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.EducationDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Diese Klasse bildet das EducationDAO ab. Hierzu wird das @see AbstractDAO spezialisiert und das @see DAOInterface implementiert
 */
public class EducationDAO extends AbstractDAO<EducationDTO> {
    public EducationDAO() throws DatabaseException {
    }

    public List<EducationDTO> getAll() throws Exception {
        //language=PostgreSQL
        final String query = "SELECT * FROM \"macrosoft_collHBRS\".education;";
        return executeQuery(query);
    }

    public List<EducationDTO> getAll(int id) throws Exception {
        //language=PostgreSQL
        String selectQuery = "SELECT * from \"macrosoft_collHBRS\".education where studentid = " + id + ";";
        return executeQuery(selectQuery);

    }

    @Override
    public EducationDTO createDTO(ResultSet set) throws DatabaseException {
        EducationDTO dto = new EducationDTO();
        try {
            dto.setInstitution(set.getString("institution"));
        } catch (SQLException e) {

        }
        try {
            dto.setDegree(set.getString("degree"));
        } catch (SQLException e) {

        }
        try {
            dto.setStart(set.getDate("start").toLocalDate());
        } catch (SQLException e) {

        }
        try {
            if (set.getDate("finish") != null)
                dto.setFinish(set.getDate("finish").toLocalDate());
        } catch (SQLException e) {
            throw new DatabaseException();
        }

        return dto;
    }
}
