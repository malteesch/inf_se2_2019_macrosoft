package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.DocumentDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class DocumentDAO extends AbstractDAO<DocumentDTO> implements DAOInterface<DocumentDTO> {

    public DocumentDAO() throws DatabaseException {
    }

    @Override
    public List<DocumentDTO> getAll() throws Exception {
        final String query = "SELECT * FROM \"macrosoft_collHBRS\".document;";
        return executeQuery(query);
    }

    public List<DocumentDTO> getAll(int id) throws Exception {
        //language=PostgreSQL
        final String selectQuery = "SELECT * FROM \"macrosoft_collHBRS\".document WHERE userid = ?";
        List<DocumentDTO> result = executePreparedQuery(selectQuery, id);
        if (result.size() < 1) {
            throw new DatabaseException("[" + DocumentDAO.class.toString() + "] getAll(" + id + ") did not return a DTO");
        }
        return result;
    }

    @Override
    @Deprecated
    public DocumentDTO getOne(String identifier) throws Exception {
        return getOne(Integer.parseInt(identifier));
    }

    @Override
    public DocumentDTO getOne(int id) throws Exception {
        //language=PostgreSQL
        final String selectQuery = "SELECT * FROM \"macrosoft_collHBRS\".document WHERE documentid = ?";
        List<DocumentDTO> result = executePreparedQuery(selectQuery, id);
        if (result.size() < 1) {
            throw new DatabaseException("[" + DocumentDAO.class.toString() + "] getOne(" + id + ") did not return a DTO");
        }
        return result.get(0);
    }

    @Override
    public DocumentDTO deleteOne(DocumentDTO itemToDelete) throws Exception {
        //language=PostgreSQL
        final String query = "DELETE\n" +
                "FROM \"macrosoft_collHBRS\".document\n" +
                "WHERE documentid = ? RETURNING *";
        List<DocumentDTO> result = executePreparedQuery(query, itemToDelete.getDocumentid());
        if (result.size() < 1)
            throw new DatabaseException("[" + DocumentDAO.class.toString() + "] deleteOne() did not return a DTO");
        return result.get(0);
    }

    @Override
    public DocumentDTO createOne(DocumentDTO dto) throws Exception {
        //language=PostgreSQL
        final String query = "INSERT INTO \"macrosoft_collHBRS\".document (visibility, documenttitle, documentdescription, file, userid)\n" +
                "VALUES (?,?,?,?,?)\n" +
                "RETURNING *;";

        List<DocumentDTO> list = executePreparedQuery(query,
                dto.getVisibility(),
                dto.getDocumenttitle(),
                dto.getDocumentdescription(),
                dto.getFile(),
                dto.getUserid()
        );
        if (list.size() < 1)
            throw new DatabaseException("[" + DocumentDAO.class.toString() + "] createOne() did not return a DTO");
        return list.get(0);
    }

    @Override
    public DocumentDTO updateOne(DocumentDTO dto) throws Exception {
        //language=PostgreSQL
        final String query = "UPDATE \"macrosoft_collHBRS\".document\n" +
                "SET (visibility, documenttitle, documentdescription, documentupdate, file, userid) = (?, ?, ?, ?, ?, ?)\n" +
                "WHERE documentid = ? RETURNING *;";
        List<DocumentDTO> result = executePreparedQuery(query,
                dto.getVisibility(),
                dto.getDocumenttitle(),
                dto.getDocumentdescription(),
                dto.getDocumentupdate(),
                dto.getFile(),
                dto.getUserid(),
                dto.getDocumentid()
        );

        if (result.size() < 1)
            throw new DatabaseException("[" + DocumentDAO.class.toString() + "] updateOne() did not return a DTO");
        return result.get(0);
    }

    @Override
    protected DocumentDTO createDTO(ResultSet set) {
        DocumentDTO dto = new DocumentDTO();
        try {
            dto.setUserid(set.getInt("userid"));
            dto.setDocumentid(set.getInt("documentid"));
            dto.setFile(set.getBytes("file"));
            dto.setDocumenttitle(set.getString("documenttitle"));
            dto.setDocumentdescription(set.getString("documentdescription"));
            dto.setVisibility(set.getBoolean("visibility"));
            dto.setDocumentupdate(new java.sql.Date(set.getDate("documentupdate").getTime()).toLocalDate());

        } catch (SQLException e) {
            e.printStackTrace(); //TODO
        }
        return dto;
    }
}
