/**
 * @author Platzhalter 
 * @version 1.0 
 */
 
/**
 * Weist die Klasse dem Control-Package zu
 */
package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.model.dtos.DTO;

import java.util.List;

/**
 * Dieses Interface dient zur implementierung der spezialisiereten DAO´s und zeigt die Methoden auf
 */
public interface DAOInterface<E extends DTO> {

    List<E> getAll() throws Exception;

    E getOne(String identifier) throws Exception;

    E getOne(int id) throws Exception;

    E deleteOne(E itemToDelete) throws Exception;

    E createOne(E dto) throws Exception;

    E updateOne(E updatedItem) throws Exception;
}
