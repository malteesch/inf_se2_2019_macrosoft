/**
 * @author Platzhalter
 * @version 1.0
 * <p>
 * Weist die Klasse dem DAO-Package zu
 */

/**
 * Weist die Klasse dem DAO-Package zu
 */

package com.macrosoft.collhbrs.model.daos;


import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.WorkDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


/**
 * Diese Klasse bildet das WorkDAO ab. Hierzu wird das @see AbstractDAO spezialisiert und das @see DAOInterface implementiert
 */
public class WorkDAO extends AbstractDAO<WorkDTO> {

    public WorkDAO() throws DatabaseException {
    }

    public List<WorkDTO> getAll(int id) throws Exception {
        //language=PostgreSQL
        String selectQuery = "SELECT * from \"macrosoft_collHBRS\".work where studentid = " + id + ";";
        return executeQuery(selectQuery);
    }

    @Override
    public WorkDTO createDTO(ResultSet set) throws DatabaseException {
        WorkDTO dto = new WorkDTO();
        try {
            dto.setCompany(set.getString("company"));
        } catch (SQLException e) {
            e.printStackTrace(); //TODO
        }
        try {
            dto.setDescription(set.getString("description"));
        } catch (SQLException e) {
            e.printStackTrace(); //TODO
        }
        try {
            dto.setStart(set.getDate("start").toLocalDate());
        } catch (SQLException e) {
            e.printStackTrace(); //TODO
        }
        try {
            dto.setFinish(set.getDate("finish").toLocalDate());
        } catch (SQLException e) {
            e.printStackTrace(); //TODO
        }

        return dto;
    }
}
