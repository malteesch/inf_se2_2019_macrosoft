/**
 * @author Platzhalter
 * @version 1.0
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 * <p>
 * Weist die Klasse dem DAO-Package zu
 */

/**
 * Weist die Klasse dem DAO-Package zu
 */
package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.model.dtos.*;
import com.macrosoft.collhbrs.services.utils.Utils;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class StudentDAO extends AbstractDAO<StudentDTO> implements DAOInterface<StudentDTO> {

    public StudentDAO() throws DatabaseException {
    }

    @Override
    public List<StudentDTO> getAll() throws DatabaseException {
        //language=PostgreSQL
        String query = "SELECT *\n" +
                "FROM \"macrosoft_collHBRS\".\"user\"\n" +
                "         INNER JOIN \"macrosoft_collHBRS\".student\n" +
                "                    ON \"user\".userid = student.userid\n" +
                "         INNER JOIN \"macrosoft_collHBRS\".address\n" +
                "                    ON \"user\".address = address.addressid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".skills\n" +
                "                    ON student.studentid = skills.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".award\n" +
                "                    ON student.studentid = award.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".work\n" +
                "                    ON student.studentid = work.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".education\n" +
                "                    ON student.studentid = education.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".document\n" +
                "                    ON student.userid = document.userid;";
        return executeQuery(query);
    }

    @Override
    public StudentDTO getOne(String identifier) throws UserException, DatabaseException {
        //language=PostgreSQL
        final String selectQuery = "SELECT *\n" +
                "FROM \"macrosoft_collHBRS\".\"user\"\n" +
                "         JOIN \"macrosoft_collHBRS\".student ON \"user\".userid = student.userid\n" +
                "         JOIN \"macrosoft_collHBRS\".address ON \"user\".address = address.addressid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".skills \n" +
                "                         ON student.studentid = skills.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".award\n" +
                "                         ON student.studentid = award.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".work\n" +
                "                         ON student.studentid = work.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".education\n" +
                "                         ON student.studentid = education.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".document\n" +
                "                         ON student.userid = document.userid\n" +
                "WHERE username = ?\n" +
                "OR email = ?;";

        List<StudentDTO> result = executePreparedQuery(selectQuery, identifier, identifier);
        if (result.size() < 1) {
            throw new DatabaseException("[" + StudentDAO.class.toString() + "] getOne(" + identifier + ") did not return a DTO");
        }
        return result.get(0);
    }

    @Override
    public StudentDTO getOne(int id) throws Exception {
        //language=PostgreSQL
        final String selectQuery = "SELECT *\n" +
                "FROM \"macrosoft_collHBRS\".\"user\"\n" +
                "         JOIN \"macrosoft_collHBRS\".student ON \"user\".userid = student.userid\n" +
                "         JOIN \"macrosoft_collHBRS\".address ON \"user\".address = address.addressid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".skills \n" +
                "                         ON student.studentid = skills.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".award\n" +
                "                         ON student.studentid = award.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".work\n" +
                "                         ON student.studentid = work.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".education\n" +
                "                         ON student.studentid = education.studentid\n" +
                "         LEFT OUTER JOIN \"macrosoft_collHBRS\".document\n" +
                "                         ON student.userid = document.userid\n" +
                "WHERE student.studentid = ?;";
        List<StudentDTO> result = executePreparedQuery(selectQuery, id);
        if (result.size() < 1) {
            throw new DatabaseException("[" + StudentDAO.class.toString() + "] getOne(" + id + ") did not return a DTO");
        }
        return result.get(0);
    }

    @Override
    public StudentDTO deleteOne(StudentDTO itemToDelete) throws Exception {
        //todo: implement
        return null;
    }

    @Override
    public StudentDTO createOne(StudentDTO dto) throws Exception {
        dto.setImage(new byte[0]);
        UserDTO user = new UserDAO().createOne(dto);
        //language=PostgreSQL
        final String query = "INSERT INTO \"macrosoft_collHBRS\".student (name, familyname, birthdaydate, userid)\n" +
                "VALUES (?,?,?,?)\n" +
                "RETURNING studentid";
        PreparedStatement pst = this.getPreparedStatement(query);
        pst.setString(1, dto.getName());
        pst.setString(2, dto.getFamilyname());
        pst.setDate(3, Date.valueOf(dto.getBirthdate()));
        pst.setInt(4, user.getUserid());
        ResultSet set = pst.executeQuery();
        if (!set.next())
            throw new DatabaseException("[" + StudentDAO.class.toString() + "] Student has not been created!");
        return getOne(set.getInt(1));
    }

    @Override
    public StudentDTO updateOne(StudentDTO updatedItem) throws Exception {
        // update address
        AddressDAO dao = new AddressDAO();
        dao.updateOne(updatedItem.getAddress());

        // update user
        UserDAO user = new UserDAO();
        user.updateOne(new UserDTO(updatedItem.getUsername(), updatedItem.getEmail(), updatedItem.getPassword(), updatedItem.getUserid(), updatedItem.getImage(), updatedItem.getAddress()));

        // update student
        //language=PostgreSQL
        String queryStudent = "UPDATE \"macrosoft_collHBRS\".student " +
                "SET (major, schwerpunkt) = (?, ?) " +
                "WHERE userid = '" + updatedItem.getUserid() + "';";
        try {
            PreparedStatement pst = this.getPreparedStatement(queryStudent);
            pst.setString(1, updatedItem.getMajor());
            pst.setString(2, updatedItem.getSpecalization());
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException("failed to udpate studentdata");
        }

        updatedItem.setStudentid(getOne(updatedItem.getUsername()).getStudentid());

        //language=PostgreSQL
        String querySkills = "INSERT INTO \"macrosoft_collHBRS\".skills (interests, skills, studentid) VALUES (?, ?, ?)\n" +
                "ON CONFLICT (studentid) DO UPDATE SET skills = ?, interests = ? WHERE skills.studentid = ?;";
        try {
            PreparedStatement pst = this.getPreparedStatement(querySkills);
            Object[] strings = updatedItem.getSkills().toArray();
            Object[] strings2 = updatedItem.getInterests().toArray();

            pst.setArray(1, pst.getConnection().createArrayOf("text", strings));
            pst.setArray(2, pst.getConnection().createArrayOf("text", strings2));
            pst.setInt(3, updatedItem.getStudentid());
            pst.setArray(4, pst.getConnection().createArrayOf("text", strings));
            pst.setArray(5, pst.getConnection().createArrayOf("text", strings2));
            pst.setInt(6, updatedItem.getStudentid());
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException("failed to update skills table: " + e.getMessage());
        }
        return this.getOne(updatedItem.getEmail());
    }

    @Override
    public StudentDTO createDTO(ResultSet set) throws DatabaseException {
        StudentDTO dto = new StudentDTO();

        //todo: handeln des cases, wenn ne spalte null ist
        try {
            dto.setUserid(set.getInt("userid"));
            dto.setStudentid(set.getInt("studentid"));
            dto.setName(set.getString("name"));
            dto.setFamilyname(set.getString("familyname"));
            dto.setBirthdate(new java.sql.Date(set.getDate("birthdaydate").getTime()).toLocalDate());
            dto.setMajor(set.getString("major"));
            dto.setUniversitylocation(set.getString("universitylocation"));
            dto.setSemester(set.getInt("semester"));
            dto.setJob(set.getString("job"));
            dto.setEmployer(set.getString("arbeitgeber"));
            dto.setSpecalization(set.getString("schwerpunkt"));

            ArrayList<String> arr;
            ArrayList<String> arr2;

            if (set.getArray("skills") != null) {
                arr = Utils.transform(set.getArray("skills"));
            } else {
                arr = new ArrayList<String>();
                arr.add("keine Werte vorhanden");
            }
            dto.setSkills(arr);

            if (set.getArray("interests") != null) {
                arr2 = Utils.transform(set.getArray("interests"));
            } else {
                arr2 = new ArrayList<String>();
                arr2.add("keine Werte vorhanden");
            }
            dto.setInterests(arr2);

            DocumentDTO docs = new DocumentDTO();
            docs.setDocumentid(set.getInt("documentid"));
            docs.setDocumenttitle(set.getString("documenttitle"));
            docs.setDocumentdescription(set.getString("documentdescription"));
            docs.setFile(set.getBytes("file"));
            dto.setDocuments(docs);

            UserDTO user = new UserDAO().getOne(dto.getUserid());
            dto.setEmail(user.getEmail());
            dto.setUsername(user.getUsername());
            dto.setPasswordAlreadyHashed(user.getPassword());
            dto.setImage(user.getImage());

            AddressDTO address = new AddressDAO().getOne(user.getAddressid());
            dto.setAddress(address);

            List<WorkDTO> works = new WorkDAO().getAll(dto.getStudentid());
            List<EducationDTO> educations = new EducationDAO().getAll(dto.getStudentid());
            List<AwardDTO> awards = new AwardDAO().getAll(dto.getStudentid());

            if (works == null) {
                works = new ArrayList<>();
                works.add(new WorkDTO());
            }
            dto.setWork((ArrayList<WorkDTO>) works);

            if (awards == null) {
                awards = new ArrayList<>();
                awards.add(new AwardDTO());
            }
            dto.setAward((ArrayList<AwardDTO>) awards);

            if (educations == null) {
                educations = new ArrayList<>();
                educations.add(new EducationDTO());
            }
            dto.setEducation((ArrayList<EducationDTO>) educations);
        } catch (Exception e) {
            e.printStackTrace();
            throw new DatabaseException("StudentDTO has not been created");
        }
        return dto;
    }
}