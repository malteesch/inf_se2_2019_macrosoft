package com.macrosoft.collhbrs.model.daos;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.AddressDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AddressDAO extends AbstractDAO<AddressDTO> implements DAOInterface<AddressDTO> {

    public AddressDAO() throws DatabaseException {
    }

    @Override
    public List<AddressDTO> getAll() throws DatabaseException {
        //language=PostgreSQL
        final String query = "SELECT street, housenumber, postalcode, city, country, addressid AS id\n" +
                "FROM \"macrosoft_collHBRS\".address;";
        return executeQuery(query);
    }

    /**
     * @param identifier please use getOne(int id)
     * @return AddressDTO identified by its id passed as a String
     * @throws DatabaseException with the error message from JDBC
     */
    @Override
    @Deprecated
    public AddressDTO getOne(String identifier) throws DatabaseException {
        return getOne(Integer.parseInt(identifier));
    }

    @Override
    public AddressDTO getOne(int id) throws DatabaseException {
        //language=PostgreSQL
        final String query = "SELECT street, housenumber, postalcode, city, country, addressid AS id\n" +
                "FROM \"macrosoft_collHBRS\".address\n" +
                "WHERE addressid = ?;";
        List<AddressDTO> list = executePreparedQuery(query, id);
        if (list.size() < 1) {
            throw new DatabaseException("[" + AddressDAO.class.toString() + "] AddressDAO.getOne(" + id + ") did not return a DTO");
        }
        return list.get(0);
    }

    @Override
    public AddressDTO deleteOne(AddressDTO dto) throws DatabaseException {
        //language=PostgreSQL
        final String query = "DELETE\n" +
                "FROM \"macrosoft_collHBRS\".address\n" +
                "WHERE addressid = ? RETURNING street, housenumber, postalcode, city, country, addressid AS id;";
        List<AddressDTO> list = executePreparedQuery(query, dto.getAddressid());
        if (list.size() == 0)
            throw new DatabaseException("[" + AddressDAO.class.toString() + "] AddressDAO.deleteOne() did not delete the address");
        return list.get(0);
    }

    @Override
    public AddressDTO createOne(AddressDTO dto) throws DatabaseException {
        //language=PostgreSQL
        final String query = "INSERT INTO \"macrosoft_collHBRS\".address\n" +
                "VALUES (?, ?, ?, ?, ?, DEFAULT) RETURNING street, housenumber, postalcode, city, country, addressid AS id;";

        List<AddressDTO> list = executePreparedQuery(query,
                dto.getStreet(),
                dto.getHousenumber(),
                dto.getPostalcode(),
                dto.getCity(),
                dto.getCountry()
        );
        if (list.size() == 0)
            throw new DatabaseException("[" + VacancyDAO.class.toString() + "] Error updating the vacancy in the Database");
        return list.get(0);
    }

    @Override
    public AddressDTO updateOne(AddressDTO dto) throws DatabaseException {
        //language=PostgreSQL
        final String query = "UPDATE \"macrosoft_collHBRS\".address\n" +
                "SET (street, housenumber, postalcode, city, country) = (?, ?, ?, ?, ?)\n" +
                "WHERE addressid = ? RETURNING street, housenumber, postalcode, city, country, addressid AS id;";

        List<AddressDTO> list = executePreparedQuery(query,
                dto.getStreet(),
                dto.getHousenumber(),
                dto.getPostalcode(),
                dto.getCity(),
                dto.getCountry(),
                dto.getAddressid()
        );
        if (list.size() == 0)
            throw new DatabaseException("[" + AddressDAO.class.toString() + "] Error updating address(id: " + dto.getAddressid() + ") in the Database");
        return list.get(0);
    }

    @Override
    protected AddressDTO createDTO(ResultSet set) throws DatabaseException {
        AddressDTO dto;
        try {
            dto = new AddressDTO();
            dto.setStreet(set.getString("street"));
            dto.setHousenumber((set.getString("housenumber")));
            dto.setPostalcode(set.getString("postalcode"));
            dto.setCity(set.getString("city"));
            dto.setCountry(set.getString("country"));
            dto.setAddressid(set.getInt("id"));
        } catch (SQLException e) {
            throw new DatabaseException("[" + AddressDAO.class.toString() + "] Error creating AddressDTO: " + e.getMessage());
        }
        return dto;
    }
}
