/**
 * @author Platzhalter 
 * @version 1.0 
 */

package com.macrosoft.collhbrs.model.dtos;

import com.macrosoft.collhbrs.services.utils.Utils;
import org.json.JSONObject;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Diese Klasse dient zur Definition des EducationDTO indem es vom @see UserDTO erbt und das @see SErializable implementiert
 */
public class EducationDTO implements Serializable, DTO {
    /**
     * Die Variablen dienen zur Speicherung der Daten (Zeitraum+Abschluss+Einrichtung) der Ausbildung
     */
    private LocalDate start;
    private LocalDate finish;
    private String degree, institution;
    private int educationid;

    public EducationDTO() {}

    public EducationDTO(LocalDate start, LocalDate finish, String degree, String institution, int educationid) {
        this.start = start;
        this.finish = finish;
        this.degree = degree;
        this.institution = institution;
        this.educationid = educationid;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getFinish() {
        return finish;
    }

    public void setFinish(LocalDate finish) {
        this.finish = finish;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public int getEducationid() {
        return educationid;
    }

    public void setEducationid(int educationid) {
        this.educationid = educationid;
    }

    @Override
    public String toString(){
        if (this.finish == null){
            return Utils.convertDateToString(this.start) + " bis Heute <br> Abschluss: " + this.degree + "<br> Institution: " + this.institution;
        }
        return Utils.convertDateToString(this.start) + " bis " + Utils.convertDateToString(this.finish) + "<br> Abschluss: " + this.degree + "<br> Institution: " + this.institution;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("degree", degree);
        obj.put("institution", institution);
        obj.put("start", start);
        obj.put("finish", finish);
        return obj;
    }
}
