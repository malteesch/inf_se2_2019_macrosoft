package com.macrosoft.collhbrs.model.dtos;

import java.io.Serializable;
import java.util.Date;

abstract class CandidatureDTO implements Serializable, DTO {

    private StudentDTO student;
    private VacancyDTO vacancy;
    private CompanyDTO company;
    private String coverletter;
    private Date canditaturedate;

    public StudentDTO getStudent() {
        return student;
    }

    public void setStudent(StudentDTO student) {
        this.student = student;
    }

    public VacancyDTO getVacancy() {
        return vacancy;
    }

    public void setVacancy(VacancyDTO vacancy) {
        this.vacancy = vacancy;
    }

    public CompanyDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyDTO company) {
        this.company = company;
    }

    public String getCoverletter() {
        return coverletter;
    }

    public void setCoverletter(String coverletter) {
        this.coverletter = coverletter;
    }

    public Date getCanditaturedate() {
        return canditaturedate;
    }

    public void setCanditaturedate(Date canditaturedate) {
        this.canditaturedate = canditaturedate;
    }
}
