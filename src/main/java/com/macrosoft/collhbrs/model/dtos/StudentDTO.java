/**
 * @author Platzhalter
 * @version 1.0
 */

package com.macrosoft.collhbrs.model.dtos;

import com.macrosoft.collhbrs.services.utils.Similarity;
import com.macrosoft.collhbrs.services.utils.Utils;
import org.json.JSONObject;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Diese Klasse dient zur Definition des StudentDTO mithilfe der Vererbung vom @see UserDTO und Implementierung des @see Serializable, DTO
 */
public class StudentDTO extends UserDTO implements Serializable, Comparable<StudentDTO> {

    private String name;
    private String familyname;
    private String major;
    private String universitylocation;
    private String job;
    private String employer;
    private String specalization;
    private String anrede;
    private LocalDate birthdate;
    private int studentid, semester;
    private DocumentDTO documents;

    private ArrayList<WorkDTO> work;
    private ArrayList<EducationDTO> education;
    private ArrayList<AwardDTO> award;
    private ArrayList<String> interests, skills;


    public String toString() {
        return "Name: " + this.name +
                "\n Nachname: " + this.familyname +
                "\n Major: " + this.major +
                "\n universitylocation: " + this.universitylocation +
                "\n job: " + this.job +
                "\n employer: " + this.employer +
                "\n specalization: " + this.specalization +
                "\n userid: " + this.getUserid() +
                "\n studentid: " + this.getStudentid() +
                "\n email: " + this.getEmail() +
                "\n Adresse: " + this.getAddress().toString();
    }

    public StudentDTO() {
    }

    public StudentDTO(UserDTO dto) {
        super(dto.getUsername(), dto.getEmail(), dto.getPassword());
    }

    public StudentDTO(String name, String familyname, String major, String unversitylocation, String job, String employer, String specalization, LocalDate birthdate, ArrayList<WorkDTO> work, ArrayList<EducationDTO> education, ArrayList<AwardDTO> award, int studentid, int semester, ArrayList<String> interests, ArrayList<String> skills, DocumentDTO documents) {
        this.name = name;
        this.familyname = familyname;
        this.major = major;
        this.universitylocation = unversitylocation;
        this.job = job;
        this.employer = employer;
        this.specalization = specalization;
        this.birthdate = birthdate;
        this.work = work;
        this.education = education;
        this.award = award;
        this.studentid = studentid;
        this.semester = semester;
        this.interests = interests;
        this.skills = skills;
        this.documents = documents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyname() {
        return familyname;
    }

    public void setFamilyname(String familyname) {
        this.familyname = familyname;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getUniversitylocation() {
        return universitylocation;
    }

    public void setUniversitylocation(String universitylocation) {
        this.universitylocation = universitylocation;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getSpecalization() {
        return specalization;
    }

    public void setSpecalization(String specalization) {
        this.specalization = specalization;
    }

    public String getAnrede() {
        return anrede;
    }

    public void setAnrede(String anrede) {
        this.anrede = anrede;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = LocalDate.from(birthdate);
    }

    public ArrayList<WorkDTO> getWork() {
        return work;
    }

    public void setWork(ArrayList<WorkDTO> work) {
        this.work = work;
    }

    public ArrayList<EducationDTO> getEducation() {
        return education;
    }

    public void setEducation(ArrayList<EducationDTO> education) {
        this.education = education;
    }

    public ArrayList<AwardDTO> getAward() {
        return award;
    }

    public void setAward(ArrayList<AwardDTO> award) {
        this.award = award;
    }

    public int getStudentid() {
        return studentid;
    }

    public void setStudentid(int studentid) {
        this.studentid = studentid;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public ArrayList<String> getInterests() {
        return interests;
    }

    public void setInterests(ArrayList<String> interests) {
        this.interests = interests;
    }

    public ArrayList<String> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<String> skills) {
        this.skills = skills;
    }

    public DocumentDTO getDocuments() {
        return documents;
    }

    public void setDocuments(DocumentDTO documents) {
        this.documents = documents;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("user", super.toJSON());
        obj.put("name", name);
        obj.put("familyname", familyname);
        obj.put("major", major);
        obj.put("universitylocation", universitylocation);
        obj.put("job", job);
        obj.put("employer", employer);
        obj.put("specialization", specalization);
        obj.put("birthday", birthdate != null ? birthdate.toString() : null);
        obj.put("studentid", studentid);
        obj.put("semester", semester);
        obj.put("work", Utils.listToJSONArray(work));
        obj.put("education", Utils.listToJSONArray(education));
        obj.put("awards", Utils.listToJSONArray(award));
        obj.put("skills", Utils.stringListToJSONArray(skills));
        obj.put("interests", Utils.stringListToJSONArray(interests));
        return obj;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof StudentDTO) {
            return this.studentid == ((StudentDTO) obj).studentid;
        } else if (obj instanceof UserDTO) {
            return super.equals(obj);
        }
        return false;
    }

    @Override
    public int compareTo(StudentDTO o) {
        return Similarity.getInstance().compare(this, o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
