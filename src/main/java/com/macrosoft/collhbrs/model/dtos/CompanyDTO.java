/**
 * @author Platzhalter
 * @version 1.0
 */

package com.macrosoft.collhbrs.model.dtos;

import org.json.JSONObject;

import java.io.Serializable;


/**
 * Diese Klasse dient zur Definition des CompanyDTO mithilfe der Vererbung vom @see UserDTO und Implementierung des @see Serializable, DTO
 */
public class CompanyDTO extends UserDTO implements Serializable, DTO {

    private String name, description, website;
    private int companyId;



    private double rating;

    public CompanyDTO() {
    }

    public CompanyDTO(UserDTO dto) {
        super(dto.getUsername(), dto.getEmail(), dto.getPassword());
    }

    public CompanyDTO(String name, String description, String website, int companyId) {
        this.name = name;
        this.description = description;
        this.website = website;
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public double getRating() { return rating; }

    public void setRating(double rating) { this.rating = rating; }
    @Override
    public String toString() {
        return companyId + ": " + name + ", " + description;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("user", super.toJSON());
        obj.put("name", name);
        obj.put("description", description);
        obj.put("website", website);
        return obj;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CompanyDTO)) {
            if (obj instanceof UserDTO) return super.equals(obj);
            return false;
        }
        return this.companyId == ((CompanyDTO) obj).companyId;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
