/**
 * @author Platzhalter
 * @version 1.0
 * <p>
 * Weist die Klasse dem Dtos-Package zu
 */

/**
 * Weist die Klasse dem Dtos-Package zu
 */

package com.macrosoft.collhbrs.model.dtos;

import com.macrosoft.collhbrs.services.utils.Utils;
import org.json.JSONObject;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Diese Klasse dient zur Definition des WorkDTO mithilfe der Vererbung vom @see UserDTO und Implementierung des @see Serializable, DTO
 */
public class WorkDTO implements Serializable, DTO {

    /**
     * Die Variablen dienen zur Speicherung der allgemeinen Arbeitsdaten, wie z.B. Beschreibung und Unternehmen
     */
    private LocalDate start;
    private LocalDate finish;
    private String description, company;
    private int workid, studentid;

    public WorkDTO() {}

    public WorkDTO(String username, String email, String password, LocalDate start, LocalDate finish, String description, String company, int workid, int studentid) {
        this.start = start;
        this.finish = finish;
        this.description = description;
        this.company = company;
        this.workid = workid;
        this.studentid = studentid;
    }

    /**
     * Diese Methode gibt den Startzeitpunkt des Objektes(WorkDTO) zurück
     */
    public LocalDate getStart() {
        return start;
    }

    /**
     * Diese Methode erlaubt den Startzeitpunkt anzupassen
     */
    public void setStart(LocalDate start) {
        this.start = start;
    }

    /**
     * Diese Methode gibt den Endzeitpunkt des Objektes(WorkDTO) zurück
     */
    public LocalDate getFinish() {
        return finish;
    }

    /**
     * Diese Methode erlaubt den Endzeitpunkt anzupassen
     */
    public void setFinish(LocalDate finish) {
        this.finish = finish;
    }

    /**
     * Diese Methode gibt die Beschreibung des Objektes(WorkDTO) zurück
     */
    public String getDescription() {
        return description;
    }

    /**
     * Diese Methode erlaubt die Beschreibung anzupassen
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Diese Methode gibt das Unternehmen des Objektes(CompanyDTO) zurück
     */
    public String getCompany() {
        return company;
    }

    /**
     * Diese Methode erlaubt das Unternehmen anzupassen
     */
    public void setCompany(String company) {
        this.company = company;
    }

    public int getWorkid() {
        return workid;
    }

    public void setWorkid(int workid) {
        this.workid = workid;
    }

    public int getStudentid() {
        return studentid;
    }

    public void setStudentid(int studentid) {
        this.studentid = studentid;
    }

    @Override
    public String toString() {
        if (this.finish == null){
            return Utils.convertDateToString(this.start) + " bis Heute <br> Firma: " + this.company + "<br> Tätigkeit: " + this.description;

        }
        return Utils.convertDateToString(this.start) + " bis " + Utils.convertDateToString(this.finish) + "<br> Firma: " + this.company + "<br> Tätigkeit: " + this.description;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("description", description);
        obj.put("company", company);
        obj.put("start", start);
        obj.put("finish", finish);
        return obj;
    }
}