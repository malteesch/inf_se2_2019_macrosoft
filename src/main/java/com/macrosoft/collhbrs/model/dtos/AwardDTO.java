package com.macrosoft.collhbrs.model.dtos;

import com.macrosoft.collhbrs.services.utils.Utils;
import org.json.JSONObject;

import java.io.Serializable;
import java.time.LocalDate;

public class AwardDTO implements Serializable, DTO {
    private LocalDate date;
    private String title;
    private int awardid, studentid;

    public AwardDTO(){}

    public AwardDTO(LocalDate date, String title, int awardid, int studentid) {
        this.date = LocalDate.from(date);
        this.title = title;
        this.awardid = awardid;
        this.studentid = studentid;
    }

    public LocalDate getDate() {
        return LocalDate.from(date);
    }

    public void setDate(LocalDate date) {
        this.date = LocalDate.from(date);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAwardid() {
        return awardid;
    }

    public void setAwardid(int awardid) {
        this.awardid = awardid;
    }

    public int getStudentid() {
        return studentid;
    }

    public void setStudentid(int studentid) {
        this.studentid = studentid;
    }

    @Override
    public String toString(){
        return Utils.convertDateToString(this.date) + "<br> Auszeichnung: " + this.title;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("date", date != null ? date.toString() : null);
        obj.put("title", title);
        return obj;
    }
}