package com.macrosoft.collhbrs.model.dtos;

import org.json.JSONObject;

import java.io.Serializable;
import java.time.LocalDate;

public class ApplicantDTO implements Serializable, DTO {

    private String studentName, studentFamilyname, vacancyTitle;
    private int studentId;
    private LocalDate applicationDate;

    public ApplicantDTO() {
    }

    public ApplicantDTO(String studentName, String studentFamilyname, String vacancyName, int studentId, LocalDate applicationDate) {
        this.studentName = studentName;
        this.studentFamilyname = studentFamilyname;
        this.vacancyTitle = vacancyName;
        this.studentId = studentId;
        this.applicationDate = applicationDate;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentFamilyname() {
        return studentFamilyname;
    }

    public void setStudentFamilyname(String studentFamilyname) {
        this.studentFamilyname = studentFamilyname;
    }

    public String getVacancyTitle() {
        return vacancyTitle;
    }

    public void setVacancyTitle(String vacancyTitle) {
        this.vacancyTitle = vacancyTitle;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public LocalDate getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(LocalDate applicationDate) {
        this.applicationDate = applicationDate;
    }

    @Override
    public String toString() {
        return "Studentname: " + getStudentName() + getStudentFamilyname() + ", Vacancytitle: " + getVacancyTitle() + " , ApplicationDate: " + getApplicationDate();
    }

    @Override
    public JSONObject toJSON() {
        return null;
    }
}
