/**
 * @author Platzhalter
 * @version 1.0
 * <p>
 * Weist die Klasse dem Dtos-Package zu
 */

/**
 * Weist die Klasse dem Dtos-Package zu
 */

package com.macrosoft.collhbrs.model.dtos;

import com.macrosoft.collhbrs.services.utils.Utils;
import org.json.JSONObject;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Diese Klasse dient zur Definition des VacancyDTO und implementiert des @see Serializable, DTO
 */
public class VacancyDTO implements Serializable, DTO {

    private String title, description;
    private CompanyDTO company;
    private int vacancyId;
    private LocalDate date;
    private List<String> tags, tasks, requirements, advantages;

    public VacancyDTO() {
        title = "";
        description = "";
        company = new CompanyDTO();
        tags = new ArrayList<>();
        tasks = new ArrayList<>();
        requirements = new ArrayList<>();
        advantages = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CompanyDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyDTO company) {
        this.company = company;
    }

    public int getVacancyId() {
        return vacancyId;
    }

    public void setVacancyId(int vacancyId) {
        this.vacancyId = vacancyId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getTasks() {
        return tasks;
    }

    public void setTasks(List<String> tasks) {
        this.tasks = tasks;
    }

    public List<String> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<String> requirements) {
        this.requirements = requirements;
    }

    public List<String> getAdvantages() {
        return advantages;
    }

    public void setAdvantages(List<String> advantages) {
        this.advantages = advantages;
    }

    @Override
    public String toString() {
        return "Title: " + this.getTitle();
    }

    @Override
    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("title", title);
        obj.put("description", description);
        obj.put("company", company.toJSON());
        obj.put("tags", Utils.stringListToJSONArray(tags));
        obj.put("tasks", Utils.stringListToJSONArray(tasks));
        obj.put("requirements", Utils.stringListToJSONArray(requirements));
        obj.put("advantages", Utils.stringListToJSONArray(advantages));
        return obj;
    }
}
