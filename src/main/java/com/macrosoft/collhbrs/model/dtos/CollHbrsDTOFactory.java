package com.macrosoft.collhbrs.model.dtos;

public class CollHbrsDTOFactory extends AbstractDTOFactory {
    @Override
    public CollHbrsCandidatureDTO createCandidatureDTO() {
        return new CollHbrsCandidatureDTO();
    }
}
