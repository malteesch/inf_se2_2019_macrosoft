package com.macrosoft.collhbrs.model.dtos;

import org.json.JSONObject;

import java.io.Serializable;
import java.time.LocalDate;

public class DocumentDTO implements Serializable, DTO {

    private int documentid, userid;
    private boolean visibility;
    private String documenttitle, documentdescription;
    private LocalDate documentupdate;
    private byte[] file;

    public DocumentDTO() {
    }

    public DocumentDTO(int documentid, int userid, boolean visibility, String documenttitle, String documentdescription, LocalDate documentupdate, byte[] file) {
        this.documentid = documentid;
        this.userid = userid;
        this.visibility = visibility;
        this.documenttitle = documenttitle;
        this.documentdescription = documentdescription;
        this.documentupdate = documentupdate;
        this.file = file.clone();
    }

    public int getDocumentid() {
        return documentid;
    }

    public void setDocumentid(int documentid) {
        this.documentid = documentid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }

    public String getDocumenttitle() {
        return documenttitle;
    }

    public void setDocumenttitle(String documenttitle) {
        this.documenttitle = documenttitle;
    }

    public String getDocumentdescription() {
        return documentdescription;
    }

    public void setDocumentdescription(String documentdescription) {
        this.documentdescription = documentdescription;
    }

    public LocalDate getDocumentupdate() {
        return LocalDate.from(documentupdate);
    }

    public void setDocumentupdate(LocalDate documentupdate) {
        this.documentupdate = LocalDate.from(documentupdate);
    }

    public byte[] getFile() {
        return this.file != null ? this.file.clone() : null;
    }

    public void setFile(byte[] file) {
        this.file = file != null ? file.clone() : null;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("title", documenttitle);
        obj.put("description", documentdescription);
        obj.put("date", documentupdate != null ? documentupdate.toString() : null);
        return obj;
    }
}
