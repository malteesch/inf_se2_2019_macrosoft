/**
 * @author Platzhalter
 * @version 1.0
 */

package com.macrosoft.collhbrs.model.dtos;

import com.macrosoft.collhbrs.services.utils.BCryptUtil;
import org.json.JSONObject;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Diese Klasse dient zur Definition des UserDTO durch die Implementierung des @see Serializable, DTO
 */
public class UserDTO implements Serializable, DTO {

    private String username, email, password;
    private int userid, addressid;
    private byte[] image;
    private AddressDTO address;
    private LocalDate registrationdate;

    public UserDTO() {
    }

    public UserDTO(String username, String email, String password, int userid, byte[] image, AddressDTO address) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.userid = userid;
        this.image = image.clone();
        this.address = address;
    }

    public UserDTO(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = BCryptUtil.hash(password);
    }

    public void setPasswordAlreadyHashed(String password) {
        this.password = password;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public byte[] getImage() {
        return image != null ? image.clone() : null;
    }

    public void setImage(byte[] image) {
        if (image == null) return;
        this.image = image.clone();
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public LocalDate getRegistrationdate() {
        return registrationdate;
    }

    public void setRegistrationdate(LocalDate registrationdate) {
        this.registrationdate = LocalDate.from(registrationdate);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserDTO) {
            return ((UserDTO) obj).username.equals(this.username) && ((UserDTO) obj).email.equals(email);
        }
        return false;
    }

    public int getAddressid() {
        return addressid;
    }

    public void setAddressid(int addressid) {
        this.addressid = addressid;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("username", username);
        obj.put("email", email);
        obj.put("registrationDate", registrationdate);
        obj.put("address", address != null ? address.toJSON() : null);
        return obj;
    }
}
