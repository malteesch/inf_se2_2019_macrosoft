/**
 * @author Platzhalter 
 * @version 1.0 
 */
 
/**
 * Weist die Klasse dem Dtos-Package zu
 */
 
package com.macrosoft.collhbrs.model.dtos;

/**
 * Diese Klasse dient zur Definition des UserLoginDTO mithilfe der Vererbung vom @see UserDTO
 */
public class UserLoginDTO extends UserDTO {

    private String email, password;

    public UserLoginDTO(String login, String password) {
        this.email = login;
        this.password = password;
    }

    public String getLogin() {
        return email;
    }
    public String getPassword() {
        return password;
    }
}
