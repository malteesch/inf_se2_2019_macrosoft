/**
 * @author Platzhalter
 * @version 1.0
 */

package com.macrosoft.collhbrs.model.dtos;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Diese Klasse dient zur Definition des AdressDTO
 */
public class AddressDTO implements Serializable, DTO {
    /**
     * Die Variablen dienen zur Speicherung der Adressdaten wie z.b. Straßenname
     */
    private String street, housenumber, postalcode, city, country;
    private Integer addressid;

    public AddressDTO(String street, String housenumber, String postalcode, String city, String country) {
        this.street = street;
        this.housenumber = housenumber;
        this.postalcode = postalcode;
        this.city = city;
        this.country = country;
    }

    public AddressDTO() {

    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getAddressid() {
        return addressid;
    }

    public void setAddressid(int addressid) {
        this.addressid = addressid;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("strees", street);
        obj.put("housenumber", housenumber);
        obj.put("postalcode", postalcode);
        obj.put("city", city);
        obj.put("country", country);
        return obj;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AddressDTO)) return false;
        AddressDTO dto = (AddressDTO) obj;
        return this.street.equals(dto.street)
                && this.housenumber.equals(dto.housenumber)
                && this.postalcode.equals(dto.postalcode)
                && this.city.equals(dto.city)
                && this.country.equals(dto.country);
    }
}
