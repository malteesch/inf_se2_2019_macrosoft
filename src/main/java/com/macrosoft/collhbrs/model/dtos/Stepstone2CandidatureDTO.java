package com.macrosoft.collhbrs.model.dtos;

import org.json.JSONObject;

import java.util.Date;

public class Stepstone2CandidatureDTO extends CandidatureDTO {

    Stepstone2CandidatureDTO() {
    }

    @Override
    public String toString() {
        return toJSON().toString();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    @Override
    public void setStudent(StudentDTO student) {
        super.setStudent(student);
    }

    @Override
    public void setVacancy(VacancyDTO vacancy) {
        super.setVacancy(vacancy);
    }

    @Override
    public void setCompany(CompanyDTO company) {
        super.setCompany(company);
    }

    @Override
    public void setCoverletter(String coverletter) {
        super.setCoverletter(coverletter);
    }

    @Override
    public void setCanditaturedate(Date canditaturedate) {
        super.setCanditaturedate(canditaturedate);
    }

    @Override
    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("student", getStudent() != null ? getStudent().toJSON() : "");
        obj.put("vacancy", getVacancy() != null ? getVacancy().toJSON() : "");
        obj.put("company", getCompany() != null ? getCompany().toJSON() : "");
        obj.put("coverletter", getCoverletter());
        obj.put("candidatureDate", getCanditaturedate() != null ? getCanditaturedate() : "");
        return obj;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
