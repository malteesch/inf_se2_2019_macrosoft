package com.macrosoft.collhbrs.model.dtos;

public abstract class AbstractDTOFactory {

    public abstract CandidatureDTO createCandidatureDTO();
}
