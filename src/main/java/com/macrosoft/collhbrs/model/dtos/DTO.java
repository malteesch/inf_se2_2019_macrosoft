/**
 * @author Platzhalter 
 * @version 1.0 
 */
 
/**
 * Weist die Klasse dem Dtos-Package zu
 */

package com.macrosoft.collhbrs.model.dtos;

import org.json.JSONObject;

public interface DTO {

    JSONObject toJSON();
}
