package com.macrosoft.collhbrs.gui.ui;

import com.macrosoft.collhbrs.gui.views.*;
import com.macrosoft.collhbrs.services.utils.Config;
import com.vaadin.annotations.*;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

import javax.servlet.annotation.WebServlet;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
@Title("Coll@HBRS")
@PreserveOnRefresh()
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        Navigator navi = new Navigator(this, this);

        navi.addView(Config.Views.MAIN, MainView.class);
        navi.addView(Config.Views.LOGIN, LoginView.class);
        navi.addView(Config.Views.REGISTER, RegisterView.class);
        navi.addView(Config.Views.SEARCH, SearchView.class);
        navi.addView(Config.Views.PROFILE, ProfileView.class);
        navi.addView(Config.Views.CREATE_VACANCY, CreateVacancyView.class);
        navi.addView(Config.Views.VACANCY_DETAILS, VacancyDetailsView.class);
        navi.addView(Config.Views.APPLICATION_VIEW, ApplicationView.class);
        navi.addView(Config.Views.HELP_VIEW, HelpView.class);

        UI.getCurrent().getNavigator().navigateTo(Config.Views.LOGIN);
   }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}

