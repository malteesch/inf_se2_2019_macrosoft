package com.macrosoft.collhbrs.gui.components;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.VacancyDetailsControl;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.control.toggle.ToggleRouter;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.macrosoft.collhbrs.services.utils.Session;
import com.macrosoft.collhbrs.services.utils.Utils;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class VacancyDetailsComponent extends VerticalLayout {

    public VacancyDetailsComponent(VacancyDTO vDTO) throws SQLException, DatabaseException, UserException {
        this.setSizeFull();
        this.setSpacing(true);
        this.addStyleName("vacancy-components");

        GridLayout headerLayout = new GridLayout(4, 6);
        headerLayout.setWidth("80%");
        headerLayout.setColumnExpandRatio(0, 2.5F);
        headerLayout.setColumnExpandRatio(1, 6F);
        headerLayout.setColumnExpandRatio(2, 0.5F);
        headerLayout.setColumnExpandRatio(3, 0.1F);
        this.addComponent(headerLayout);
        this.setComponentAlignment(headerLayout, Alignment.MIDDLE_CENTER);

        Label title = new Label(vDTO.getTitle());
        title.addStyleName(MaterialTheme.LABEL_H4);
        title.setWidth("100%");
        headerLayout.addComponent(title, 0, 0, 1, 0);

        Label location = new Label(VaadinIcons.MAP_MARKER.getHtml() + vDTO.getCompany().getAddress().getCity()
                + ", " + vDTO.getCompany().getAddress().getCountry(), ContentMode.HTML);
        location.setWidth("100%");
        headerLayout.addComponent(location, 0, 1);

        Label date = new Label(VaadinIcons.CALENDAR.getHtml() + Utils.convertDateToString(vDTO.getDate()), ContentMode.HTML);
        date.setWidth("100%");
        headerLayout.addComponent(date, 1, 1);

        if (vDTO.getTags().size() > 0) {
            StringBuilder builder = new StringBuilder(vDTO.getTags().get(0));
            for (int i = 1; i < vDTO.getTags().size(); ++i) {
                builder.append(", ").append(vDTO.getTags().get(i));
            }
            String tags = builder.toString();
            Label tagsLabel = new Label(VaadinIcons.TAGS.getHtml() + tags, ContentMode.HTML);
            tagsLabel.setWidth("100%");
            headerLayout.addComponent(tagsLabel, 0, 2, 3, 2);
        }

        if (Session.getCurrentRole().equals(Config.Role.STUDENT)) {
            if (ToggleRouter.featureIsEnabled(Config.Features.APPLY)) {
                StudentDAO studentDAO = new StudentDAO();
                StudentDTO studentDTO = studentDAO.getOne(Session.getCurrentUser().getEmail());
                Button apply = new Button("One-Klick Bewerbung", (Button.ClickListener) click -> {
                    String y = "";
                    try {
                        boolean x = VacancyDetailsControl.createOneClickApplication(vDTO.getVacancyId(), studentDTO.getStudentid());
                        if (x) {
                            y = "Sie haben sich erfolgreich auf die Stelle beworben!";
                        } else {
                            y = "Sie haben sich bereits auf diese Stelle beworben!";
                        }
                    } catch (DatabaseException | SQLException e) {
                        new Notifier().createWarningNotification(y)
                                .at(Position.TOP_CENTER)
                                .show();
                        e.printStackTrace();
                    }
                });
                apply.addStyleName(MaterialTheme.BUTTON_PRIMARY);
                headerLayout.addComponent(apply, 2, 0);
                headerLayout.setComponentAlignment(apply, Alignment.MIDDLE_RIGHT);

                Button fav = VacancyDetailsControl.createFavButton(vDTO);
                headerLayout.addComponent(fav, 3, 0);
                headerLayout.setComponentAlignment(fav, Alignment.MIDDLE_RIGHT);
            }
        }

        GridLayout vacanyBody = new GridLayout(2, 3);
        vacanyBody.setWidth("80%");
        vacanyBody.setSpacing(true);
        this.addComponent(vacanyBody);
        this.setComponentAlignment(vacanyBody, Alignment.MIDDLE_CENTER);

        Panel descriptionPanel;
        if (vDTO.getDescription().isEmpty()) {
            descriptionPanel = createPanel("Das Unternehmen hat keine Beschreibung eingetragen", "Beschreibung");
        } else {
            descriptionPanel = createPanel(vDTO.getDescription(), "Beschreibung");
        }
        descriptionPanel.setWidth("100%");
        vacanyBody.addComponent(descriptionPanel, 0, 0, 1, 0);


        Panel tasksPanel;
        if (vDTO.getTasks().size() < 1) {
            tasksPanel = createPanel("Das Unternehmen hat keine Aufgaben eingetragen", "Deine Aufgaben");
        } else {
            tasksPanel = createPanel(vDTO.getTasks(), "Deine Aufgaben");
        }
        tasksPanel.setWidth("100%");
        vacanyBody.addComponent(tasksPanel, 0, 1);


        Panel skillsPanel;
        if (vDTO.getRequirements().size() < 1) {
            skillsPanel = createPanel("Das Unternehmen hat keine Fähigkeiten eingetragen", "Deine Fähigkeiten");
        } else {
            skillsPanel = createPanel(vDTO.getRequirements(), "Deine Fähigkeiten");
        }
        skillsPanel.setWidth("100%");
        vacanyBody.addComponent(skillsPanel, 1, 1);


        Panel benefitPanel;
        if (vDTO.getAdvantages().size() < 1) {
            benefitPanel = createPanel("Das Unternehmen hat keine Vorteile eingetragen", "Deine Vorteile");
        } else {
            benefitPanel = createPanel(vDTO.getAdvantages(), "Deine Vorteile");
        }
        benefitPanel.setWidth("100%");
        vacanyBody.addComponent(benefitPanel, 0, 2, 1, 2);
    }

    private <T> Panel createPanel(T content, String title) {
        Panel panel = new Panel();
        panel.addStyleName("vacancydetails-panel");
        VerticalLayout layout = new VerticalLayout();
        Label l = new Label(title);
        l.setWidth("100%");
        l.addStyleName(MaterialTheme.LABEL_BOLD);
        layout.addComponent(l);

        if (content instanceof String) {
            Label lcontent = new Label((String) content);
            lcontent.setWidth("100%");
            layout.addComponent(lcontent);
        } else if (content instanceof List) {
            for (String inter : (ArrayList<String>) content) {
                Label l1 = new Label(VaadinIcons.ARROWS_LONG_RIGHT.getHtml() + " " + inter, ContentMode.HTML);
                l1.setWidth("100%");
                layout.addComponent(l1);
            }
        }

        panel.setContent(layout);
        return panel;
    }
}
