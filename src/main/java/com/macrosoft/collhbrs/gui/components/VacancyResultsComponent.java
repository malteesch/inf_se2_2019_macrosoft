package com.macrosoft.collhbrs.gui.components;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.ProfileControl;
import com.macrosoft.collhbrs.control.VacancyDetailsControl;
import com.macrosoft.collhbrs.control.toggle.ToggleRouter;
import com.macrosoft.collhbrs.gui.views.ProfileView;
import com.macrosoft.collhbrs.model.dtos.DTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.macrosoft.collhbrs.services.utils.Session;
import com.macrosoft.collhbrs.services.utils.Utils;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;

import java.util.ArrayList;

public class VacancyResultsComponent extends HorizontalLayout {

    static VerticalLayout detailsLayout = new VerticalLayout();

    public VacancyResultsComponent(ArrayList<VacancyDTO> list, String title) throws Exception {
        this.setWidth("100%");
        this.addStyleName("search-components");

        GridLayout vacancyResults = new GridLayout(2, 1);
        vacancyResults.setWidth("80%");
        vacancyResults.setColumnExpandRatio(0, 4F);
        vacancyResults.setColumnExpandRatio(1, 6F);
        this.addComponent(vacancyResults);
        this.setComponentAlignment(vacancyResults, Alignment.MIDDLE_CENTER);

        VerticalLayout vacancyLayout = new VerticalLayout();
        vacancyResults.addComponent(vacancyLayout);

        Label vacancyHeader = new Label(title + " (" + list.size() + ")");
        vacancyHeader.addStyleName(MaterialTheme.LABEL_H4);
        vacancyLayout.addComponent(vacancyHeader);

        for (VacancyDTO dto : list) {
            vacancyLayout.addComponent(new HorizontalCardsComponent<>(dto));
        }

        vacancyResults.addComponent(detailsLayout);
        detailsLayout.setVisible(false);
        detailsLayout.setWidth("100%");
    }

    static <T extends DTO> Panel createDetailsCard(T dto) throws Exception {
        Panel p = new Panel();
        Image img;
        Label title;
        Button detailsButton = new Button("Details");

        GridLayout all = new GridLayout(1, 2);

        all.setWidth("100%");

        GridLayout details = new GridLayout(4, 7);
        details.addStyleName("gridlayout-detailscard");
        details.setWidth("100%");
        details.setColumnExpandRatio(0, 1F);
        details.setColumnExpandRatio(1, 9F);
        all.addComponent(details, 0, 0);

        if (dto instanceof VacancyDTO) {
            VacancyDTO vacancy = (VacancyDTO) dto;

            Label detailsHeader = new Label("Stellendetails");
            detailsHeader.addStyleName(MaterialTheme.LABEL_H4);
            detailsLayout.addComponent(detailsHeader);

            if (vacancy.getCompany().getImage().length > 0) {
                img = Utils.convertToImage(vacancy.getCompany().getImage());
            } else {
                ThemeResource resource = new ThemeResource("img/no-image-placeholder.jpg");
                img = new Image(null, resource);
            }

            title = new Label(vacancy.getTitle());

            if (Session.getCurrentRole().equals(Config.Role.STUDENT)) {
                if (ToggleRouter.featureIsEnabled(Config.Features.APPLY)) {
                    Button fav = VacancyDetailsControl.createFavButton(vacancy);
                    details.addComponent(fav, 2, 0, 2, 1);
                }
            }

            Label companyName = new Label(VaadinIcons.INFO_CIRCLE.getHtml() + vacancy.getCompany().getName(), ContentMode.HTML);
            details.addComponent(companyName, 1, 1);

            Label date = new Label(VaadinIcons.CALENDAR.getHtml() + Utils.convertDateToString(vacancy.getDate()), ContentMode.HTML);
            details.addComponent(date, 1, 2);

            Label descTitle = new Label("Beschreibung");
            descTitle.addStyleName(MaterialTheme.LABEL_BOLD);
            details.addComponent(descTitle, 0, 4);

            Label description = new Label(vacancy.getDescription());
            description.setWidth("100%");
            details.addComponent(description, 0, 5, 2, 5);

            detailsButton.addClickListener((Button.ClickListener) click -> {
                try {
                    VacancyDetailsControl.showVacancy(vacancy);
                } catch (Exception e) {
                    new Notifier().createErrorNotification("Die gewünschte Stelle konnte nicht gefunden werden! Bitte kontaktieren Sie den Administrator")
                            .at(Position.TOP_CENTER)
                            .show();
                    e.printStackTrace();
                }
            });
        } else {
            StudentDTO student = (StudentDTO) dto;
            if (student.getImage() != null && student.getImage().length > 0) {
                img = Utils.convertToImage(student.getImage());
            } else {
                ThemeResource resource = new ThemeResource("img/Portrait_Placeholder.png");
                img = new Image(null, resource);
            }

            title = new Label(student.getName() + " " + student.getFamilyname());

            Button decline = new Button("Ablehnen", (Button.ClickListener) click -> {
                //TODO
            });
            decline.addStyleName(MaterialTheme.BUTTON_TINY);
            decline.addStyleName(MaterialTheme.BUTTON_DANGER);
            details.addComponent(decline, 2, 0);


            Button accept = new Button("Annehmen", (Button.ClickListener) click -> {
                //TODO Malte
            });
            accept.addStyleName(MaterialTheme.BUTTON_FRIENDLY);
            accept.addStyleName(MaterialTheme.BUTTON_TINY);
            details.addComponent(accept, 3, 0);

            Label major = new Label(VaadinIcons.INFO_CIRCLE.getHtml() + student.getMajor() + " (" + student.getSpecalization() + ")", ContentMode.HTML);
            details.addComponent(major, 1, 1, 3, 1);

            Label date = new Label(VaadinIcons.CALENDAR.getHtml() + Utils.convertDateToString(student.getBirthdate()), ContentMode.HTML);
            details.addComponent(date, 1, 2, 3, 2);

            Label email = new Label(VaadinIcons.ENVELOPE.getHtml() + student.getEmail(), ContentMode.HTML);
            details.addComponent(email, 1, 3, 3, 3);

            detailsButton.addClickListener((Button.ClickListener) click -> {
                ProfileView.setStudent(student);
                ProfileView.setMyProfile(false);
                ProfileControl.showStudent();
            });
        }

        img.setWidth("150px");
        details.addComponent(img, 0, 0, 0, 3);

        title.setWidth("100%");
        title.addStyleName(MaterialTheme.LABEL_BOLD);
        details.addComponent(title, 1, 0);

        detailsButton.setWidth("100%");
        all.addComponent(detailsButton, 0, 1);

        p.setContent(all);
        return p;
    }
}
