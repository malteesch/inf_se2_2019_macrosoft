package com.macrosoft.collhbrs.gui.components;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.LiveSearchControl;
import com.macrosoft.collhbrs.control.LoginControl;
import com.macrosoft.collhbrs.control.ProfileControl;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.toggle.ToggleRouter;
import com.macrosoft.collhbrs.gui.views.LoginView;
import com.macrosoft.collhbrs.gui.views.ProfileView;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.event.MouseEvents;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;

import java.util.Optional;

public class NavbarComponent extends HorizontalLayout {

    public NavbarComponent() throws DatabaseException {
        this.setWidth("100%");
        this.addStyleName("darkLayout-navbar");

        GridLayout navbar = new GridLayout(5, 1);
        navbar.setSizeFull();
        navbar.setColumnExpandRatio(0, 0.2f);
        navbar.setColumnExpandRatio(1, 7);
        navbar.setColumnExpandRatio(2, 0.1f);
        navbar.setColumnExpandRatio(3, 0.1f);
        navbar.setColumnExpandRatio(4, 0.1f);

        Image logo = createLogo();
        navbar.addComponent(logo, 0, 0);
        navbar.setComponentAlignment(logo, Alignment.MIDDLE_LEFT);

        if (Session.isLoggedIn()) {
            if (Session.getCurrentRole().equals(Config.Role.STUDENT)) {
                if (!(UI.getCurrent().getNavigator().getCurrentView().getClass().toString().toLowerCase().contains(Config.Views.MAIN.toLowerCase()))) {
                    ComboBox<String> searchbox = LiveSearchControl.createLiveSearch();
                    searchbox.addShortcutListener(new ShortcutListener("", ShortcutAction.KeyCode.ENTER, null) {
                        @Override
                        public void handleAction(Object o, Object o1) {
                            searchbox.setNewItemProvider(input -> {
                                LiveSearchControl.comboboxSearch(input);
                                return Optional.empty();
                            });

                        }
                    });
                    navbar.addComponent(searchbox, 1, 0);
                    navbar.setComponentAlignment(searchbox, Alignment.MIDDLE_CENTER);
                }
            } else {
                Button advertiseButton = createAdvertiseButton();
                navbar.addComponent(advertiseButton, 1, 0);
                navbar.setComponentAlignment(advertiseButton, Alignment.MIDDLE_RIGHT);
            }
            MenuBar menuBar = createMenuBar();
            navbar.addComponent(menuBar, 2, 0, 4, 0);
            navbar.setComponentAlignment(menuBar, Alignment.MIDDLE_RIGHT);
        } else {
            MenuBar menuBar = new MenuBar();
            menuBar.addStyleName(MaterialTheme.MENUBAR_CUSTOM);
            menuBar.addStyleName(MaterialTheme.MENUBAR_BORDERLESS);
            MenuBar.MenuItem help = menuBar.addItem("Hilfe", clickEvent ->
                    UI.getCurrent().getNavigator().navigateTo(Config.Views.HELP_VIEW));
            help.setIcon(VaadinIcons.QUESTION);
            navbar.addComponent(menuBar, 3, 0);
            navbar.setComponentAlignment(menuBar, Alignment.MIDDLE_RIGHT);

            Button buttonRegister = new Button();
            buttonRegister.addStyleName(MaterialTheme.BUTTON_CUSTOM);
            navbar.addComponent(buttonRegister, 4, 0);
            navbar.setComponentAlignment(buttonRegister, Alignment.MIDDLE_RIGHT);
            if(UI.getCurrent().getNavigator().getCurrentView() instanceof LoginView) {
                buttonRegister.addClickListener((Button.ClickListener) event -> UI.getCurrent().getNavigator().navigateTo(Config.Views.REGISTER));
                buttonRegister.setCaption("Registrierung");
            }
            else{
                buttonRegister.addClickListener((Button.ClickListener) event -> UI.getCurrent().getNavigator().navigateTo(Config.Views.LOGIN));
                buttonRegister.setCaption("Login");
            }
        }
        this.addComponent(navbar);
    }

    private Image createLogo() {
        ThemeResource resource = new ThemeResource("img/colllogo1.png");
        Image logo = new Image(null, resource);
        logo.setWidth("250px");
        logo.addStyleName("logo");
        logo.addClickListener((MouseEvents.ClickListener) event -> UI.getCurrent().getNavigator().navigateTo(Config.Views.MAIN));
        return logo;
    }

    private MenuBar createMenuBar() {
        MenuBar menuBar = new MenuBar();
        menuBar.addStyleName(MaterialTheme.MENUBAR_CUSTOM);
        menuBar.addStyleName(MaterialTheme.MENUBAR_BORDERLESS);

        MenuBar.Command userProfile = clickEvent -> {
            ProfileView.setMyProfile(true);
            ProfileControl.showStudent();
        };

        MenuBar.Command companyProfile = clickEvent -> {
            ProfileView.setMyProfile(true);
            ProfileControl.showCompany();
        };

        if (Session.getCurrentRole().equals(Config.Role.STUDENT)) {
            MenuBar.MenuItem profile = menuBar.addItem("Mein Profil", userProfile);
            profile.setIcon(VaadinIcons.USER);
        } else {
            MenuBar.MenuItem profile = menuBar.addItem("Mein Profil", companyProfile);
            profile.setIcon(VaadinIcons.USER);
        }

        if (ToggleRouter.featureIsEnabled(Config.Features.APPLY)) {
            String s = "Stellen";
            if (Session.getCurrentRole().equals(Config.Role.STUDENT)) {
                s = "Bewerbungen";
            }

            MenuBar.MenuItem cand = menuBar.addItem(s, clickEvent ->
                    UI.getCurrent().getNavigator().navigateTo(Config.Views.APPLICATION_VIEW));
            cand.setIcon(VaadinIcons.FILE_TEXT);
        }

        MenuBar.MenuItem help = menuBar.addItem("Hilfe", clickEvent ->
                UI.getCurrent().getNavigator().navigateTo(Config.Views.HELP_VIEW));
        help.setIcon(VaadinIcons.QUESTION);

        MenuBar.MenuItem logout = menuBar.addItem("Logout", clickEvent -> LoginControl.logoutUser());
        logout.setIcon(VaadinIcons.SIGN_OUT);

        return menuBar;
    }

    private Button createAdvertiseButton() {
        Button advertiseButton = new Button("Inserieren", (Button.ClickListener) event -> {
            UI.getCurrent().getNavigator().navigateTo(Config.Views.CREATE_VACANCY);
        });
        advertiseButton.addStyleName(MaterialTheme.BUTTON_CUSTOM);
        return advertiseButton;
    }
}
