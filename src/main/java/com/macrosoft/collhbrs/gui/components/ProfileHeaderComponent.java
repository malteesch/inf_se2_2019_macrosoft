package com.macrosoft.collhbrs.gui.components;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.ProfileControl;
import com.macrosoft.collhbrs.control.exceptions.NotRatedException;
import com.macrosoft.collhbrs.gui.views.ProfileView;
import com.macrosoft.collhbrs.gui.windows.EditCompanyWindow;
import com.macrosoft.collhbrs.gui.windows.EditStudentWindow;
import com.macrosoft.collhbrs.gui.windows.NachrichtWindow;
import com.macrosoft.collhbrs.gui.windows.RateWindow;
import com.macrosoft.collhbrs.model.dtos.AddressDTO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.macrosoft.collhbrs.services.utils.Session;
import com.macrosoft.collhbrs.services.utils.Utils;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;

public class ProfileHeaderComponent extends VerticalLayout {

    private GridLayout profileHeaderGridLayout = new GridLayout(4, 5);


    public ProfileHeaderComponent(StudentDTO student) {
        setUp(student);
    }

    public ProfileHeaderComponent(CompanyDTO company) {
        setUp(company);
    }

    private <T extends UserDTO> void setUp(T dto) {
        this.setWidth("100%");
        this.addStyleName("profile-components");
        this.addStyleNames("profileheader-components");
        this.addComponent(profileHeaderGridLayout);
        this.setComponentAlignment(profileHeaderGridLayout, Alignment.MIDDLE_CENTER);
        String current = UI.getCurrent().getNavigator().getCurrentView().getClass().toString().toLowerCase();

        profileHeaderGridLayout.setWidth("80%");

        Image profileImage;
        if (dto.getImage() != null && dto.getImage().length > 0) {
            profileImage = Utils.convertToImage(dto.getImage());
        } else {
            ThemeResource resource = new ThemeResource("img/Portrait_Placeholder.png");
            profileImage = new Image("", resource);
        }
        profileImage.addStyleName("profileImages");

        AddressDTO address;
        Label nameLabel;
        Label locationLabel;

        if (dto instanceof StudentDTO) {

            StudentDTO student = (StudentDTO) dto;
            address = student.getAddress();

            Label captionAboutMe = createLabel("Über mich");
            captionAboutMe.addStyleName(MaterialTheme.LABEL_H4);
            Label university = createLabel("Campus: " + student.getUniversitylocation(), VaadinIcons.ACADEMY_CAP);
            Label major = createLabel(student.getMajor(), VaadinIcons.CALC_BOOK);
            Label employer = createLabel(student.getJob() + " bei " + student.getEmployer(), VaadinIcons.OFFICE);
            Label specialization = createLabel("Schwerpunkt: " + student.getSpecalization(), VaadinIcons.BULLSEYE);

            nameLabel = createLabel(student.getName() + " " + student.getFamilyname());
            if (address != null) {
                locationLabel = createLabel(address.getCity() + ", " + address.getCountry(), VaadinIcons.MAP_MARKER);
                profileHeaderGridLayout.addComponent(locationLabel, 2, 3);

            }

            profileHeaderGridLayout.addComponent(major, 1, 1);
            profileHeaderGridLayout.addComponent(university, 1, 2);
            profileHeaderGridLayout.addComponent(captionAboutMe, 2, 0);
            profileHeaderGridLayout.addComponent(employer, 2, 1);
            profileHeaderGridLayout.addComponent(specialization, 2, 2);

            if (ProfileView.getMyProfile()) {
                createEditButton(student);
            }
        } else {
            CompanyDTO company = (CompanyDTO) dto;
            address = company.getAddress();

            Link website = new Link(company.getWebsite(), new ExternalResource("http://" + company.getWebsite()));
            website.setIcon(VaadinIcons.GLOBE_WIRE);
            Label description = createLabel(company.getDescription(), VaadinIcons.INFO_CIRCLE_O);

            nameLabel = createLabel(company.getName());
            locationLabel = createLabel(address.getCity() + ", " + address.getCountry(), VaadinIcons.MAP_MARKER);

            System.out.println(current);
            System.out.println(Config.Views.VACANCY_DETAILS);
            System.out.println(Config.Views.SEARCH);

            if (current.contains(Config.Views.SEARCH.toLowerCase()) || current.contains(Config.Views.VACANCY_DETAILS.toLowerCase())) {
                Button nameButton = createCompanyTitleButton(company);
                profileHeaderGridLayout.addComponent(nameButton, 1, 0);
            }

            profileHeaderGridLayout.addComponent(locationLabel, 1, 2);
            profileHeaderGridLayout.addComponent(website, 1, 3);
            profileHeaderGridLayout.addComponent(description, 1, 4, 3, 4);

            if (ProfileView.getMyProfile()) {
                createEditButton(company);
            } else {
                createContactButton();
            }
            createRateButton(company);
            createRatingLabel(company);
        }

        profileHeaderGridLayout.addComponent(profileImage, 0, 0, 0, 4);
        profileHeaderGridLayout.setComponentAlignment(profileImage, Alignment.TOP_CENTER);

        if (!(current.contains(Config.Views.VACANCY_DETAILS.toLowerCase())) && !(current.contains(Config.Views.SEARCH.toLowerCase()))) {
            profileHeaderGridLayout.addComponent(nameLabel, 1, 0);
            nameLabel.addStyleName(MaterialTheme.LABEL_H4);
        }

        profileHeaderGridLayout.setColumnExpandRatio(0, 0.25F);
        profileHeaderGridLayout.setColumnExpandRatio(1, 4F);
        profileHeaderGridLayout.setColumnExpandRatio(2, 4F);
        profileHeaderGridLayout.setColumnExpandRatio(3, 1F);

        setComponentAlignmentMiddleLeft();
    }

    private void setComponentAlignmentMiddleLeft() {
        for (Component comp : profileHeaderGridLayout) {
            if (!(comp instanceof Button) && !(comp instanceof Image)) {
                profileHeaderGridLayout.setComponentAlignment(comp, Alignment.MIDDLE_LEFT);
            }
        }
    }

    private Label createLabel(String s_label) {
        Label l_label = new Label(s_label);
        l_label.setWidth("100%");
        return l_label;
    }

    private Label createLabel(String s_label, VaadinIcons icon) {
        Label l_label = new Label(icon.getHtml() + "&nbsp;" + s_label, ContentMode.HTML);
        l_label.setWidth("100%");
        return l_label;
    }

    private void createEditButton(UserDTO dto) {
        Button edit = new Button(VaadinIcons.PENCIL);
        edit.addStyleName(MaterialTheme.BUTTON_BORDERLESS);
        edit.addStyleName(MaterialTheme.BUTTON_TINY);
        profileHeaderGridLayout.addComponent(edit, 3, 0);
        profileHeaderGridLayout.setComponentAlignment(edit, Alignment.MIDDLE_RIGHT);
        edit.addClickListener((Button.ClickListener) event -> {
            if (dto instanceof StudentDTO) {
                StudentDTO student = (StudentDTO) dto;
                EditStudentWindow w = new EditStudentWindow(student);
                UI.getCurrent().addWindow(w);
                if(!w.isVisible()){
                    w.setVisible(true);
                }
            } else {
                CompanyDTO company = (CompanyDTO) dto;
                try {
                    EditCompanyWindow w = new EditCompanyWindow(company);
                    UI.getCurrent().addWindow(w);
                    if (!w.isVisible()) {
                        w.setVisible(true);
                    }
                } catch (Exception e) {
                    new Notifier().createErrorNotification("Das Fenster zur Profilbearbeitung konnte nicht geöffnet werden. Bitte wenden Sie sich an den Administrator.")
                            .at(Position.TOP_CENTER)
                            .show();
                    e.printStackTrace();
                }
            }
        });
    }

    private  void createRateButton(CompanyDTO dto){
        Button rate = new Button("Bewertung Abgeben",VaadinIcons.STAR);
        rate.addStyleNames(MaterialTheme.BUTTON_BORDERLESS,MaterialTheme.BUTTON_TINY);
        profileHeaderGridLayout.addComponent(rate,3,1);
        profileHeaderGridLayout.setComponentAlignment(rate,Alignment.MIDDLE_RIGHT);
        if(Session.getCurrentRole().equals(Config.Role.COMPANY)){
            rate.setEnabled(false);
        }
        rate.addClickListener(clickEvent-> {
                RateWindow r = new RateWindow(dto,this);
                UI.getCurrent().addWindow(r);
        });
    }

    private void createRatingLabel(CompanyDTO dto){
        Label label;
        try {
            double rating = Utils.setRating(dto).getRating();
            label = new Label(VaadinIcons.STAR_O.getHtml() + "&nbsp;Bewertung: "+ rating +"/5",ContentMode.HTML);
        }
        catch (NotRatedException e){
            label = new Label(VaadinIcons.STAR_O.getHtml() + "&nbsp;Bewertung: Noch nicht Bewertet",ContentMode.HTML);
        }
        profileHeaderGridLayout.addComponent(label,1,1);
    }

    private void createContactButton() {
        Button edit = new Button("Kontaktieren Sie uns!", VaadinIcons.ENVELOPE);
        edit.addStyleName(MaterialTheme.BUTTON_BORDERLESS);
        edit.addStyleName(MaterialTheme.BUTTON_TINY);
        profileHeaderGridLayout.addComponent(edit, 3, 0);
        profileHeaderGridLayout.setComponentAlignment(edit, Alignment.MIDDLE_RIGHT);
        edit.addClickListener((Button.ClickListener) event -> {
            NachrichtWindow w = new NachrichtWindow();
            UI.getCurrent().addWindow(w);
                if(!w.isVisible()) {
                    w.setVisible(true);
                }
        });
    }

    public void refreshRating(CompanyDTO dto){
        profileHeaderGridLayout.removeComponent(1,1);
        createRatingLabel(dto);
    }

    private static Button createCompanyTitleButton(CompanyDTO company) {
        Button btn = new Button(company.getName(), (Button.ClickListener) event -> {
            ProfileView.setCompany(company);
            ProfileView.setMyProfile(false);
            ProfileControl.showCompany();
        });
        btn.addStyleName(MaterialTheme.BUTTON_BORDERLESS);
        btn.addStyleName("companytitle-button");
        return btn;
    }
}
