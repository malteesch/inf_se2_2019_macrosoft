package com.macrosoft.collhbrs.gui.components;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.Proxy.SearchInterface;
import com.macrosoft.collhbrs.control.Proxy.SearchProxy;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.gui.views.SearchView;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.vaadin.shared.Position;
import com.vaadin.ui.*;

import java.util.ArrayList;
import java.util.List;


public class VacancyFilterComponent extends HorizontalLayout {

    public VacancyFilterComponent(){

        this.addStyleName("search-components");
        this.setWidth("100%");
        this.setSpacing(true);

        Panel filterPanel = new Panel();
        filterPanel.setWidth("80%");
        filterPanel.addStyleName("gridlayouts");
        this.addComponent(filterPanel);
        this.setComponentAlignment(filterPanel, Alignment.MIDDLE_CENTER);

        GridLayout horGrid = new GridLayout(5, 2);
        horGrid.addStyleName("companycomponent-gridlayout");
        horGrid.setWidth("100%");
        filterPanel.setContent(horGrid);

        horGrid.setColumnExpandRatio(0, 2.5F);
        horGrid.setColumnExpandRatio(1, 2.5F);
        horGrid.setColumnExpandRatio(2, 2.5F);
        horGrid.setColumnExpandRatio(3, 2F);
        horGrid.setColumnExpandRatio(4, 0.5F);

        Label header = new Label("Filter");
        header.addStyleName(MaterialTheme.LABEL_H4);
        horGrid.addComponent(header, 0, 0);

        TextField location = new TextField();
        location.setPlaceholder("Ort");
        horGrid.addComponent(location, 0, 1);
        horGrid.setComponentAlignment(location, Alignment.MIDDLE_CENTER);

        TextField tag = new TextField();
        tag.setPlaceholder("Schlagwort");
        horGrid.addComponent(tag, 1, 1);
        horGrid.setComponentAlignment(tag, Alignment.MIDDLE_CENTER);

        DateField date = new DateField();
        horGrid.addComponent(date, 2, 1);
        horGrid.setComponentAlignment(date, Alignment.MIDDLE_CENTER);

        Button deleteFilter = new Button("Filter löschen");
        deleteFilter.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        horGrid.addComponent(deleteFilter, 3, 1);
        horGrid.setComponentAlignment(deleteFilter, Alignment.MIDDLE_RIGHT);
        deleteFilter.setVisible(false);
        deleteFilter.addClickListener((Button.ClickListener) event -> {
            location.clear();
            tag.clear();
            date.clear();
            deleteFilter.setVisible(false);
            SearchView.reloadResults(SearchView.getCurrentResults(), "Ergebnisse der Suche");
        });

        Button filter = new Button("Filtern");
        filter.addStyleName(MaterialTheme.BUTTON_PRIMARY);
        horGrid.addComponent(filter, 4, 1);
        horGrid.setComponentAlignment(filter, Alignment.MIDDLE_RIGHT);

        SearchInterface search = new SearchProxy();
        filter.addClickListener((Button.ClickListener) click -> {
            List<VacancyDTO> filtered = new ArrayList<>();

            if (location.getValue().isEmpty() && tag.getValue().isEmpty() && date.getValue() == null) {
                new Notifier().createNotification("Es wurde kein Filter ausgewählt. Bitte wählen Sie einen Filter.")
                        .at(Position.TOP_CENTER)
                        .show();
            } else {
                try {
                    filtered = search.filterResults(location.getValue(), tag.getValue(), date.getValue());
                } catch (DatabaseException e) {
                    new Notifier().createErrorNotification("Die Ergebnisse konnten nicht gefiltert werden. Bitte wenden Sie sich an den Administrator.")
                            .at(Position.TOP_CENTER)
                            .show();
                    e.printStackTrace();
                }

                if (filtered.size() == 0) {
                    new Notifier().createNotification("Keine Ergebnisse gefunden!")
                            .at(Position.TOP_CENTER)
                            .show();
                } else {
                    SearchView.setFilteredResults(filtered);
                    SearchView.reloadResults(filtered, "Ergebnisse der Suche");
                }
                deleteFilter.setVisible(true);
            }
        });
    }
}
