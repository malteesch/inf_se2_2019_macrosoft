package com.macrosoft.collhbrs.gui.components;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.ui.*;

import java.util.ArrayList;
import java.util.List;

public class ExtensibleListComponent extends VerticalLayout {

    private List<ExtensibleListItem> items = new ArrayList<>();
    private Button addButton;
    private String title;

    public ExtensibleListComponent(String title) {
        this.title = title;

        addButton = new Button("Neuer Eintrag", (Button.ClickListener) click -> {
            items.add(new ExtensibleListItem(""));
            buildView(items);
        });

        buildView(items);
    }

    public ExtensibleListComponent(String title, List<String> prefilledData) {
        this.title = title;

        prefilledData.forEach(entry -> items.add(new ExtensibleListItem(entry)));

        addButton = new Button("Neuer Eintrag", (Button.ClickListener) click -> {
            items.add(new ExtensibleListItem(""));
            buildView(items);
        });

        buildView(items);
    }

    private void buildView(List<ExtensibleListItem> list) {
        this.removeAllComponents();
        Label titleLabel = new Label(title);
        titleLabel.addStyleName(MaterialTheme.LABEL_H4);
        this.addComponent(titleLabel);
        this.addComponent(addButton);
        list.forEach(item -> {
            item.getRemoveButton().addClickListener((Button.ClickListener) click -> {
                items.remove(item);
                buildView(items);
            });
            this.addComponent(item);
        });
    }

    public List<String> getEntries() {
        List<String> entries = new ArrayList<>();
        items.forEach(item -> {
            String entry = item.getValue();
            if (entry.length() > 0) {
                entries.add(entry);
            }
        });
        return entries;
    }

    private static class ExtensibleListItem extends HorizontalLayout {
        private TextField input;
        private Button removeButton;

        ExtensibleListItem(String initialValue) {
            this.setWidth("80%");
            input = new TextField();
            input.setValue(initialValue);
            input.setSizeFull();
            removeButton = new Button("Entfernen");
            this.addComponent(input);
            this.addComponent(removeButton);
        }

        Button getRemoveButton() {
            return removeButton;
        }

        String getValue() {
            return input.getValue();
        }
    }
}
