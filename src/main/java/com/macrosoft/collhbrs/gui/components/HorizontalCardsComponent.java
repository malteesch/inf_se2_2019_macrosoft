package com.macrosoft.collhbrs.gui.components;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.ProfileControl;
import com.macrosoft.collhbrs.gui.views.ProfileView;
import com.macrosoft.collhbrs.model.dtos.DTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.macrosoft.collhbrs.services.utils.Session;
import com.macrosoft.collhbrs.services.utils.Utils;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.Position;
import com.vaadin.ui.*;

import java.time.LocalDate;
import java.util.ArrayList;

class HorizontalCardsComponent<T extends DTO> extends HorizontalLayout {

    private GridLayout horizontalCard = new GridLayout(2, 4);

    HorizontalCardsComponent(T dto) throws Exception {
        this.addStyleName("horizontalcard");
        setUp(dto);
    }

    HorizontalCardsComponent(T dto, String vacancyTitle, LocalDate date) throws Exception {
        this.addStyleName("horizontalcard");
        this.addStyleName("gridlayout-detailscard");
        setUp(dto, vacancyTitle, date);
    }

    private void setUp(T dto) throws Exception {
        Panel p = new Panel();
        p.setWidth("430px");
        p.setHeight("100px");
        horizontalCard.setSizeFull();
        Image img;

        if (dto instanceof VacancyDTO) {
            VacancyDTO vacancy = (VacancyDTO) dto;
            int i = Utils.getNumberApplicationToVacancy(((VacancyDTO) dto).getVacancyId());

            img = createImage(vacancy.getCompany().getImage());

            Label companyNameLabel = new Label(vacancy.getCompany().getName());
            companyNameLabel.setWidth("320px");
            companyNameLabel.addStyleName("horizontalcard-namelabel");
            horizontalCard.addComponent(companyNameLabel, 1, 2);

            Button vacancyName;

            if (vacancy.getTitle().length() > 30) {
                vacancyName = new Button(vacancy.getTitle().substring(0, 30) + "...");
            } else {
                vacancyName = new Button(vacancy.getTitle());
            }

            vacancyName.addStyleName(MaterialTheme.BUTTON_BORDERLESS);
            vacancyName.addStyleName(MaterialTheme.BUTTON_TINY);
            vacancyName.setWidth("330px");
            vacancyName.addStyleName("horizontalcard-button");

            vacancyName.addClickListener((Button.ClickListener) click -> {
                VacancyResultsComponent.detailsLayout.removeAllComponents();
                try {
                    VacancyResultsComponent.detailsLayout.addComponent(VacancyResultsComponent.createDetailsCard(vacancy));
                    if (i > 0 && ((VacancyDTO) dto).getCompany().getUserid() == Session.getCurrentUser().getUserid()) {
                        int vacancyid = ((VacancyDTO) dto).getVacancyId();
                        ArrayList<StudentDTO> liststudi = Utils.getStudentsApplicationToVacancy(vacancyid);

                        Label detailsHeader = new Label("Bewerbungen");
                        detailsHeader.addStyleName(MaterialTheme.LABEL_H4);
                        VacancyResultsComponent.detailsLayout.addComponent(detailsHeader);

                        for (StudentDTO sdto : liststudi) {
                            VacancyResultsComponent.detailsLayout.addComponent(VacancyResultsComponent.createDetailsCard(sdto));
                        }
                    }
                } catch (Exception e) {
                    new Notifier().createErrorNotification("Die Details der Stellenausschreibung konnten nicht gefunden werden. Bitte kontaktieren Sie den Administrator.")
                            .at(Position.TOP_CENTER)
                            .show();
                    e.printStackTrace();
                }
                VacancyResultsComponent.detailsLayout.setVisible(true);
            });
            horizontalCard.addComponent(vacancyName, 1, 0);
            horizontalCard.setComponentAlignment(vacancyName, Alignment.TOP_RIGHT);


        } else if (dto instanceof StudentDTO) {
            StudentDTO student = (StudentDTO) dto;

            img = createImage(student.getImage());

            Label skills;
            if (student.getSkills() != null) {
                StringBuilder builder = new StringBuilder("Skills: " + student.getSkills().get(0));
                for (int i = 1; i < student.getSkills().size(); ++i) {
                    builder.append(", " + student.getSkills().get(i));
                }
                skills = new Label(builder.toString());
            } else {
                skills = new Label("Keine Skills eingetragen.");
            }
            skills.setWidth("320px");
            horizontalCard.addComponent(skills, 1, 1);

            Label interests;
            if (student.getInterests() != null) {
                StringBuilder builder = new StringBuilder("Intressen: " + student.getInterests().get(0));
                for (int i = 1; i < student.getInterests().size(); ++i) {
                    builder.append(", " + student.getInterests().get(i));
                }
                interests = new Label(builder.toString());
            } else {
                interests = new Label("Keine Interessen eingetragen.");
            }
            interests.setWidth("320px");
            horizontalCard.addComponent(interests, 1, 2);

            Button studentNameButton = createNameButton(student);
            horizontalCard.addComponent(studentNameButton, 1, 0);
        } else {
            throw new Exception("[" + HorizontalCardsComponent.class.toString() + "] Error übergebenes DTO ist weder ein VacancyDTO noch ein StudentDTO!");
        }

        img.setHeight("90px");

        horizontalCard.addComponent(img, 0, 0, 0, 3);
        horizontalCard.setComponentAlignment(img, Alignment.MIDDLE_CENTER);
        horizontalCard.setColumnExpandRatio(0, 2.5F);

        p.setContent(horizontalCard);
        this.addComponent(p);
    }

    private void setUp(T dto, String vacancyTitle, LocalDate date) {
        Panel p = new Panel();
        p.setWidth("450px");

        horizontalCard.setSizeFull();

        if (dto instanceof StudentDTO) {
            StudentDTO student = (StudentDTO) dto;

            Image img = createImage(student.getImage());

            img.setHeight("100px");
            horizontalCard.addComponent(img, 0, 0, 0, 3);

            Button studentNameButton = createNameButton(student);
            horizontalCard.addComponent(studentNameButton, 1, 0);


            Label vacancyTitleLabel = new Label("Stelle: " + vacancyTitle);
            vacancyTitleLabel.setWidth("320px");
            horizontalCard.addComponent(vacancyTitleLabel, 1, 1);

            Label applicationDateLabel = new Label("Bewerbungsdatum: " + Utils.convertDateToString(date));
            applicationDateLabel.setWidth("320px");
            horizontalCard.addComponent(applicationDateLabel, 1, 2);

            horizontalCard.setComponentAlignment(img, Alignment.MIDDLE_CENTER);
            horizontalCard.setColumnExpandRatio(0, 2F);
            horizontalCard.setColumnExpandRatio(1, 8F);
        }

        p.setContent(horizontalCard);
        this.addComponent(p);
    }

    private Button createNameButton(StudentDTO student) {
        Button b = new Button(student.getName() + " " + student.getFamilyname());
        b.addStyleName(MaterialTheme.BUTTON_TINY);
        b.addStyleName(MaterialTheme.BUTTON_BORDERLESS);
        b.setWidth("100%");
        b.addClickListener((Button.ClickListener) click -> {
            ProfileView.setStudent(student);
            ProfileView.setMyProfile(false);
            ProfileControl.showStudent();
        });
        return b;
    }

    private Image createImage(byte[] img) {
        Image image;
        if (img != null && img.length > 0) {
            image = Utils.convertToImage(img);
        } else {
            ThemeResource resource = new ThemeResource("img/Portrait_Placeholder.png");
            image = new Image(null, resource);
        }
        return image;
    }
}