package com.macrosoft.collhbrs.gui.components;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.gui.views.SearchView;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Utils;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;

import java.util.ArrayList;

public class ProfileBodyComponent extends VerticalLayout {

    public ProfileBodyComponent(StudentDTO student) throws Exception {
        setUp(student);
    }

    public ProfileBodyComponent(CompanyDTO company) throws Exception {
        setUp(company);
    }

    private <T extends UserDTO> void setUp(T dto) throws Exception {
        this.setSizeFull();
        this.addStyleName("profile-components");
        this.addStyleName("profilebody-components");

        if (dto instanceof StudentDTO) {
            StudentDTO student = (StudentDTO) dto;

            GridLayout layout = new GridLayout(2, 4);
            layout.setWidth("80%");
            layout.setHeight("100%");
            layout.setSpacing(true);
            layout.addStyleName("profileview-gridlayout");
            this.addComponent(layout);
            this.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);

            Label header = new Label("Über " + ((StudentDTO) dto).getName());
            header.addStyleName(MaterialTheme.LABEL_H4);
            header.setWidth("100%");
            layout.addComponent(header, 0, 0, 1, 0);


            Panel interests;
            if (student.getInterests() != null) {
                interests = createPanel(student.getInterests(), "Meine Interessen");
            } else {
                interests = createPanel("Meine Interessen");
            }
            interests.setSizeFull();
            interests.setIcon(VaadinIcons.QUESTION);
            layout.addComponent(interests, 0, 1);


            Panel skills;
            if (student.getSkills() != null) {
                skills = createPanel(student.getSkills(), "Meine Fähigkeiten");
            } else {
                skills = createPanel("Meine Fähigkeiten");
            }
            skills.setSizeFull();
            skills.setIcon(VaadinIcons.STAR);
            layout.addComponent(skills, 1, 1);


            Panel work;
            if (student.getWork().size() > 0) {
                work = createPanel(student.getWork(), "Werdegang");
            } else {
                work = createPanel("Werdegang");
            }
            work.setSizeFull();
            work.setIcon(VaadinIcons.HOURGLASS);
            layout.addComponent(work, 0, 2);

            Panel education;
            if (student.getEducation().size() > 0) {
                education = createPanel(student.getEducation(), "Ausbildung");
            } else {
                education = createPanel("Ausbildung");
            }
            education.setSizeFull();
            education.setIcon(VaadinIcons.ACADEMY_CAP);
            layout.addComponent(education, 1, 2);

            Panel awards;
            if (student.getAward().size() > 0) {
                awards = createPanel(student.getAward(), "Auszeichnungen");
            } else {
                awards = createPanel("Auszeichnungen");
            }
            awards.setSizeFull();
            awards.setIcon(VaadinIcons.BULLSEYE);
            layout.addComponent(awards, 0, 3);

            StreamResource doc = Utils.convertByteArrayToPdf(student.getDocuments().getFile(), student.getDocuments().getDocumenttitle());
            Panel documents = createPanel(doc, student.getDocuments().getDocumenttitle());
            documents.setSizeFull();
            documents.setIcon(VaadinIcons.FILE);
            layout.addComponent(documents, 1, 3);

        } else {
            HorizontalLayout layout = new HorizontalLayout();
            layout.setWidth("100%");
            this.addComponent(layout);

            VacancyResultsComponent vacancies = new VacancyResultsComponent((ArrayList<VacancyDTO>) SearchView.getCurrentResults(), "Aktuelle Stellen");
            layout.addComponent(vacancies);
        }
    }

    private <T> Panel createPanel(ArrayList<T> list, String caption) {
        Panel panel = new Panel(caption);
        panel.addStyleName("student-panel");
        VerticalLayout layout = new VerticalLayout();
        for (T inter : list) {
            layout.addComponent(new Label(VaadinIcons.ARROWS_LONG_RIGHT.getHtml() + " " + inter.toString(), ContentMode.HTML));
        }
        panel.setContent(layout);
        return panel;
    }

    private Panel createPanel(String caption) {
        Panel panel = new Panel(caption);
        panel.addStyleName("student-panel");
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(new Label("Der User hat hier noch nichts eingetragen."));
        panel.setContent(layout);
        return panel;
    }

    private Panel createPanel(StreamResource file, String filename) {
        Panel panel;
        VerticalLayout layout = new VerticalLayout();

        if (file.getBufferSize() < 0) {
            panel = createPanel("Dokumente");
        } else {
            panel = new Panel("Dokumente");
            if (filename != null) {
                Link downloadLink = new Link(filename + ".pdf", file);
                layout.addComponent(downloadLink);
            }
        }

        panel.addStyleName("student-panel");
        panel.setContent(layout);
        return panel;
    }
}
