package com.macrosoft.collhbrs.gui.components;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.LoginControl;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.model.dtos.UserLoginDTO;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.vaadin.event.ShortcutAction;
import com.vaadin.shared.Position;
import com.vaadin.ui.*;

public class LoginComponent extends VerticalLayout {

    public LoginComponent(){
        this.setSizeFull();
        this.addStyleName("login-component");
        this.setSpacing(true);

        GridLayout loginGridLayout = new GridLayout(1,3);
        loginGridLayout.setSizeFull();
        loginGridLayout.setSpacing(true);
        loginGridLayout.addStyleName("login-gridlayout");
        loginGridLayout.setRowExpandRatio(0,4);
        loginGridLayout.setRowExpandRatio(1,6);
        loginGridLayout.setRowExpandRatio(2,0.1F);

        Panel panel = new Panel("Bitte Login-Daten eingeben");
        panel.addStyleName("login");
        panel.setWidth("300");
        panel.setHeight("320");
        panel.setContent(loginGridLayout);
        this.addComponent(panel);
        this.setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

        final TextField userLogin = new TextField();
        userLogin.setWidth("90%");
        userLogin.setPlaceholder("UserID");
        loginGridLayout.addComponent(userLogin,0,0);
        loginGridLayout.setComponentAlignment(userLogin,Alignment.BOTTOM_CENTER);

        final PasswordField passwordField = new PasswordField();
        passwordField.setWidth("90%");
        passwordField.setPlaceholder("Passwort");
        loginGridLayout.addComponent(passwordField,0,1);
        loginGridLayout.setComponentAlignment(passwordField,Alignment.TOP_CENTER);

        Button buttonLogin = new Button("Login");
        buttonLogin.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        buttonLogin.setWidth("100%");
        buttonLogin.addStyleName(MaterialTheme.BUTTON_HUGE);
        loginGridLayout.addComponent(buttonLogin,0,2);
        loginGridLayout.setComponentAlignment(buttonLogin, Alignment.BOTTOM_CENTER);

        buttonLogin.addClickListener((Button.ClickListener) event -> {
            String login = userLogin.getValue();
            String password = passwordField.getValue();

            try {
                LoginControl.checkAuthentication(new UserLoginDTO(login, password));
            } catch (UserException e) {
                new Notifier().createErrorNotification("FEHLER: " + e.getMessage())
                        .at(Position.BOTTOM_CENTER)
                        .withDelay(5000)
                        .show();
                userLogin.setValue("");
                passwordField.setValue("");
            } catch (DatabaseException e) {
                new Notifier().createErrorNotification("FEHLER: Es gab einen Fehler mit dem Zugriff auf die Datenbank. Bitte wenden Sie sich an den Hersteller.")
                        .at(Position.BOTTOM_CENTER)
                        .withDelay(5000)
                        .show();
            }
        });
    }
}
