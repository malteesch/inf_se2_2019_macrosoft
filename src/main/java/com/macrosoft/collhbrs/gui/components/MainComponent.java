package com.macrosoft.collhbrs.gui.components;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.LiveSearchControl;
import com.macrosoft.collhbrs.control.SuggestionsControl;
import com.macrosoft.collhbrs.control.toggle.ToggleRouter;
import com.macrosoft.collhbrs.model.daos.CompanyDAO;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.dtos.ApplicantDTO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.services.db.JDBCConnection;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.ui.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MainComponent extends VerticalLayout {
    private ComboBox<String> searchbox = LiveSearchControl.createLiveSearch();

    public MainComponent(String role) throws Exception {
        this.setSizeFull();
        this.addStyleName("main-component");
        this.setSpacing(true);

        GridLayout labelGridLayout = new GridLayout(1, 2);
        labelGridLayout.setWidth("80%");
        labelGridLayout.setHeight("100%");
        labelGridLayout.setRowExpandRatio(0, 0.1F);
        this.addComponent(labelGridLayout);
        this.setComponentAlignment(labelGridLayout, Alignment.MIDDLE_CENTER);

        Label labelKlein = new Label("Die Studenten-Jobbörse");
        labelKlein.addStyleName("mainview-label-klein");
        labelGridLayout.addComponent(labelKlein);
        labelGridLayout.setComponentAlignment(labelKlein, Alignment.BOTTOM_CENTER);

        Label label = new Label();
        label.addStyleName("mainview-label");
        labelGridLayout.addComponent(label);
        labelGridLayout.setComponentAlignment(label, Alignment.TOP_CENTER);

        if (role.equals(Config.Role.STUDENT)) {

            label.setValue("Finde den Job der zu dir passt!");

            GridLayout searchLayout = new GridLayout(2, 1);
            searchLayout.setWidth("60%");
            searchLayout.setColumnExpandRatio(0, 9);
            searchLayout.setColumnExpandRatio(1, 1);
            searchLayout.addStyleName("mainview-searchlayout");
            this.addComponent(searchLayout);
            this.setComponentAlignment(searchLayout, Alignment.MIDDLE_CENTER);

            searchLayout.addComponent(searchbox, 0, 0);

            Button searchButton = new Button("Suchen");
            searchButton.addStyleName("searchButton");
            searchButton.addStyleName(MaterialTheme.BUTTON_CUSTOM);
            searchButton.addStyleName(MaterialTheme.BUTTON_BORDER);
            searchButton.addStyleName(MaterialTheme.BUTTON_HUGE);
            searchLayout.addComponent(searchButton, 1, 0);
            searchLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_CENTER);

            searchButton.addClickListener((Button.ClickListener) clickEvent -> {
                searchbox.setNewItemProvider(input -> {
                    LiveSearchControl.comboboxSearch(input);
                    return Optional.empty();
                });
            });
        } else {
            label.setValue("Findet die Studenten die zu euch passen!");

            GridLayout studentPanels = new GridLayout(2, 1);
            studentPanels.setWidth("80%");
            studentPanels.setHeight("100%");
            studentPanels.addStyleName("studentPanels");
            this.addComponent(studentPanels);
            this.setComponentAlignment(studentPanels, Alignment.MIDDLE_CENTER);

            GridLayout applicationLayout = new GridLayout();
            applicationLayout.addStyleName("maincompany-component");
            applicationLayout.setSpacing(true);
            applicationLayout.setWidth("90%");
            studentPanels.addComponent(applicationLayout, 1, 0);
            studentPanels.setComponentAlignment(applicationLayout, Alignment.TOP_CENTER);

            Label applicants = new Label("Eingegangende Bewerbungen");
            applicants.addStyleName("maincompany-component-header");
            applicants.addStyleName(MaterialTheme.LABEL_BOLD);
            applicationLayout.addComponent(applicants);

            ArrayList<ApplicantDTO> applicantList = new ArrayList<>();
            CompanyDTO dto = new CompanyDAO().getOne(Session.getCurrentUser().getUsername());

            //language=PostgreSQL
            final String sql = "SELECT * FROM \"macrosoft_collHBRS\".application_student\n" +
                    "INNER JOIN \"macrosoft_collHBRS\".vacancy ON application_student.vacancyid = vacancy.vacancyid\n" +
                    "INNER JOIN \"macrosoft_collHBRS\".company ON vacancy.companyid = company.companyid\n" +
                    "INNER JOIN \"macrosoft_collHBRS\".student ON application_student.studentid = student.studentid\n" +
                    "WHERE companyname = ?;";
            PreparedStatement pst = JDBCConnection.getInstance().getPreparedStatement(sql);
            pst.setString(1, dto.getName());
            ResultSet set = pst.executeQuery();
            while (set.next()) {
                applicantList.add(new ApplicantDTO(set.getString("name"), set.getString("familyname"), set.getString("vacancytitle"),
                        set.getInt("studentid"), new java.sql.Date(set.getDate("applicationdate").getTime()).toLocalDate()));
            }

            if (ToggleRouter.featureIsEnabled(Config.Features.STUDENT_SUGGESTIONS)) {
                List<StudentDTO> studentSuggestions = SuggestionsControl.getStudentSuggestions();
                if (studentSuggestions.size() > 0) {
                    GridLayout suggestionLayout = new GridLayout();
                    suggestionLayout.addStyleName("maincompany-component");
                    suggestionLayout.setSpacing(true);
                    suggestionLayout.setWidth("90%");
                    studentPanels.addComponent(suggestionLayout, 0, 0);
                    studentPanels.setComponentAlignment(suggestionLayout, Alignment.TOP_CENTER);

                    Label suggestions = new Label("Vorschläge");
                    suggestions.addStyleName("maincompany-component-header");
                    suggestions.addStyleName(MaterialTheme.LABEL_BOLD);
                    suggestionLayout.addComponent(suggestions);

                    for (StudentDTO student : studentSuggestions) {
                        suggestionLayout.addComponent(new HorizontalCardsComponent<>(student));
                    }

                    for (Component c : suggestionLayout) {
                        if (!(c instanceof Label)) {
                            suggestionLayout.setComponentAlignment(c, Alignment.MIDDLE_CENTER);
                        }
                    }
                }
            }

            for (ApplicantDTO appl : applicantList) {
                applicationLayout.addComponent(new HorizontalCardsComponent<>(new StudentDAO().getOne(appl.getStudentId()), appl.getVacancyTitle(), appl.getApplicationDate()));
            }

            for (Component c : applicationLayout) {
                if (!(c instanceof Label)) {
                    applicationLayout.setComponentAlignment(c, Alignment.MIDDLE_CENTER);
                }
            }
        }
    }
}
