package com.macrosoft.collhbrs.gui.components;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.ui.*;

public class CreateVacancyComponent extends VerticalLayout {

    private TextField titleInput;
    private TextArea descriptionInput;

    public CreateVacancyComponent() {
        this.setHeight("100%");
        GridLayout componentGrid = new GridLayout(10, 10);
        componentGrid.setSizeFull();
        componentGrid.setSpacing(true);
        this.addComponent(componentGrid);

        for (int i = 0; i < 10; ++i) {
            componentGrid.setColumnExpandRatio(i, 1F);
        }

        Label titleLabel = new Label("Titel");
        titleLabel.setSizeFull();
        titleLabel.setStyleName(MaterialTheme.LABEL_H4);

        Label descriptionLabel = new Label("Beschreibung");
        descriptionLabel.setSizeFull();
        descriptionLabel.setStyleName(MaterialTheme.LABEL_H4);

        titleInput = new TextField();
        titleInput.setSizeFull();
        descriptionInput = new TextArea();
        descriptionInput.setSizeFull();

        componentGrid.addComponent(titleLabel, 0, 0, 1, 0);
        componentGrid.addComponent(titleInput, 2, 0, 5, 0);
        componentGrid.setComponentAlignment(titleLabel, Alignment.MIDDLE_LEFT);

        componentGrid.addComponent(descriptionLabel, 0, 1, 1, 1);
        componentGrid.addComponent(descriptionInput, 2, 1, 9, 9);
    }

    public String getTitleValue() {
        return titleInput.getValue();
    }

    public String getDescriptionValue() {
        return descriptionInput.getValue();
    }
}
