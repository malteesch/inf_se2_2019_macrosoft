package com.macrosoft.collhbrs.gui.views;

import com.macrosoft.collhbrs.control.Proxy.SearchProxy;
import com.macrosoft.collhbrs.gui.components.ProfileBodyComponent;
import com.macrosoft.collhbrs.gui.components.ProfileHeaderComponent;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;

public class ProfileView extends MainView implements View {
    private static CompanyDTO companyDTO;
    public static void setCompany(CompanyDTO dto) {
        ProfileView.companyDTO = dto;
    }
    public static CompanyDTO getCompany() {
        return ProfileView.companyDTO;
    }

    private static StudentDTO studentDTO;
    public static void setStudent(StudentDTO dto) {
        ProfileView.studentDTO = dto;
    }
    public static StudentDTO getStudent() {
        return ProfileView.studentDTO;
    }

    private static boolean myProfile = true;
    public static void setMyProfile(boolean profile) {
        ProfileView.myProfile = profile;
    }
    public static boolean getMyProfile() {
        return ProfileView.myProfile;
    }

    public GridLayout layout;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        if (!Session.isLoggedIn()) {
            UI.getCurrent().getNavigator().navigateTo(Config.Views.LOGIN);
        } else {
            this.parentSetUp();
            try {
                this.setUp();
            } catch (Exception e) {
                e.printStackTrace(); //TODO
            }
        }
    }

    public void setUp() throws Exception {
        this.addStyleName("views");
        this.addStyleName("profileview");
        this.setSizeFull();

        layout = new GridLayout(1, 2);
        layout.addStyleName("profileview-gridlayout");
        layout.setSpacing(true);
        layout.setSizeFull();
        this.addComponent(layout);

        if (Session.getCurrentRole().equals(Config.Role.STUDENT) && ProfileView.getMyProfile()) {
            setUpStudent();
        } else if (Session.getCurrentRole().equals(Config.Role.COMPANY) && !ProfileView.getMyProfile()) {
            setUpStudent();
        } else {
            setUpCompany();
        }
    }

    private void setUpStudent() throws Exception{
        HorizontalLayout headerLayout = new HorizontalLayout();
        headerLayout.setWidth("100%");
        ProfileHeaderComponent headerStudent = new ProfileHeaderComponent(ProfileView.getStudent());
        headerLayout.addComponent(headerStudent);
        layout.addComponent(headerLayout, 0, 0);

        HorizontalLayout bodyLayout = new HorizontalLayout();
        bodyLayout.setWidth("100%");
        ProfileBodyComponent bodyStudent = new ProfileBodyComponent(ProfileView.getStudent());
        bodyLayout.addComponent(bodyStudent);
        layout.addComponent(bodyLayout, 0, 1);
    }

    private void setUpCompany() throws Exception{
        SearchView.setCurrentResults(new SearchProxy().getCompanyVacancies(getCompany()));

        HorizontalLayout headerLayout = new HorizontalLayout();
        headerLayout.setWidth("100%");
        ProfileHeaderComponent headerCompany = new ProfileHeaderComponent(ProfileView.getCompany());
        headerLayout.addComponent(headerCompany);
        layout.addComponent(headerLayout, 0, 0);

        HorizontalLayout bodyLayout = new HorizontalLayout();
        bodyLayout.setWidth("100%");
        ProfileBodyComponent bodyCopany = new ProfileBodyComponent(ProfileView.getCompany());
        bodyLayout.addComponent(bodyCopany);
        layout.addComponent(bodyLayout, 0, 1);
    }
}
