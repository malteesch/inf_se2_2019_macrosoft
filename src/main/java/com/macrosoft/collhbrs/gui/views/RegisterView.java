package com.macrosoft.collhbrs.gui.views;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.gui.components.NavbarComponent;
import com.macrosoft.collhbrs.model.daos.CompanyDAO;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.dtos.AddressDTO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.macrosoft.collhbrs.services.utils.PasswordValidator;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.annotations.Theme;
import com.vaadin.data.*;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Theme("registration")
public class RegisterView extends VerticalLayout implements View {

    private Panel Step1Panel = new Panel("Schritt 2: Bitte geben Sie Nutzername, E-Mail und Passwort ein.");
    private Panel Step2Panel = new Panel("Schritt 3: Bitte geben Sie Ihre perönlichen Daten an.");
    private Panel Step3Panel = new Panel("Schritt 3: Bitte geben Sie Name, Anschrift und Beschreibung ihres Unternehmens an.");
    private Panel ChoicePanel = new Panel("Registrieren Sie sich als Student");
    private Binder<UserDTO> binder = new Binder<>();
    private Binder<StudentDTO> StudentBinder = new Binder<>();
    private Binder<AddressDTO> AdressBinder = new Binder<>();
    private Binder<CompanyDTO> CompanyBinder = new Binder<>();

    private Binder.Binding<UserDTO, String> passwortBinding;
    private Binder.Binding<UserDTO, String> bestaetigePasswortBinding;

    private boolean zeigePasswortStatus;
    private boolean isStudent;

    private void setUpChoice() {

        if(!ChoicePanel.isVisible()){
            ChoicePanel.setVisible(true);
            return;
        }
        NavbarComponent nav = null;
        try {
            nav = new NavbarComponent();
            nav.setHeight("90px");
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        this.setSizeFull();
        this.addComponent(nav);
        this.addStyleName("registration");
        this.addStyleName("scrollable");

        this.addComponent(ChoicePanel);
        this.setComponentAlignment(ChoicePanel, Alignment.MIDDLE_CENTER);
        VerticalLayout content = new VerticalLayout();
        content.setSizeFull();
        HorizontalLayout buttonContainer = new HorizontalLayout();
        content.addComponent(buttonContainer);

        ChoicePanel.setContent(buttonContainer);
        ChoicePanel.setWidth(37, Unit.PERCENTAGE);
        ChoicePanel.setHeight(50, Unit.PERCENTAGE);
        buttonContainer.setSizeFull();


        Button student = new Button("Student");
        Button company = new Button("Unternehmen");

        buttonContainer.setSpacing(false);
        buttonContainer.addComponent(student);
        buttonContainer.addComponent(company);

        student.setWidth(100, Unit.PERCENTAGE);
        company.setWidth(100, Unit.PERCENTAGE);

        student.setHeight(90, Unit.PERCENTAGE);
        company.setHeight(90, Unit.PERCENTAGE);

        buttonContainer.setComponentAlignment(student, Alignment.BOTTOM_CENTER);
        buttonContainer.setComponentAlignment(company, Alignment.BOTTOM_CENTER);

        student.addClickListener(ClickEvent -> {
            ChoicePanel.setVisible(false);
            setUpStep1();
            isStudent = true;
        });
        company.addClickListener(ClickEvent -> {
            ChoicePanel.setVisible(false);
            setUpStep1();
            isStudent = false;
        });

    }

    private void setUpStep1() {
        if (!Step1Panel.isVisible()) {
            Step1Panel.setVisible(true);
            return;
        }
        this.setSizeFull();
        this.addStyleName("scrollable");

        VerticalLayout Step1layout = new VerticalLayout();
        this.addComponent(Step1Panel);
        Step1Panel.setContent(Step1layout);
        this.setComponentAlignment(Step1Panel, Alignment.MIDDLE_CENTER);
        Step1Panel.setWidth(30, Unit.PERCENTAGE);

        TextField userInput = new TextField("Nutzername:");
        userInput.setWidth(50, Unit.PERCENTAGE);
        Step1layout.addComponent(userInput);
        binder.forField(userInput)
                .asRequired(new StringLengthValidator("Der Nutzername muss zwischen 4 und 30 Zeichen haben", 4, 30))
                .bind(UserDTO::getUsername, UserDTO::setUsername);


        TextField emailInput = new TextField("E-Mail:");
        Step1layout.addComponent(emailInput);
        emailInput.setWidth(100, Unit.PERCENTAGE);
        binder.forField(emailInput)
                .asRequired(new EmailValidator("Das sieht nicht nach einer gültigen E-Mail aus."))
                .bind(UserDTO::getEmail, UserDTO::setEmail);

        PasswordField passwordField = new PasswordField("Passwort:");
        passwordField.setWidth(100, Unit.PERCENTAGE);
        Step1layout.addComponent(passwordField);
        passwortBinding = binder.forField(passwordField)
                .asRequired(new PasswordValidator())
                .bind(UserDTO::getPassword, UserDTO::setPassword);
        passwordField.addValueChangeListener(event -> bestaetigePasswortBinding.validate());

        PasswordField confirmPasswordField = new PasswordField("Passwort wiederholen: ");
        confirmPasswordField.setWidth(100, Unit.PERCENTAGE);
        Step1layout.addComponent(confirmPasswordField);

        bestaetigePasswortBinding = binder.forField(confirmPasswordField)
                .asRequired(Validator.from(this::validateConfirmPasswd,
                        "Passwörter müssen übereinstimmen!"))
                .bind(UserDTO::getPassword, (person, pwd) -> {
                });
        Step1layout.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);

        HorizontalLayout ButtonContainer = new HorizontalLayout();
        ButtonContainer.setSizeFull();

        Button back = new Button("zurück");
        Button buttonToStep2 = new Button("Weiter");
        ButtonContainer.addComponent(back);
        ButtonContainer.setComponentAlignment(back, Alignment.BOTTOM_LEFT);
        Step1layout.addComponent(ButtonContainer);
        ButtonContainer.addComponent(buttonToStep2);
        ButtonContainer.setComponentAlignment(buttonToStep2, Alignment.BOTTOM_RIGHT);

        back.addClickListener(clickEvent -> {
            Step1Panel.setVisible(false);
            setUpChoice();
        });

        buttonToStep2.addClickListener(clickEvent -> {

            UserDTO dto = new UserDTO();
            try {
                binder.writeBean(dto);
                //b.writeBean(new String());
            } catch (ValidationException e) {
                Notification notif = new Notification("Ein oder mehrere Felder sind ungültig", Notification.Type.ERROR_MESSAGE);
                notif.setPosition(Position.BOTTOM_CENTER);
                notif.setDelayMsec(5000);
                notif.show(Page.getCurrent());
                return;
            }
            if (isStudent) {
                setUpStudent(dto);
            } else {
                setUpCompany(dto);
            }
            Step1Panel.setVisible(false);
        });


    }

    private void setUpStudent(UserDTO dto) {
        if (!Step2Panel.isVisible()) {
            Step2Panel.setVisible(true);
            return;
        }

        this.addComponent(Step2Panel);
        Step2Panel.setWidth(33, Unit.PERCENTAGE);
        Step2Panel.setStyleName("RegisterPanel");
        this.setComponentAlignment(Step2Panel, Alignment.MIDDLE_CENTER);
        VerticalLayout layout = new VerticalLayout();
        Step2Panel.setContent(layout);

        ComboBox<String> anrede = new ComboBox<>("Anrede:");
        anrede.setItems("Herr", "Frau");
        anrede.setTextInputAllowed(false);
        anrede.setEmptySelectionAllowed(false);
        layout.addComponent(anrede);
        StudentBinder.forField(anrede)
                .bind(StudentDTO::getAnrede, StudentDTO::setAnrede);

        HorizontalLayout nameContainer = new HorizontalLayout();
        nameContainer.setSizeFull();

        TextField Name = new TextField("Nachname:");

        Name.setWidth(100, Unit.PERCENTAGE);
        Name.setRequiredIndicatorVisible(true);
        StudentBinder.forField(Name)
                .asRequired("Bitte geben Sie Ihren Nachname an.")
                .bind(StudentDTO::getFamilyname, StudentDTO::setFamilyname);


        nameContainer.addComponent(Name);

        TextField Vorname = new TextField("Vorname:");
        Vorname.setWidth(100, Unit.PERCENTAGE);
        StudentBinder.forField(Vorname)
                .asRequired("Bitte geben Sie ihren Vornamen an.")
                .bind(StudentDTO::getName, StudentDTO::setName);


        nameContainer.addComponent(Vorname);

        layout.addComponent(nameContainer);

        DateField date = new DateField("Geburtstag:");
        date.setDateFormat("dd.MM.yyyy");
        date.setPlaceholder("dd.mm.yyyy");
        date.setParseErrorMessage("Bitte Datum in diesem Format eingeben:");
        layout.addComponent(date);
        StudentBinder.forField(date)
                .asRequired("Bitte geben Sie ihr Geburtsdatum an.")
                .bind(StudentDTO::getBirthdate, StudentDTO::setBirthdate);

        addAdress(layout);

        List<Button> l = addButtons(layout);
        Button buttonToStep1 = l.get(0);
        Button buttonToStep3 = l.get(1);


        buttonToStep1.addClickListener(clickEvent -> {
            Step2Panel.setVisible(false);
            this.setUpStep1();
        });

        buttonToStep3.addClickListener(clickEvent -> {
            boolean isValid = true;
            StudentDTO s = new StudentDTO(dto);
            AddressDTO a = new AddressDTO();
            try {
                AdressBinder.writeBean(a);
            } catch (ValidationException e) {
                isValid = false;
            }
            try {
                StudentBinder.writeBean(s);
                s.setAddress(a);
                StudentDAO add = new StudentDAO();
                add.createOne(s);
                setUpStep3();

            } catch (ValidationException e) {
                isValid = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isValid) {
                new Notifier().createWarningNotification("Ein oder mehrere Felder enthalten invalide Eingaben.")
                        .at(Position.BOTTOM_CENTER)
                        .show();
            }
        });
    }

    private void setUpCompany(UserDTO dto) {
        if (!Step3Panel.isVisible()) {
            Step3Panel.setVisible(true);
            return;
        }
        this.addComponent(Step3Panel);
        Step3Panel.setWidth(50, Unit.PERCENTAGE);
        Step3Panel.setStyleName("RegisterPanel");
        this.setComponentAlignment(Step3Panel, Alignment.MIDDLE_CENTER);
        VerticalLayout layout = new VerticalLayout();
        Step3Panel.setContent(layout);

        TextField companyName = new TextField("Name Ihres Unternehmens:");
        companyName.setWidth(50, Unit.PERCENTAGE);
        CompanyBinder.forField(companyName)
                .asRequired("Bitte geben Sie den Namen Ihres Unternehmens an.")
                .bind(CompanyDTO::getName, CompanyDTO::setName);
        layout.addComponent(companyName);

        RichTextArea companyDescription = new RichTextArea("Beschreibung Ihres Unternehmens:");
        companyDescription.setSizeFull();
        CompanyBinder.forField(companyDescription)
                .asRequired("Bitte geben Sie eine kurze Beschreibung Ihres Unternehmens an.")
                .bind(CompanyDTO::getDescription, CompanyDTO::setDescription);
        layout.addComponent(companyDescription);

        TextField website = new TextField("Website ihres Unternehmens");
        website.setSizeFull();
        CompanyBinder.forField(website)
                .bind(CompanyDTO::getWebsite, CompanyDTO::setWebsite);
        layout.addComponent(website);

        addAdress(layout);

        List<Button> l = addButtons(layout);
        Button buttonToStep1 = l.get(0);
        Button buttonToStep3 = l.get(1);

        buttonToStep1.addClickListener(ClickEvent -> {
            Step3Panel.setVisible(false);
            setUpStep1();
        });

        buttonToStep3.addClickListener(ClickEvent -> {
            boolean isValid = true;
            CompanyDTO c = new CompanyDTO(dto);
            AddressDTO a = new AddressDTO();
            try {
                AdressBinder.writeBean(a);
            } catch (ValidationException e) {
                isValid = false;
            }
            try {
                CompanyBinder.writeBean(c);
                AdressBinder.writeBean(a);

                c.setAddress(a);
                CompanyDAO cDAO = new CompanyDAO();
                cDAO.createOne(c);
                setUpStep3();
            } catch (ValidationException e) {
                isValid = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isValid) {
                new Notifier().createWarningNotification("Ein oder mehrere Felder enthalten invalide Eingaben.")
                        .at(Position.BOTTOM_CENTER)
                        .show();
            }

        });


    }

    private void setUpStep3() {
        new Notifier().createNotification("Sie haben Sich erfolgreich Registriert! Bitte melden Sie sich mit Ihrem Nutzernamen und Passwort an.")
                .at(Position.BOTTOM_CENTER)
                .show();
        UI.getCurrent().getNavigator().navigateTo(Config.Views.LOGIN);


    }

    private boolean validateConfirmPasswd(String confirmPasswordValue) {
        zeigePasswortStatus = false;
        if (confirmPasswordValue.isEmpty()) {
            return true;

        }
        BindingValidationStatus<String> status = passwortBinding.validate();
        if (status.isError()) {
            return true;
        }
        zeigePasswortStatus = true;
        HasValue<?> pwdField = passwortBinding.getField();
        return Objects.equals(pwdField.getValue(), confirmPasswordValue);
    }

    private void addAdress(Layout layout){
        HorizontalLayout addressContainer1 = new HorizontalLayout();
        addressContainer1.setSizeFull();
        layout.addComponent(addressContainer1);

        TextField street = new TextField("Straße:");
        AdressBinder.forField(street)
                .asRequired("Bitte geben Sie die Straße an.")
                .bind(AddressDTO::getStreet, AddressDTO::setStreet);
        addressContainer1.addComponent(street);
        street.setWidth(100, Unit.PERCENTAGE);

        TextField housenr = new TextField("Hausnummer");
        addressContainer1.addComponent(housenr);
        housenr.setMaxLength(4);
        AdressBinder.forField(housenr)
                .asRequired("Bitte geben Sie die Hausnummer an.")
                .bind(AddressDTO::getHousenumber, AddressDTO::setHousenumber);
        housenr.setWidth(35, Unit.PERCENTAGE);

        HorizontalLayout addressContainer2 = new HorizontalLayout();
        addressContainer2.setSizeFull();
        layout.addComponent(addressContainer2);

        TextField postalcode = new TextField("Postleitzahl:");
        postalcode.setMaxLength(5);
        addressContainer2.addComponent(postalcode);
        AdressBinder.forField(postalcode)
                .asRequired("Bitte geben Sie die Postleitzahl an!")
                .bind(AddressDTO::getPostalcode, AddressDTO::setPostalcode);
        postalcode.setWidth(100, Unit.PERCENTAGE);

        TextField city = new TextField("Ort:");
        addressContainer2.addComponent(city);
        AdressBinder.forField(city)
                .asRequired("Bitte geben Sie Ihren Ort an.")
                .bind(AddressDTO::getCity, AddressDTO::setCity);
        city.setWidth(100, Unit.PERCENTAGE);

        TextField country = new TextField("Land:");
        addressContainer2.addComponent(country);
        AdressBinder.forField(country)
                .asRequired("Bitte geben Sie das Land an.")
                .bind(AddressDTO::getCountry, AddressDTO::setCountry);
        country.setWidth(100, Unit.PERCENTAGE);
    }

    private List<Button> addButtons(Layout layout){
        HorizontalLayout buttonContainer = new HorizontalLayout();
        layout.addComponent(buttonContainer);
        buttonContainer.setSizeFull();

        Button buttonToStep1 = new Button("Zurück");
        buttonContainer.addComponent(buttonToStep1);
        buttonContainer.setComponentAlignment(buttonToStep1, Alignment.BOTTOM_LEFT);

        Button buttonToStep3 = new Button("Weiter");
        buttonContainer.addComponent(buttonToStep3);
        buttonContainer.setComponentAlignment(buttonToStep3, Alignment.BOTTOM_RIGHT);

        List l = new ArrayList();
        l.add(buttonToStep1);
        l.add(buttonToStep3);
        return l;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        if (Session.isLoggedIn()) {
            UI.getCurrent().getNavigator().navigateTo(Config.Views.MAIN);
        } else {
            this.setUpChoice();
        }
    }
}

