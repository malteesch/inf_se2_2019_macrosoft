package com.macrosoft.collhbrs.gui.views;

import com.macrosoft.collhbrs.gui.components.ProfileHeaderComponent;
import com.macrosoft.collhbrs.gui.components.VacancyDetailsComponent;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;

public class VacancyDetailsView extends MainView implements View {

    private static VacancyDTO currentVacancy;
    public static void setCurrentVacancy(VacancyDTO currentVacancy) {
        VacancyDetailsView.currentVacancy = currentVacancy;
    }
    public static VacancyDTO getCurrentVacancy() {
        return VacancyDetailsView.currentVacancy;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        if (!Session.isLoggedIn()) {
            UI.getCurrent().getNavigator().navigateTo(Config.Views.LOGIN);
        } else {
            parentSetUp();
            setUp();
        }
    }

    private void setUp() {
        this.addStyleName("views");
        this.addStyleName("vacancyviews");
        this.setSizeFull();
        this.setSpacing(true);

        GridLayout layout = new GridLayout(1, 2);
        layout.addStyleName("vacancyview-gridlayout");
        layout.setSpacing(true);
        layout.setSizeFull();
        this.addComponent(layout);

        try {
            if (Session.getCurrentRole().equals(Config.Role.STUDENT)) {
                HorizontalLayout headerlayout = new HorizontalLayout();
                headerlayout.setWidth("100%");
                ProfileHeaderComponent header = new ProfileHeaderComponent(getCurrentVacancy().getCompany());
                headerlayout.addComponent(header);
                layout.addComponent(headerlayout);
            }

            HorizontalLayout vacancylayout = new HorizontalLayout();
            vacancylayout.setWidth("100%");
            VacancyDetailsComponent details = new VacancyDetailsComponent(getCurrentVacancy());
            vacancylayout.addComponent(details);
            layout.addComponent(vacancylayout);

        } catch (Exception e) {
            new Notifier().createErrorNotification("Die Stellenanzeigen konnten nicht gefunden werden.")
                    .at(Position.TOP_CENTER)
                    .show();
            e.printStackTrace();
        }
    }
}
