package com.macrosoft.collhbrs.gui.views;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class HelpView extends MainView implements View {

    private GridLayout grid = new GridLayout();

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        this.parentSetUp();
        this.setUp();
    }

    public void setUp() {
        this.addStyleName("helpview");
        this.addStyleName("views");
        this.setSizeFull();

        VerticalLayout layout = new VerticalLayout();
        layout.addStyleName("search-components");
        layout.addStyleName("profilebody-components");
        this.addComponent(layout);


        grid.setSpacing(true);
        grid.setWidth("80%");
        layout.addComponent(grid);
        layout.setComponentAlignment(grid, Alignment.TOP_CENTER);

        Label heading = new Label("Frequently Asked Questions");
        heading.addStyleName(MaterialTheme.LABEL_H4);
        grid.addComponent(heading);

        createIsNotLoggedInContent();

        if (Session.isLoggedIn()) {
            createIsLoggedInContent();
        }
    }

    private void createGridRow(String heading, String content) {
        Label title = new Label(heading);
        title.setWidth("100%");
        title.addStyleName(MaterialTheme.LABEL_BOLD);

        Label cont = new Label(content);
        cont.setWidth("100%");

        grid.addComponent(new Label());
        grid.addComponent(title);
        grid.addComponent(cont);
    }

    private void createIsNotLoggedInContent() {
        createGridRow("Wie kann ich mich als Student registrieren?", "Dein Konto können Sie in wenigen Minuten erstellen und die Registrierung bei Coll@HBRS\n" +
                "ist natürlich kostenlos. Nachfolgend erläutern wir Ihnen die einzelnen Schritte:\n" +
                "\n" +
                "Zunächst müssen Sie auf der Landing-Page auf den Button „Register“ klicken. Damit öffnet das Portal die Registrierungsfunktion.\n" +
                "Hierbei wählen Sie Student aus.\n" +
                "Anschließend fordert das System Sie auf, die folgenden Daten einzugeben:\n" +
                "UserID: Dient in Zukunft als Ihr Benutzername. Hiermit melden Sie sich beim Login im System an. Die UserID kann aus einer beliebigen 4-30 stelligen Ansammlung aus Buchstaben, Zeichen und Zahlen bestehen.\n" +
                "E-Mail: Dient zur Verifizierung Ihrer Person. Zu Beginn schicken wir Ihnen einmalig eine E-Mail, die Sie bestätigen müssen. Sobald Ihr Account in Betrieb ist, nutzen wir Ihre E-Mailadresse nur noch für freiwillige Sonderfunktionen, die Ihr Erlebnis verbessern sollen, z.B. um Ihnen Stellenvorschläge passend zu Ihren Vorlieben zu schicken oder im Falle eines Kennwortverlustes ein neues erstellen zu können.\n" +
                "Passwort: Dient in Zukunft als Ihr Passwort. Hiermit melden Sie sich beim Login im System an. Das Passwort muss eine mindestens 6-stellige Kombination sein und jeweils mindestens einen Buchstaben und eine Zahl beinhalten.\n" +
                "\n" +
                "Die Daten bestätigen Sie, indem Sie auf den Button „Weiter“ klicken.\n" +
                "Nun werden auch die studentenspezifischen Informationen von Ihnen abgefragt. Diese setzen sich zusammen aus:\n" +
                "Anrede: Wählen Sie Ihrem Geschlecht entsprechend Mann/Frau aus oder falls sie Divers sind, lassen Sie dieses Feld unausgewählt(Nicht implementiert).\n" +
                "Nachname: Geben Sie Ihren Nachnamen ein. Wir benötigen diesen, um Ihre Bewerbungen zu personalisieren zu können und es den Unternehmen zu erleichtern Sie zu finden.\n" +
                "Vorname: Geben Sie Ihren Vornamen ein. Wir benötigen diesen, um Ihre Bewerbungen zu personalisieren zu können und es den Unternehmen zu erleichtern Sie zu finden.\n" +
                "Geburtsdatum: Hier geben Sie bitte Ihr Geburtsdatum ein. Hierfür ist wichtig, dass Sie DD.MM.JJJJ eingeben, d.h. getrennt von einem „-“ und das Jahr ausgeschrieben. 19.09.1990 wird akzeptiert, 19-09-1990 oder 19.09.90 nicht.\n" +
                "Adressdaten: Geben Sie die nachfolgenden Anschriftsdaten entsprechend Ihres Personalausweises an:\n" +
                "Straße\n" +
                "Hausnummer\n" +
                "PLZ\n" +
                "Ort/Stadt/Gemeinde\n" +
                "Land\n" +
                "Den Registrierungsprozess beenden Sie, indem Sie ein letztes Mal mit dem Button „Weiter“ bestätigen\n" +
                "Herzlichen Glückwünsch! Sie haben sich einen Account bei Coll@HBRS erstellt.\n");

        createGridRow("Wie kann ich mein „Unternehmen“ registrieren?", "Dein Konto können Sie in wenigen Minuten erstellen und die Registrierung bei Coll@HBRS\n" +
                "ist natürlich kostenlos. Nachfolgend erläutern wir Ihnen die einzelnen Schritte:\n" +
                "\n" +
                "Zunächst müssen Sie auf der Landing-Page auf den Button „Register“ klicken. Damit öffnet das Portal die Registrierungsfunktion.\n" +
                "Hierbei wählen Sie Unternehmen aus.\n" +
                "Anschließend fordert das System Sie auf, die folgenden Daten einzugeben:\n" +
                "UserID: Dient in Zukunft als Ihr Benutzername. Hiermit melden Sie sich beim Login im System an. Die UserID kann aus einer beliebigen 4-30 stelligen Ansammlung aus Buchstaben, Zeichen und Zahlen bestehen.\n" +
                "E-Mail: Dient zur Verifizierung Ihrer Person. Zu Beginn schicken wir Ihnen einmalig eine E-Mail, die Sie bestätigen müssen. Sobald Ihr Account in Betrieb ist, nutzen wir Ihre E-Mailadresse nur noch für freiwillige Sonderfunktionen, die Ihr Erlebnis verbessern sollen, z.B. um Ihnen Stellenvorschläge passend zu Ihren Vorlieben zu schicken oder im Falle eines Kennwortverlustes ein neues erstellen zu können.\n" +
                "Passwort: Dient in Zukunft als Ihr Passwort. Hiermit melden Sie sich beim Login im System an. Das Passwort muss eine mindestens 6-stellige Kombination sein und jeweils mindestens einen Buchstaben und eine Zahl beinhalten.\n" +
                "\n" +
                "Die Daten bestätigen Sie, indem Sie auf den Button „Weiter“ klicken.\n" +
                "Nun werden auch die unternehmensspezifischen Informationen von Ihnen abgefragt. Diese setzen sich zusammen aus:\n" +
                "Unternehmensnamen: Geben Sie hier den Namen des Unternehmens ein, dass Sie vertreten. Dies dient dazu Sie leichter zu finden, sowie es den Studierenden zu erleichtern zu wissen, bei wem sie sich bewerben.\n" +
                "Beschreibung: Hier können Sie eine umfangreiche Beschreibung Ihres Unternehmens angeben, damit die Studierenden wissen wer Sie sind und ob sie sich mit Ihrem Unternehmen identifizieren können.\n" +
                "Website: Hier können Sie den Link zu Ihrem offiziellem Webauftritt angeben, um interessierten Studierenden einen tieferen Einblick zu ermöglichen..\n" +
                "Adressdaten: Geben Sie die nachfolgenden Anschriftsdaten entsprechend Ihres Personalausweises an:\n" +
                "Straße\n" +
                "Hausnummer\n" +
                "PLZ\n" +
                "Ort/Stadt/Gemeinde\n" +
                "Land\n" +
                "Den Registrierungsprozess beenden Sie, indem Sie ein letztes Mal mit dem Button „Weiter“ bestätigen\n" +
                "Herzlichen Glückwünsch! Sie haben sich einen Account bei Coll@HBRS erstellt.\n");

        createGridRow("Wie kann ich mich in meinen Account einloggen?", "Da wir jedem Nutzer von Coll@HBRS dasselbe angenehme Nutzungserlebnis bieten wollen, haben wir den Log-In für Sie vereinfacht und ermöglichen es Unternehmern und Studenten gleichermaßen sich über die Anmeldemaske auf http://localhost:8080/collhbrs/#!login einloggen zu können. Dafür müssen Sie lediglich Ihren bei Ihrer Registrierung ausgewählten Benutzername (ID), sowie das von Ihnen bestimmte Passwort (Siehe Wie kann ich mein „Unternehmen“ registrieren? / Wie kann ich mich als Student registrieren?) Um die Anmeldung abzuschließen klicken Sie nun auf „Login“.\n");
    }

    private void createIsLoggedInContent() {
        createGridRow("Wie kann ich mich in meinen Account ausloggen?", "Den Auslogbutton finden Sie oben rechts in Ihrem Profil. Um die Auslogfunktion zu nutzen, klicken Sie auf diesen. Sie werden anschließend nach erfolgreichen Log-Out wieder auf die Startseite von Coll@HBRS geleitet. ");

        createGridRow("Wie kann ich meine hinterlegten Daten überprüfen?", "Um dir deine gespeicherten Daten anzusehen und anpassen zu können musst du oben im Menü auf \"Mein Profil gehen\". Hier werden dir deine persönlichen Informationen, welche relevant für Bewerbungen sind angezeigt. Beispielsweise dein Name, dein Studiengang und deine Fähigkeiten. Wir empfhlen dir regelmäßig zu kontrollieren, ob alles auf dem aktuellen Stand ist, damit du bei deinem zukünftigen Arbeitgeber von Anfang an einen guten Eindruck hinterlässt und es ihm erleichterst, sich für dich zu entscheiden.");
        createGridRow("Wie kann ich meine Daten ändern?", "");
        createGridRow("Studentenprofil", "Dafür reicht es in deinem Profil rechts oben auf das Bearbeitungssymbol(blauer Stift) zu klicken. Nun kannst du deine Daten updaten.");

        createGridRow("Wie kann ich meine hinterlegten Daten überprüfen?", "Um die über Ihr Unternehmen gespeicherten Daten anzusehen und anpassen zu können müssen Sie oben im Menü auf \"Mein Profil\" gehen. Hier werden Ihre hinterlegten Daten wie Website, Name, Ort, sowie auch Ihre inserierten Stellenangebote angezeigt  Wir empfehlen Ihnen regelmäßig zu kontrollieren, ob alles auf dem aktuellen Stand ist, damit sich auch Studierende, die sich für Ihren Bereich interssieren bewerben.");
        createGridRow("Wie kann ich meine Daten ändern?", "Dafür reicht es in Ihrem Profil rechts oben auf das Bearbeitungssymbol(blauer Stift) zu klicken. Nun könne Ihre Daten updaten.\n");

        createGridRow("Wie kann ich Stellenanzeigen inserieren?", "Um eine Stellenanzeige zu inserieren, klicken sie oben im Menü auf den Button \"Inserieren\". Anschließend öffnet sich die Inseratmaske. In dieser könne Sie alle wichtigen Daten zu Ihrer Stelle hinterlegen. Damit Sie leichter den Überblick behalten können, haben wir diese in 5 Registerblätter aufgeteilt. \n" +
                "\n" +
                "Beschreibung: In diesem Registerblatt werden die gröbsten Informationen zu Ihrer Anzeige abgefragt, aus dennen wir auch die undetaillierten Anzeigen in der Ergebnisliste der Suchfunktion der Studierenden erstellen. Achten Sie also darauf wichtige Schlagwörter zu nennen. Bei den Daten die abgefragt werden handelt es sich um die folgenden: \n" +
                "Titel: Hier sollen Sie den Titel Ihrer Stellenanzeige eingeben. Wir empfehlen Ihnen daher den Job z.B: Websitedesigner zu nennen\n" +
                "Beschreibung: Hier können Sie die Stelle genauer beschreiben.\n" +
                "Aufgaben: Hier können Sie die einzelnen Aufgaben, die der Studiernde im Rahmen seiner Tätigkeit ausüben soll nennen.\n" +
                "Anforderungen: Hier können Sie die einzelnen Anforderungen, die der Studiernde zur Ausübung seiner Tätigkeit haben soll nennen.\n" +
                "Vorteile: Hier können Sie die Vorteile nennen, die der Studierende im Unfang seiner Tätigkeit in Ihrem Unternehmen genießt nennen. Nutzen Sie die Gelegenheit sich ansprechend vor Interessenten zu positionieren und sich von der Konkurrenz abzuheben.\n" +
                "Tags: Nutzen Sie das Einbinden von Tags um es den Studierenden zu erleichtern Ihr Inserat zu finden.\n" +
                "Veröffentlichen: Hier können die Stellenanzeige abschließend über den Button \"veröffentlichen\" online stellen\n");
        createGridRow("Wo kann ich mir meine Stellenanzeigen ansehen?\n", "Sie können die Stellenanzeigen Ihres Unternehmens in Ihrem Profil, sowie dem Menüpunkt Stellen ansehen. Hierbei ist zu beachten, dass in Ihrem Profil lediglich der Überblick von Ihnen getätigten Inseraten dargestellt wird. Im Vergleich dazu zeigen Ihnen Ihre Inserate im Menüpunkt Stellen auch die entsprechenden Bewerbungen, die Sie zu der jeweiligen Anzeige erhalten haben. Hier können Sie auch entsprechend auf das Profil des Studierenden zugreifen.");
        createGridRow("Wie kann ich meine Daten löschen?", "Kommt noch.");

        createGridRow("Wie kann ich nach Stellenangeboten suchen?", "Da wir wissen, dass die Suchfunktion das wesentliche Feature für die Studenten bei unserer Anwendung ist, haben wir diese direkt auf der Startseite Ihres Profils hinterlegt, sowie ist jederzeit oben in Ihrem Webfenster eingebunden. Um die Suche zu starten, müssen Sie einfach ein oder mehrere Schlagwörter nach dennen gesucht werden soll in das dafür vorgesehene Feld eingeben. z.B. können sie nach Javaentwickler suchen, damit Ihnen entsprechende Stellenanzeigen herausgesucht werden. Um die Suche zu beginnen tippen Sie auf Enter oder klicken Sie einfach auf den \"Suchen\" Button. Schon durchforsten wir für Sie unser System nach geeigneten Stellenanzeigen. Übrigens: Sind Sie einmal in der Suchfunktion, können Sie schnell Ihre Suchbegriffe im oberen Teil der Maske anpassen, sowie Filtereinstellung auf die Anzeigen anwenden. Mehr dazu im Punkt \"Wie kann ich filtern?\"\n");
        createGridRow("Wie ist die Suchfunktion aufgebaut?", "Die Suchfunktion setzt sich zusammen aus dem Suchfeld(hier können Sie jederzeit neue Suchbegriffe eingeben, für Stellen, die Sie interessieren), der Filterfunktion(um die Ergbnisse auf dich zuzuschneiden z.B. per Wunschort) und dem Ergebnisfeld(in welchem alle ggf. gefilterten Einträge in unserem System aufgelistet sind, die zu deiner Suche passen) ");
        createGridRow("Wie kann ich filtern?", "Um die Filterfunktion von Coll@HBRS benutzen zu können, müssen Sie eine Suche starten (Siehe \"Wie kann ich nach Stellenangeboten suchen?\"). Anschließend werden Sie zur vollständigen Suchfunktion weitergeleitet. Hier können Sie auf die Ergebnisse entsprechende Filter anwenden, wie z.B Ort und Zeitraum(ab welchen Zeitpunkt). Durch eine entsprechende Bestätigung über den Filter Button werden die Ergebnisse entsprechend Ihren Wünschen angepasst, sodass garantiert nur Stellen angezeigt werden, die auch wirklich für dich interssant sind.");
        createGridRow("Wie kann ich mich bewerben?", "Dafür musst du zunächst eine Suche durchführen und ein für dich interessantes Stellenangebot auswählen. Anschließend wird dir rechts eine Detailansicht der Stelle angezeigt, welche genauere Infos beinhaltet. Sollte deine Interesse weiterhin ungebrochen sein, tippe unten auf Details. Hier werden nun sämtliche vom Unternehmen hinterlegten Informationen zu der Stelle angezeigt. Für eine Bewerbung kannst du einfach unser One-Klick-Feature in Anspruch nehmen, dass du unter der Unternehmensbeschreibung findest.");
        createGridRow("Wie kann ich eine Stelle speichern?", "Falls eine Stelle Ihre Interesse geweckt hat, aber Sie noch nicht vollkommen überzeugt sind, ermöglicht Ihnen unsere Favoritenoption die Speicherung des Stellenangebots in Ihrem Profil, damit Sie es nicht mehr aus den Augen verlieren können. Um eine Stelle zu favoritisieren müssen Sie einfach die Stelle anwählen und auf das Herzsymbol. Sie finden dieses Auch unter der Detailansicht neben der One-Klick-Bewerbungsfunktion. Sie sehen, ob Sie die Stelle erfolgreich favoritisiert haben, wenn das Herz blau ausgefüllt wurde.");
        createGridRow("Wie kann ich meine Favoriten aufrufen?", "Um Ihre Favoriten sehen zu können müssen Sie oben im Menü auf Bewerbungen gehen. \n" +
                "Die Stellen auf die Sie sich beworben haben, werden Ihnen unter \"Meine favoritisierten Stellen\" angezeigt\n");
        createGridRow("Wo kann ich meine Bewerbungen sehen?", "Um Ihre Bewerbungen sehen zu können müssen Sie oben im Menü auf Bewerbungen gehen. Die Stellen auf die Sie sich beworben haben, werden Ihnen unter \"Meine Bewerbungen\" angezeigt\n");

        createGridRow("Wie komme ich zu meiner Startseite zurück?\n", "Um erneut Ihre Startseite aufzurufen, reicht es oben Links auf unser Coll@HBRS Logo zu klicken.");
        createGridRow("Was bedeuten die Studentenprofile auf der Startseite meines Unternehmens?", "Man kann die Profile in zwei Gruppen einteilen. Die rechte Gruppe zeigt Ihnen aktuelle Bewerbungen auf Ihre Inserate an. Resultierend daraus entsteht die linke Gruppe. Diese symbolisiert alle Studierenden, die eine sehr hohe Ähnlichkeit zu den Studierenden aufweisen, welche Sie bereits eingestellt haben. Mit dieser einzigartigen Funktionen wollen wir es Ihnen erleichtern die perfekte Besetzung für Ihr Inserat zu finden.");

    }
}
