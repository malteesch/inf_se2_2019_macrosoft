package com.macrosoft.collhbrs.gui.views;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.CreateVacancyControl;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.gui.components.CreateVacancyComponent;
import com.macrosoft.collhbrs.gui.components.ExtensibleListComponent;
import com.macrosoft.collhbrs.gui.components.VacancyDetailsComponent;
import com.macrosoft.collhbrs.model.daos.CompanyDAO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.ui.*;

import java.time.LocalDate;
import java.util.Objects;

public class CreateVacancyView extends MainView implements View {

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        if (!Session.isLoggedIn()) {
            UI.getCurrent().getNavigator().navigateTo(Config.Views.LOGIN);
        } else {
            this.parentSetUp();
            this.setUp();
        }
    }

    public void setUp() {
        this.addStyleName("views");
        this.addStyleName("createvacancyview");
        this.setSizeFull();

        VerticalLayout pageBody = new VerticalLayout();
        pageBody.addStyleNames("search-components", "create-vacancy-view-body");
        this.addComponent(pageBody);

        // Page layout
        GridLayout pageGrid = new GridLayout(12, 12);
        pageGrid.setWidth("100%");
        pageBody.addComponent(pageGrid);
        pageGrid.addStyleName("create-vacancy-content-grid");

        // Page header
        HorizontalLayout pageHeader = new HorizontalLayout();
        Label pageTitle = new Label("Ausschreibung erstellen");
        pageTitle.addStyleName(MaterialTheme.LABEL_H4);

        Button cancelButton = new Button("Abbrechen", (Button.ClickListener) click -> {
            UI.getCurrent().getNavigator().navigateTo(Config.Views.MAIN);
        });
        cancelButton.addStyleName(MaterialTheme.BUTTON_DANGER);

        pageHeader.addComponent(pageTitle);
        pageHeader.addComponent(cancelButton);
        pageGrid.addComponent(pageHeader, 2, 0, 5, 0);


        // TabSheet components
        CreateVacancyComponent generalInfo = new CreateVacancyComponent();
        ExtensibleListComponent tasks = new ExtensibleListComponent("Aufgaben");
        ExtensibleListComponent requirements = new ExtensibleListComponent("Anforderungen");
        ExtensibleListComponent advantages = new ExtensibleListComponent("Vorteile");
        ExtensibleListComponent tags = new ExtensibleListComponent("Tags");


        // Build TabSheet
        TabSheet tabSheet = new TabSheet();
        tabSheet.addStyleNames("equal-width-tabs", "framed");
        tabSheet.addTab(generalInfo, "Beschreibung");
        tabSheet.addTab(tasks, "Aufgaben");
        tabSheet.addTab(requirements, "Profilanforderungen");
        tabSheet.addTab(advantages, "Vorteile");
        tabSheet.addTab(tags, "Tags");
        VerticalLayout publish = new VerticalLayout();

        tabSheet.addTab(publish, "Veröffentlichen");
        tabSheet.addSelectedTabChangeListener((TabSheet.SelectedTabChangeListener) event -> {
            TabSheet.Tab tab = event.getTabSheet().getTab(event.getTabSheet().getSelectedTab());
            if (tab.getCaption().equals("Veröffentlichen")) {
                VerticalLayout tabLayout = (VerticalLayout) tabSheet.getSelectedTab();
                tabLayout.removeAllComponents();

                try {
                    VacancyDTO dto = new VacancyDTO();
                    dto.setTitle(generalInfo.getTitleValue());
                    dto.setDescription(generalInfo.getDescriptionValue());
                    CompanyDTO company = new CompanyDAO().getOne(Objects.requireNonNull(Session.getCurrentUser()).getUsername());
                    dto.setCompany(company);
                    dto.setTags(tags.getEntries());
                    dto.setTasks(tasks.getEntries());
                    dto.setRequirements(requirements.getEntries());
                    dto.setAdvantages(advantages.getEntries());
                    dto.setDate(LocalDate.now());
                    Button publishButton = new Button("Veröffentlichen", (Button.ClickListener) click -> {
                        try {
                            CreateVacancyControl.publishVacancy(dto);
                            UI.getCurrent().getNavigator().navigateTo(Config.Views.MAIN);
                            new Notifier().createNotification("Ausschreibung \"" + generalInfo.getTitleValue() + "\" wurde veröffentlicht")
                                    .at(Position.TOP_CENTER)
                                    .withDelay(4000)
                                    .show();
                        } catch (DatabaseException e) {
                            new Notifier().createErrorNotification("Fehler: " + e.getMessage())
                                    .withDelay(5000)
                                    .at(Position.TOP_CENTER)
                                    .show();
                        }
                    });
                    publishButton.addStyleName(MaterialTheme.BUTTON_PRIMARY);
                    tabLayout.addComponent(new VacancyDetailsComponent(dto));
                    tabLayout.addComponent(publishButton);
                    tabLayout.setComponentAlignment(publishButton, Alignment.MIDDLE_CENTER);
                } catch (Exception e) {
                    e.printStackTrace();
                    // TODO
                }
            }
        });

        //Add TabSheet to page
        pageGrid.addComponent(tabSheet, 2, 1, 9, 11);

    }
}
