package com.macrosoft.collhbrs.gui.views;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.gui.components.LoginComponent;
import com.macrosoft.collhbrs.gui.components.NavbarComponent;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class LoginView extends VerticalLayout implements View {

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        if (Session.isLoggedIn()) {
            UI.getCurrent().getNavigator().navigateTo(Config.Views.MAIN);
        } else {
            this.setUp();
        }
    }

    public void setUp() {
        this.setSizeFull();
        this.setSpacing(false);
        this.addStyleName("loginview");

        NavbarComponent nav = null;
        try {
            nav = new NavbarComponent();
            nav.setHeight("90px");
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        this.addComponent(nav);

        LoginComponent login = new LoginComponent();
        this.addComponent(login);
    }
}

