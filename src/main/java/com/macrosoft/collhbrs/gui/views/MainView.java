package com.macrosoft.collhbrs.gui.views;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.gui.components.MainComponent;
import com.macrosoft.collhbrs.gui.components.NavbarComponent;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class MainView extends VerticalLayout implements View {
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        if (!Session.isLoggedIn()) {
            UI.getCurrent().getNavigator().navigateTo(Config.Views.LOGIN);
        } else {
            parentSetUp();
            setUpMain();
        }
    }

    void parentSetUp() {
        NavbarComponent nav = null;
        try {
            nav = new NavbarComponent();
            nav.setHeight("90px");
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        this.addComponent(nav);
        this.setSpacing(false);
    }

    private void setUpMain() {
        this.addStyleName("mainView");
        this.setSizeFull();
        MainComponent mainStudent = null;
        try {
            mainStudent = new MainComponent(Session.getCurrentRole());
        } catch (Exception e) {
            new Notifier().createErrorNotification("Die MainSeite konnte nicht angezeigt werden.")
                    .at(Position.TOP_CENTER)
                    .show();
            e.printStackTrace();
        }
        this.addComponent(mainStudent);
    }
}
