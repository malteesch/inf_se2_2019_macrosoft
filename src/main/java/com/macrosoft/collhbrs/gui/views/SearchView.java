package com.macrosoft.collhbrs.gui.views;

import com.macrosoft.collhbrs.control.SuggestionsControl;
import com.macrosoft.collhbrs.control.toggle.ToggleRouter;
import com.macrosoft.collhbrs.gui.components.ProfileHeaderComponent;
import com.macrosoft.collhbrs.gui.components.VacancyFilterComponent;
import com.macrosoft.collhbrs.gui.components.VacancyResultsComponent;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Session;
import com.macrosoft.collhbrs.services.utils.Utils;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

import java.util.ArrayList;
import java.util.List;

public class SearchView extends MainView implements View {

    private static List<VacancyDTO> currentResults, filteredResults;

    public static void setCurrentResults(List<VacancyDTO> currentResults) {
        SearchView.currentResults = currentResults;
    }

    public static List<VacancyDTO> getCurrentResults() { return SearchView.currentResults; }


    public static void setFilteredResults(List<VacancyDTO> filteredResults) {
        SearchView.filteredResults = filteredResults;
    }

    private static CompanyDTO companyHeader;

    public static void setHeaderCompany(CompanyDTO companyHeader) {
        SearchView.companyHeader = companyHeader;
    }

    private static boolean allVacancies = false;

    public static void setAllVacancies(boolean b) {
        allVacancies = b;
    }

    private static HorizontalLayout resultsLayout;


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        if (!Session.isLoggedIn()) {
            UI.getCurrent().getNavigator().navigateTo(Config.Views.LOGIN);
        } else {
            this.parentSetUp();
            this.setUp();
        }
    }

    private void setUp() {
        this.addStyleName("views");
        this.addStyleName("searchview");
        this.setSizeFull();

        GridLayout layout = new GridLayout();
        layout.addStyleName("searchview-gridlayout");
        layout.setSpacing(true);
        layout.setSizeFull();
        this.addComponent(layout);

        if (companyHeader.getName() != null) {
            HorizontalLayout compHorLayout = new HorizontalLayout();
            compHorLayout.setWidth("100%");
            layout.addComponent(compHorLayout);

            ProfileHeaderComponent company = new ProfileHeaderComponent(companyHeader);
            compHorLayout.addComponent(company);
        }

        if (currentResults.size() > 0) {
            HorizontalLayout filterHorLayout = new HorizontalLayout();
            filterHorLayout.setWidth("100%");
            layout.addComponent(filterHorLayout);

            VacancyFilterComponent filter = new VacancyFilterComponent();
            filterHorLayout.addComponent(filter);

            resultsLayout = new HorizontalLayout();
            resultsLayout.setWidth("100%");
            layout.addComponent(resultsLayout);
            String title = "Ergebnisse der Suche";
            if (allVacancies) title = "Alle Stellenangebote";
            reloadResults(currentResults, title);
        } else {
            HorizontalLayout noResultsLayout = Utils.createNoResultsWarning("Zu der angegebenen Suche konnten leider keine Stellen gefunden werden!");
            layout.addComponent(noResultsLayout);

            if ((currentResults.size() == 0) && (companyHeader.getName() == null) && ToggleRouter.featureIsEnabled(Config.Features.VACANCY_SUGGESTIONS)) {

                try {
                    StudentDTO currentStudent = new StudentDAO().getOne(Session.getCurrentUser().getUsername());
                    resultsLayout = new HorizontalLayout();
                    resultsLayout.setWidth("100%");
                    layout.addComponent(resultsLayout);
                    if (currentStudent.getInterests() == null || currentStudent.getSkills() == null) {
                        HorizontalLayout l = new HorizontalLayout();
                        l.addStyleName("search-components");
                        l.setWidth("80%");
                        Label noResults = new Label("Es konnten leider keine Stellvorschläge gefunden werden.");
                        noResults.setWidth("100%");
                        l.addComponent(noResults);
                        resultsLayout.addComponent(l);
                    } else {
                        reloadResults(SuggestionsControl.getVacancySuggestions(), "Vorgeschlagene Stellen");
                    }
                } catch (Exception e) {
                    e.printStackTrace(); //Ausgabe an den User bereits vorhanden
                }
            }
        }
    }

    public static void reloadResults(List<VacancyDTO> resultList, String title) {
        resultsLayout.removeAllComponents();
        VacancyResultsComponent results = null;
        try {
            results = new VacancyResultsComponent((ArrayList<VacancyDTO>) resultList, title);
        } catch (Exception e) {
            e.printStackTrace();
        }
        resultsLayout.addComponent(results);
    }
}
