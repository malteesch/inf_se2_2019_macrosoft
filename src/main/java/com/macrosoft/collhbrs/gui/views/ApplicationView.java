package com.macrosoft.collhbrs.gui.views;

import com.macrosoft.collhbrs.control.ApplicationControl;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.gui.components.VacancyResultsComponent;
import com.macrosoft.collhbrs.model.daos.CompanyDAO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Session;
import com.macrosoft.collhbrs.services.utils.Utils;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;

public class ApplicationView extends MainView implements View {
    private static ArrayList<VacancyDTO> currentResults;
    private static ArrayList<VacancyDTO> currentApplications = new ArrayList<>();
    private String title;
    private String appTitle;
    private GridLayout layout;
    private VerticalLayout favoritesLayout;

    public static ArrayList<VacancyDTO> getCurrentResults() {
        return currentResults;
    }
    private String getTitle() {
        return this.title;
    }
    private void setTitle(String s) {
        this.title = s;
    }
    public static void setCurrentResults(ArrayList<VacancyDTO> currentResults) {
        ApplicationView.currentResults = currentResults;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        if (!Session.isLoggedIn()) {
            UI.getCurrent().getNavigator().navigateTo(Config.Views.LOGIN);
        } else {
            this.parentSetUp();
            try {
                this.setUp();
            } catch (DatabaseException | SQLException | UserException e) {
                e.printStackTrace(); //TODO
            }
        }
    }

    public void setUp() throws DatabaseException, SQLException, UserException {
        this.layout = new GridLayout();
        this.layout.addStyleName("searchview-gridlayout");
        this.layout.setSpacing(true);
        this.layout.setSizeFull();

        this.addStyleName("views");
        this.addStyleNames("applicationview");
        this.setSizeFull();
        this.addComponent(layout);

        if (Session.getCurrentRole().equals(Config.Role.STUDENT)) {
            this.favoritesLayout = new VerticalLayout();
            this.favoritesLayout.setWidth("100%");
            this.layout.addComponent(favoritesLayout);
            setUpStudent();
        } else {
            try {
                setUpCompany();
            } catch (Exception e) {
                e.printStackTrace(); //TODO
            }
        }
    }

    private void setUpCompany() throws Exception {
        CompanyDTO companyDTO = new CompanyDAO().getOne(Objects.requireNonNull(Session.getCurrentUser()).getEmail());

        // offene Stellen
        ArrayList<VacancyDTO> myVacancies = ApplicationControl.getVacanciesForCompany(companyDTO.getCompanyId());
        setCurrentResults(myVacancies);
        this.setTitle("Meine offenen Stellen");
        VacancyResultsComponent results = new VacancyResultsComponent(getCurrentResults(), getTitle());
        layout.addComponent(results);

        // eingegangene Bewerbungen
        ArrayList<Integer> v = new ArrayList<>();
        assert myVacancies != null;
        for (VacancyDTO dto : myVacancies) {
            v.add(dto.getVacancyId());
        }
        ArrayList<VacancyDTO> allApplications = ApplicationControl.getApplications("SELECT * FROM \"macrosoft_collHBRS\".application_student;");
        ArrayList<VacancyDTO> finn = new ArrayList<>();
        for (VacancyDTO dto : allApplications) {
            if (v.contains(dto.getVacancyId())) {
                finn.add(dto);
            }
            currentApplications = finn;
        }
    }

    private void setUpStudent() throws SQLException, DatabaseException, UserException {
        // favorisierte Bewerbungen
        setCurrentResults(ApplicationControl.getAllfavoredApplications());
        if (currentResults.size() == 0) {
            HorizontalLayout noResultsLayout = Utils.createNoResultsWarning("Du hast noch keine Stellen favorisiert!");
            layout.addComponent(noResultsLayout);
        } else {
            this.setTitle("Meine favorisierten Stellen");
            VacancyResultsComponent results = null;
            try {
                results = new VacancyResultsComponent(getCurrentResults(), getTitle());
            } catch (Exception e) {
                e.printStackTrace();
            }
            favoritesLayout.addComponent(results);
        }

        // beworbene Stellen
        currentApplications = ApplicationControl.getAllApplications();
        if (currentApplications.size() == 0) {
            HorizontalLayout noResultsLayout = Utils.createNoResultsWarning("Du hast dich auf keine Stellen beworben!");
            layout.addComponent(noResultsLayout);
        } else {
            this.appTitle = "Meine Bewerbungen";
            VacancyResultsComponent res = null;
            try {
                res = new VacancyResultsComponent(currentApplications, appTitle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            favoritesLayout.addComponent(res);
        }
    }
}
