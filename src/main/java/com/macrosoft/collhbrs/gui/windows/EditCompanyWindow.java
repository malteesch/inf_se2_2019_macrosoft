package com.macrosoft.collhbrs.gui.windows;

import com.macrosoft.collhbrs.control.upload.FileUploader;
import com.macrosoft.collhbrs.gui.views.ProfileView;
import com.macrosoft.collhbrs.model.daos.CompanyDAO;
import com.macrosoft.collhbrs.model.dtos.AddressDTO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.macrosoft.collhbrs.services.utils.PasswordValidator;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.data.Binder;
import com.vaadin.data.BindingValidationStatus;
import com.vaadin.data.HasValue;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.util.Objects;

public class EditCompanyWindow extends Window {
    private CompanyDTO dto;

    private Binder<UserDTO> ubinder = new Binder<>();
    private Binder<AddressDTO> abinder = new Binder<>();
    private Binder<CompanyDTO> cbinder = new Binder<>();

    private Binder.Binding<UserDTO, String> passwortBinding;
    private Binder.Binding<UserDTO, String> bestaetigePasswortBinding;

    // Show uploaded file in this placeholder
    static Image profilepic = new Image("Kein Bild ausgewählt");

    // Set uploaded file to placeholder
    public static void setProfilepic(FileResource file) {
        profilepic.setSource(file);
    }

    public EditCompanyWindow(CompanyDTO dto) throws Exception {
        this.dto = dto;
        this.setUp();
    }

    private void setUp() throws Exception {

        CompanyDTO company = dto;
        AddressDTO address = this.dto.getAddress();

        VerticalLayout vl = new VerticalLayout();
        this.setContent(vl);
        this.center();
        this.setWidth("75%");


        GridLayout tabInfUnt = new GridLayout(4, 23);
        vl.addComponent(tabInfUnt);
        tabInfUnt.setWidth("95%");
        tabInfUnt.setColumnExpandRatio(0, 1);
        tabInfUnt.setColumnExpandRatio(1, 2);
        tabInfUnt.setColumnExpandRatio(2, 1);
        tabInfUnt.setColumnExpandRatio(3, 2);

        Label space0 = new Label("<p>&nbsp;</p>", ContentMode.HTML);
        tabInfUnt.addComponent(space0, 0, 0);

        Label lcompanyname = new Label("Name Ihrer Firma", ContentMode.HTML);
        lcompanyname.addStyleName(ValoTheme.LABEL_H4);
        TextField tfcompanyname = new TextField();
        cbinder.forField(tfcompanyname)
                .asRequired(new StringLengthValidator("Der Name Ihre Firma muss zwischen 3 und 60 Zeichen haben", 3, 60))
                .bind(CompanyDTO::getName, CompanyDTO::setName);
        tfcompanyname.setValue(company.getName());
        tabInfUnt.addComponent(lcompanyname, 0, 1, 1, 1);
        tabInfUnt.addComponent(tfcompanyname, 2, 1, 3, 1);
        tfcompanyname.setWidth("90%");
        tabInfUnt.setComponentAlignment(lcompanyname, Alignment.MIDDLE_CENTER);

        Label space1 = new Label("<p>&nbsp;</p>", ContentMode.HTML);
        tabInfUnt.addComponent(space1, 0, 2);

        Label lcompanydescription = new Label("Beschreibung Ihrer Firma", ContentMode.HTML);
        lcompanydescription.addStyleName(ValoTheme.LABEL_BOLD);
        RichTextArea rtacompanydescription = new RichTextArea();
        rtacompanydescription.setValue(company.getDescription());
        tabInfUnt.addComponent(lcompanydescription, 0, 3);
        tabInfUnt.addComponent(rtacompanydescription, 0, 4, 3, 4);
        rtacompanydescription.setWidth("90%");
        tabInfUnt.setComponentAlignment(rtacompanydescription, Alignment.MIDDLE_CENTER);

        Label space2 = new Label("<p>&nbsp;</p>", ContentMode.HTML);
        tabInfUnt.addComponent(space2, 0, 5);

        Label lemail = new Label("Email Ihres Users", ContentMode.HTML);
        TextField tfemail = new TextField();
        cbinder.forField(tfemail)
                .asRequired(new EmailValidator("Das sieht nicht nach einer gültigen E-Mail aus."))
                .bind(UserDTO::getEmail, UserDTO::setEmail);
        tfemail.setValue(company.getEmail());
        tabInfUnt.addComponent(lemail, 0, 6);
        tabInfUnt.addComponent(tfemail, 0, 7, 1, 7);
        tfemail.setWidth("90%");

        Label lwebseite = new Label("Website Ihrer Firma", ContentMode.HTML);
        lwebseite.addStyleName(ValoTheme.LABEL_BOLD);
        TextField tfwebseite = new TextField();
        tfwebseite.setValue(company.getWebsite());
        tabInfUnt.addComponent(lwebseite, 2, 6);
        tabInfUnt.addComponent(tfwebseite, 2, 7, 3, 7);
        tfwebseite.setWidth("90%");

        Label space3 = new Label("<p>&nbsp;</p>", ContentMode.HTML);
        tabInfUnt.addComponent(space3, 0, 8);

        Label apassword = new Label("MÖCHTEN SIE IHR PASSWORT ÄNDERN?", ContentMode.HTML);
        apassword.addStyleName(ValoTheme.LABEL_H4);
        tabInfUnt.addComponent(apassword, 0, 9, 3, 9);

        Label space4 = new Label("<p>&nbsp;</p>", ContentMode.HTML);
        tabInfUnt.addComponent(space4, 0, 10);

        Label lpassword1 = new Label("Schreiben Sie ein neues Passwort", ContentMode.HTML);
        lpassword1.addStyleName(ValoTheme.LABEL_BOLD);
        TextField tfpassword1 = new TextField();
        tabInfUnt.addComponent(lpassword1, 0, 11);
        tabInfUnt.addComponent(tfpassword1, 0, 12, 1, 12);
        tfpassword1.setWidth("90%");

        Label lpassword2 = new Label("Wiederholen Sie das Passwort", ContentMode.HTML);
        lpassword2.addStyleName(ValoTheme.LABEL_BOLD);
        TextField tfpassword2 = new TextField();
        passwortBinding = ubinder.forField(tfpassword1)
                .asRequired(new PasswordValidator())
                .bind(UserDTO::getPassword, UserDTO::setPassword);

        tfpassword1.addValueChangeListener(event -> bestaetigePasswortBinding.validate());

        bestaetigePasswortBinding = ubinder.forField(tfpassword2)
                .asRequired(Validator.from(this::validateConfirmPasswd,
                        "Passwörter müssen übereinstimmen!"))
                .bind(UserDTO::getPassword, (person, pwd) -> {
                });
        tabInfUnt.addComponent(lpassword2, 2, 11);
        tabInfUnt.addComponent(tfpassword2, 2, 12, 3, 12);
        tfpassword2.setWidth("90%");

        Label space5 = new Label("<p>&nbsp;</p>", ContentMode.HTML);
        tabInfUnt.addComponent(space5, 0, 13);

        Label laddress = new Label("ADDRESSE", ContentMode.HTML);
        laddress.addStyleName(ValoTheme.LABEL_H4);
        tabInfUnt.addComponent(laddress, 0, 14, 3, 14);

        Label space6 = new Label("<p>&nbsp;</p>", ContentMode.HTML);
        tabInfUnt.addComponent(space6, 0, 15);

        Label lstreet = new Label("Strasse Ihrer Firma", ContentMode.HTML);
        lstreet.addStyleName(ValoTheme.LABEL_BOLD);
        TextField tfstreet = new TextField();
        abinder.forField(tfstreet)
                .asRequired(new StringLengthValidator("Die Strasse Ihrer Firma muss zwischen 2 und 50 Zeichen haben", 2, 50))
                .bind(AddressDTO::getStreet, AddressDTO::setStreet);
        tfstreet.setValue(address.getStreet());
        tabInfUnt.addComponent(lstreet, 0, 16);
        tabInfUnt.addComponent(tfstreet, 1, 16, 3, 16);
        tfstreet.setWidth("90%");
        tabInfUnt.setComponentAlignment(lstreet, Alignment.MIDDLE_LEFT);

        Label space7 = new Label("<p>&nbsp;</p>", ContentMode.HTML);
        tabInfUnt.addComponent(space7, 0, 17);

        Label lhousenumber = new Label("Hausnummer Ihrer Firma", ContentMode.HTML);
        lhousenumber.addStyleName(ValoTheme.LABEL_BOLD);
        TextField tfhousenumber = new TextField();
        abinder.forField(tfhousenumber)
                .asRequired(new StringLengthValidator("Die Hausnummer Ihrer Firma muss zwischen 1 und 4 Zeichen haben", 1, 4))
                .bind(AddressDTO::getHousenumber, AddressDTO::setHousenumber);
        tfhousenumber.setValue(address.getHousenumber());
        tabInfUnt.addComponent(lhousenumber, 0, 18);
        tabInfUnt.addComponent(tfhousenumber, 1, 18);
        tfhousenumber.setWidth("90%");
        tabInfUnt.setComponentAlignment(lhousenumber, Alignment.MIDDLE_LEFT);

        Label lpostalcode = new Label("Postleitzahl Ihrer Firma", ContentMode.HTML);
        lpostalcode.addStyleName(ValoTheme.LABEL_BOLD);
        TextField tfpostalcode = new TextField();
        abinder.forField(tfpostalcode)
                .asRequired(new StringLengthValidator("Die Postleitzahl Ihrer Firma muss 5 Zeichen haben", 5, 5))
                .bind(AddressDTO::getPostalcode, AddressDTO::setPostalcode);
        tfpostalcode.setValue(address.getPostalcode());
        tabInfUnt.addComponent(lpostalcode, 2, 18);
        tabInfUnt.addComponent(tfpostalcode, 3, 18);
        tfpostalcode.setWidth("90%");
        tabInfUnt.setComponentAlignment(lpostalcode, Alignment.MIDDLE_LEFT);

        Label space8 = new Label("<p>&nbsp;</p>", ContentMode.HTML);
        tabInfUnt.addComponent(space8, 0, 19);

        Label lcity = new Label("Stadt Ihrer Firma", ContentMode.HTML);
        lcity.addStyleName(ValoTheme.LABEL_BOLD);
        TextField tfcity = new TextField();
        abinder.forField(tfcity)
                .asRequired(new StringLengthValidator("Dder Name der Stadt Ihrer Firma muss zwischen 2 und 30 Zeichen haben", 2, 30))
                .bind(AddressDTO::getCity, AddressDTO::setCity);
        tfcity.setValue(address.getCity());
        tabInfUnt.addComponent(lcity, 0, 20);
        tabInfUnt.addComponent(tfcity, 1, 20);
        tfcity.setWidth("90%");
        tabInfUnt.setComponentAlignment(lcity, Alignment.MIDDLE_LEFT);

        Label lcountry = new Label("Land Ihrer Firma", ContentMode.HTML);
        lcountry.addStyleName(ValoTheme.LABEL_BOLD);
        TextField tfcountry = new TextField();
        abinder.forField(tfcountry)
                .asRequired(new StringLengthValidator("Der Name des Landes Ihrer Firma muss zwischen 2 und 30 Zeichen haben", 2, 30))
                .bind(AddressDTO::getCountry, AddressDTO::setCountry);
        tfcountry.setValue(address.getCountry());
        tabInfUnt.addComponent(lcountry, 2, 20);
        tabInfUnt.addComponent(tfcountry, 3, 20);
        tfcountry.setWidth("90%");
        tabInfUnt.setComponentAlignment(lcountry, Alignment.MIDDLE_LEFT);

        Label space9 = new Label("<p>&nbsp;</p>", ContentMode.HTML);
        tabInfUnt.addComponent(space9, 0, 21);

        FileUploader receiver;
        receiver = new FileUploader();
        Upload upload_photo = new Upload("", receiver);
        //VaadinIcons.FILE_PICTURE);
        upload_photo.setButtonCaption("Logo hochladen");
        upload_photo.addSucceededListener(receiver);
        tabInfUnt.addComponent(upload_photo, 0, 22);
        tabInfUnt.setComponentAlignment(upload_photo, Alignment.BOTTOM_CENTER);

        Button bl = new Button("Profil löschen", VaadinIcons.TRASH);
        tabInfUnt.addComponent(bl, 2, 22);
        tabInfUnt.setComponentAlignment(bl, Alignment.BOTTOM_CENTER);
        bl.setWidth("100%");

        Button bs = new Button("Speichern", VaadinIcons.ARCHIVES);
        tabInfUnt.addComponent(bs, 3, 22);
        tabInfUnt.setComponentAlignment(bs, Alignment.BOTTOM_RIGHT);
        bs.setWidth("100%");
        bs.addClickListener(clickEvent -> {

            boolean boo = true;

            CompanyDTO c = new CompanyDTO();
            c.setUserid(dto.getUserid());
            c.setUsername(dto.getUsername());
            c.setEmail(tfemail.getValue());
            if (!tfpassword1.getValue().equals("")) {
                c.setPassword(tfpassword1.getValue());
            } else {
                c.setPasswordAlreadyHashed(null);
            }
            c.setImage(Session.getCurrentUser().getImage());

            AddressDTO a = new AddressDTO();
            a.setStreet(tfstreet.getValue());
            a.setHousenumber(tfhousenumber.getValue());
            a.setPostalcode(tfpostalcode.getValue());
            a.setCity(tfcity.getValue());
            a.setCountry(tfcountry.getValue());
            a.setAddressid(Session.getCurrentUser().getAddress().getAddressid());

            c.setName(tfcompanyname.getValue());
            c.setDescription(rtacompanydescription.getValue());
            c.setWebsite(tfwebseite.getValue());
            c.setCompanyId(dto.getCompanyId());

            try {
                abinder.writeBean(a);
                //cbinder.writeBean(c);
                c.setAddress(a);
                CompanyDAO cdao = new CompanyDAO();
                CompanyDTO dto = cdao.updateOne(c);

            } catch (Exception e) {
                boo = false;
                e.printStackTrace();
            }
            if (!boo) {
                new Notifier().createErrorNotification("Ihre Daten wurden nicht gespeichert!")
                        .at(Position.TOP_CENTER)
                        .show();
            } else {
                this.close();
                new Notifier().createNotification("Ihre Daten wurden erfolgreich gespeichert!")
                        .at(Position.TOP_CENTER)
                        .show();
            }
            ProfileView.setCompany(c);
            UI.getCurrent().getNavigator().navigateTo(Config.Views.PROFILE);
        });

    }

    private boolean validateConfirmPasswd(String confirmPasswordValue) {
        boolean zeigePasswortStatus = false;
        if (confirmPasswordValue.isEmpty()) {
            return true;

        }
        BindingValidationStatus<String> status = passwortBinding.validate();
        if (status.isError()) {
            return true;
        }
        zeigePasswortStatus = true;
        HasValue<?> pwdField = passwortBinding.getField();
        return Objects.equals(pwdField.getValue(), confirmPasswordValue);
    }

}
