package com.macrosoft.collhbrs.gui.windows;

import com.macrosoft.collhbrs.gui.components.ProfileHeaderComponent;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.services.utils.Utils;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.osgi.resources.impl.VaadinResourceServiceImpl;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.*;


public class RateWindow extends Window {

    public RateWindow(CompanyDTO dto,ProfileHeaderComponent p){setup(dto,p);}

    private void setup(CompanyDTO dto,ProfileHeaderComponent p) {

        VerticalLayout layout = new VerticalLayout();
        this.setContent(layout);
        layout.setSizeFull();
        this.setHeight("45%");
        this.setWidth("30%");
        this.center();
        ComboBox<String> c = getStars(dto);
        TextArea input = new TextArea("Nachricht (optional):");
        input.setWidth("100%");
        input.setHeight("100%");
        Button b = new Button("Bewerten");
        layout.addComponent(c);
        layout.addComponentsAndExpand(input);
        layout.addComponent(b);
        b.addClickListener(event -> {
            Utils.rate(Integer.valueOf(c.getValue()),dto);
            this.close();
            p.refreshRating(dto);
            Notification notif = new Notification("Bewertung erfolgreich abgegeben!");
            notif.setPosition(Position.BOTTOM_CENTER);
            notif.setDelayMsec(5000);
            notif.show(Page.getCurrent());
        });

    }

    private ComboBox<String> getStars(CompanyDTO dto){
       ComboBox<String> combobox = new ComboBox<>("Bitte wählen Sie mit wievielen Punkten Sie "+dto.getName()+" bewerten wollen:");
       combobox.setItems("0","1","2","3","4","5");
       combobox.setTextInputAllowed(false);
       combobox.setEmptySelectionAllowed(false);
       combobox.setSelectedItem("0");
       return combobox;

    }
}
