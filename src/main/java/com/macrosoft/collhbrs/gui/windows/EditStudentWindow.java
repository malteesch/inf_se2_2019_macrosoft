package com.macrosoft.collhbrs.gui.windows;

import com.macrosoft.collhbrs.control.upload.FileUploader;
import com.macrosoft.collhbrs.gui.components.ExtensibleListComponent;
import com.macrosoft.collhbrs.gui.views.ProfileView;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.dtos.AddressDTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.macrosoft.collhbrs.services.utils.PasswordValidator;
import com.macrosoft.collhbrs.services.utils.Session;
import com.macrosoft.collhbrs.services.utils.Utils;
import com.vaadin.data.*;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.Position;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EditStudentWindow extends Window {
    private StudentDTO dto;
    private Binder<UserDTO> binder = new Binder<>();
    private Binder<AddressDTO> AdressBinder = new Binder<>();

    private Binder.Binding<UserDTO, String> passwortBinding;
    private Binder.Binding<UserDTO, String> bestaetigePasswortBinding;


    public EditStudentWindow(StudentDTO dto) {
        this.dto = dto;
        this.setUp();
    }

    // Show uploaded file in this placeholder
    private static ThemeResource resource = new ThemeResource("img/Portrait_Placeholder.png");
    private static Image profilepic = new Image("Kein Bild ausgewählt", resource);

    private static Panel profilePicPanel = new Panel();

    // Set uploaded file to placeholder
    public static void refreshProfilePic(Image img) {
        if (img != null) {
            profilepic = img;
            profilepic.setCaption("");
        }
        profilepic.setWidth("200px");
        profilePicPanel.setContent(profilepic);
        profilePicPanel.setWidth("200px");
        profilePicPanel.addStyleName("profilepicpanel");
    }


    private void setUp() {

        AddressDTO address = this.dto.getAddress();
        GridLayout grid = new GridLayout(4, 21);
        grid.setWidth("100%");
        grid.setSpacing(true);
        grid.addStyleName("scrollable");
        grid.addStyleName("gridPadding");
        this.setContent(grid);
        this.setWidth("75%");
        this.center();
        // interessen, fähigkeiten, werdegang, ausbildung, auszeichnungen, dokumente


        // Allgemeines
        Label allgemein = new Label("Allgemeines");
        allgemein.addStyleName(ValoTheme.LABEL_H4);
        grid.addComponent(allgemein, 0, 0);
        // Email
        Label emailLabel = new Label("E-Mail-Adresse");
        TextField emailTf = new TextField();
        emailTf.setValue(this.dto.getEmail());
        emailTf.setWidth("100%");
        grid.addComponent(emailLabel, 0, 1);
        grid.addComponent(emailTf, 1, 1, 3, 1);
        grid.setComponentAlignment(emailLabel, Alignment.MIDDLE_CENTER);
        binder.forField(emailTf)
                .asRequired(new EmailValidator("Das sieht nicht nach einer gültigen E-Mail aus."))
                .bind(UserDTO::getEmail, UserDTO::setEmail);
        emailTf.setRequiredIndicatorVisible(false);
        // Passwort

        Label pwLabel = new Label("Passwort");
        Label pwLabel2 = new Label("Wiederholen");
        PasswordField pwTf = new PasswordField();
        PasswordField pwTf2 = new PasswordField();

        pwTf.setWidth("100%");
        pwTf2.setWidth("100%");
        grid.addComponent(pwLabel, 0, 2);
        grid.addComponent(pwLabel2, 2, 2);
        grid.addComponent(pwTf, 1, 2);
        grid.addComponent(pwTf2, 3, 2);
        grid.setComponentAlignment(pwLabel, Alignment.MIDDLE_CENTER);
        grid.setComponentAlignment(pwLabel2, Alignment.MIDDLE_CENTER);
        passwortBinding = binder.forField(pwTf)
                .asRequired(new PasswordValidator())
                .bind(UserDTO::getPassword, UserDTO::setPassword);
        pwTf.addValueChangeListener(event -> bestaetigePasswortBinding.validate());
        bestaetigePasswortBinding = binder.forField(pwTf2)
                .asRequired(Validator.from(this::validateConfirmPasswd,
                        "Passwörter müssen übereinstimmen!"))
                .bind(UserDTO::getPassword, (person, pwd) -> {
                });
        pwTf.setRequiredIndicatorVisible(false);
        pwTf2.setRequiredIndicatorVisible(false);


        // Studienfach
        Label fachLabel = new Label("Studiengang");
        TextField fachTf = new TextField();
        fachTf.setPlaceholder("Keine Angaben vorhanden");
        if (this.dto.getMajor() != null && !(this.dto.getMajor().isEmpty())) {
            fachTf.setValue(this.dto.getMajor());
        }
        fachTf.setWidth("100%");
        grid.addComponent(fachLabel, 0, 3);
        grid.addComponent(fachTf, 1, 3, 3, 3);
        grid.setComponentAlignment(fachLabel, Alignment.MIDDLE_CENTER);


        // Spezialisierung
        Label schwerpunktLabel = new Label("Schwerpunkt");
        TextField schwerpunktTf = new TextField();
        schwerpunktTf.setPlaceholder("Keine Angaben vorhanden");
        if (this.dto.getSpecalization() != null && !(this.dto.getSpecalization().isEmpty())) {
            schwerpunktTf.setValue(this.dto.getSpecalization());
        }
        schwerpunktTf.setWidth("100%");
        grid.addComponent(schwerpunktLabel, 0, 4);
        grid.addComponent(schwerpunktTf, 1, 4, 3, 4);
        grid.setComponentAlignment(schwerpunktLabel, Alignment.MIDDLE_CENTER);

        // FOTO


        //DATEI-UPLOAD FOTO
        FileUploader receiver;
        receiver = new FileUploader();
        // Beschriftung und Upload-Button
        Label label_photo = new Label("Profilbild");
        Upload upload_photo = new Upload("", receiver);
        //upload_photo.setImmediateMode(false);
        upload_photo.setButtonCaption("Hochladen");
        upload_photo.addSucceededListener(receiver);
        upload_photo.setAcceptMimeTypes("image/*");

        if (dto.getImage() != null && dto.getImage().length > 0) {
            refreshProfilePic(Utils.convertToImage(dto.getImage()));
        } else {
            refreshProfilePic(new Image("", resource));
        }

        GridLayout picLayout = new GridLayout(1, 2);
        picLayout.setSpacing(false);
        picLayout.addComponent(profilePicPanel, 0, 0);
        picLayout.addComponent(upload_photo, 0, 1);
        picLayout.setComponentAlignment(upload_photo, Alignment.MIDDLE_RIGHT);

        grid.addComponent(label_photo, 0, 5);
        grid.addComponent(picLayout, 1, 5);
        grid.setComponentAlignment(label_photo, Alignment.MIDDLE_CENTER);
        grid.setSpacing(true);

        //Adresse
        Label laddress = new Label("Adresse");
        laddress.addStyleName(ValoTheme.LABEL_H4);
        grid.addComponent(laddress, 0, 6);
        Label lstreet = new Label("Straße");
        TextField tfstreet = new TextField();
        AdressBinder.forField(tfstreet)
                .asRequired("Bitte geben Sie Ihre Straße an.")
                .bind(AddressDTO::getStreet, AddressDTO::setStreet);
        tfstreet.setRequiredIndicatorVisible(false);
        tfstreet.setWidth("100%");
        tfstreet.setValue(address.getStreet());
        grid.addComponent(lstreet, 0, 7);
        grid.addComponent(tfstreet, 1, 7, 1, 7);
        grid.setComponentAlignment(lstreet, Alignment.MIDDLE_CENTER);


        Label lhousenumber = new Label("Hausnummer");
        TextField tfhousenumber = new TextField();
        AdressBinder.forField(tfhousenumber)
                .asRequired("Bitte geben Sie Ihre Hausnummer an.")
                .bind(AddressDTO::getHousenumber, AddressDTO::setHousenumber);
        tfhousenumber.setRequiredIndicatorVisible(false);
        tfhousenumber.setValue(address.getHousenumber());
        grid.addComponent(lhousenumber, 2, 7);
        grid.addComponent(tfhousenumber, 3, 7);
        grid.setComponentAlignment(lhousenumber, Alignment.MIDDLE_CENTER);


        Label lpostalcode = new Label("Postleitzahl");
        TextField tfpostalcode = new TextField();
        AdressBinder.forField(tfpostalcode)
                .asRequired("Bitte geben Sie Ihre Straße an.")
                .bind(AddressDTO::getPostalcode, AddressDTO::setPostalcode);
        tfpostalcode.setRequiredIndicatorVisible(false);
        tfpostalcode.setValue(address.getPostalcode());
        grid.addComponent(lpostalcode, 0, 8);
        grid.addComponent(tfpostalcode, 1, 8);
        grid.setComponentAlignment(lpostalcode, Alignment.MIDDLE_CENTER);


        Label lcity = new Label("Stadt");
        TextField tfcity = new TextField();
        AdressBinder.forField(tfcity)
                .asRequired("Bitte geben Sie Ihre Stadt ein.")
                .bind(AddressDTO::getCity, AddressDTO::setCity);
        tfcity.setRequiredIndicatorVisible(false);
        tfcity.setValue(address.getCity());
        grid.addComponent(lcity, 2, 8);
        grid.addComponent(tfcity, 3, 8);
        grid.setComponentAlignment(lcity, Alignment.MIDDLE_CENTER);


        Label lcountry = new Label("Land");
        TextField tfcountry = new TextField();
        AdressBinder.forField(tfcountry)
                .asRequired("Bitte geben Sie Ihr Land ein.")
                .bind(AddressDTO::getCountry, AddressDTO::setCountry);
        tfcountry.setRequiredIndicatorVisible(false);
        tfcountry.setValue(address.getCountry());
        grid.addComponent(lcountry, 0, 9);
        grid.addComponent(tfcountry, 1, 9);
        grid.setComponentAlignment(lcountry, Alignment.MIDDLE_CENTER);


        // Dokumente, persönliches, etc
        Label lDocument = new Label("Lebenslauf");
        lDocument.addStyleName(ValoTheme.LABEL_H4);
        grid.addComponent(lDocument, 0, 10);

        Upload upload_doc = new Upload("", receiver);
        upload_doc.setButtonCaption("Hochladen");
        upload_doc.addSucceededListener(receiver);
        upload_doc.setAcceptMimeTypes("application/pdf");
        grid.addComponent(upload_doc, 1, 11);
        grid.setComponentAlignment(upload_doc, Alignment.MIDDLE_LEFT);

        Label lDocNotice = new Label("Nur PDF Dateien möglich");
        grid.addComponent(lDocNotice, 0, 11);
        grid.setComponentAlignment(lDocNotice, Alignment.MIDDLE_CENTER);


        // Skills
        ExtensibleListComponent elc;
        if (dto.getSkills() != null) {
            ArrayList<String> skills = dto.getSkills();
            elc = new ExtensibleListComponent("Fähigkeiten", skills);
        } else {
            elc = new ExtensibleListComponent("Fähigkeiten");
        }
        grid.addComponent(elc, 0, 12, 1, 12);


        // Interests
        ExtensibleListComponent elc2;
        if (dto.getInterests() != null) {
            ArrayList<String> interests = dto.getInterests();
            elc2 = new ExtensibleListComponent("Interessen", interests);
        } else {
            elc2 = new ExtensibleListComponent("Interessen");
        }
        grid.addComponent(elc2, 2, 12, 3, 12);

        Button close = new Button("Schließen");
        Button submit = new Button("Speichern");

        close.addClickListener((Button.ClickListener) event -> {
                    this.setVisible(false);
                }
        );
        submit.addClickListener((Button.ClickListener) event -> {
                    this.setVisible(false);
                }
        );

        submit.addClickListener(clickEvent -> {
            boolean isValid = true;
            StudentDTO studentDTO = new StudentDTO();
            studentDTO.setUsername(dto.getUsername());
            studentDTO.setUserid(dto.getUserid());
            studentDTO.setEmail(emailTf.getValue());
            if (pwTf.getValue().equals("")) {
                studentDTO.setPasswordAlreadyHashed(dto.getPassword());
            } else if (pwTf.getValue().equals(pwTf2.getValue())) {
                studentDTO.setPassword(pwTf.getValue());
            } else {
                isValid = false;
            }
            studentDTO.setMajor(fachTf.getValue());
            studentDTO.setSpecalization(schwerpunktTf.getValue());
            AddressDTO a = new AddressDTO();
            if (!tfcity.getValue().equals("")) {
                a.setCity(tfcity.getValue());

            } else {
                isValid = false;
            }
            if (!tfcountry.getValue().equals("")) {
                a.setCountry(tfcountry.getValue());
            } else {
                isValid = false;
            }
            if (!tfhousenumber.getValue().equals("")) {
                a.setHousenumber(tfhousenumber.getValue());
            } else {
                isValid = false;
            }
            if (!tfpostalcode.getValue().equals("")) {
                a.setPostalcode(tfpostalcode.getValue());
            } else {
                isValid = false;
            }
            if (!tfstreet.getValue().equals("")) {
                a.setStreet(tfstreet.getValue());
            } else {
                isValid = false;
            }
            List<String> newSkills = elc.getEntries();
            List<String> newInterests = elc2.getEntries();
            studentDTO.setSkills((ArrayList<String>) newSkills);
            studentDTO.setInterests((ArrayList<String>) newInterests);
            a.setAddressid(Session.getCurrentUser().getAddress().getAddressid());
            studentDTO.setImage(Session.getCurrentUser().getImage());
            studentDTO.setAddress(a);
            try {
                if (isValid) {
                    StudentDAO add = new StudentDAO();
                    StudentDTO dto = add.updateOne(studentDTO);
                    Session.setCurrentUser(dto);
                    ProfileView.setStudent(dto);
                    // der Zeit wegen nicht dynamisch
                    Page.getCurrent().reload();
                    new Notifier().createNotification("Änderungen erfolgreich übernommen")
                            .at(Position.TOP_CENTER)
                            .show();
                }


            } catch (ValidationException e) {
                isValid = false;
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!isValid) {
                new Notifier().createWarningNotification("Ein oder mehrere Felder enthalten invalide Eingaben.")
                        .at(Position.TOP_CENTER)
                        .show();
            }
        });
        close.addStyleName(ValoTheme.BUTTON_DANGER);
        submit.addStyleName(ValoTheme.BUTTON_FRIENDLY);
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.addComponent(close);
        buttons.addComponent(submit);
        grid.addComponent(buttons, 3, 20);
        buttons.setComponentAlignment(close, Alignment.MIDDLE_RIGHT);
        buttons.setComponentAlignment(submit, Alignment.MIDDLE_RIGHT);
    }

    private boolean validateConfirmPasswd(String confirmPasswordValue) {
        if (confirmPasswordValue.isEmpty()) {
            return true;

        }
        BindingValidationStatus<String> status = passwortBinding.validate();
        if (status.isError()) {
            return true;
        }
        HasValue<?> pwdField = passwortBinding.getField();
        return Objects.equals(pwdField.getValue(), confirmPasswordValue);
    }
}
