package com.macrosoft.collhbrs.gui.windows;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.util.concurrent.TimeUnit;

public class NachrichtWindow extends Window {

    public NachrichtWindow() {
        this.setUp();
    }

    public void setUp() {
        GridLayout gl = new GridLayout(3, 3);
        VerticalLayout vl = new VerticalLayout();
        this.setContent(vl);
        this.center();
        this.setWidth("75%");

        vl.addComponent(gl);

        Label l = new Label("Wir freuen uns auf Ihre Nachricht!",ContentMode.HTML);
        gl.addComponent(l, 0, 0, 2, 0);
        l.addStyleName(ValoTheme.LABEL_BOLD);
        vl.addComponent(new Label("&nbsp;", ContentMode.HTML));
        vl.addStyleName("scrollable");
        RichTextArea nachricht = new RichTextArea();
                vl.addComponent(nachricht);
                nachricht.setSizeFull();
                vl.addComponent(new Label("&nbsp;", ContentMode.HTML));
        Button b = new Button("Senden", VaadinIcons.ENVELOPE_O);
                vl.addComponent(b);
                vl.setComponentAlignment(b, Alignment.BOTTOM_RIGHT);
                b.addClickListener(clickEvent2 -> {
                    this.close();
                    Notification notif = new Notification("Die Nachricht wurde verschickt, vielen Dank!","", Notification.Type.WARNING_MESSAGE);
                    notif.setDelayMsec((int) TimeUnit.SECONDS.toMillis(2));
                    notif.show(Page.getCurrent());
                });

    }

}
