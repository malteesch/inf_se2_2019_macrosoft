package com.macrosoft.collhbrs.control.Proxy;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.DTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;

import java.time.LocalDate;
import java.util.List;

public interface SearchInterface {

    void search(String s) throws DatabaseException;

    List<VacancyDTO> getCompanyVacancies(CompanyDTO company) throws DatabaseException;

    List<CompanyDTO> filterCompanies(String searchTerm) throws DatabaseException;

    List<VacancyDTO> filterVacancies(String searchTerm) throws DatabaseException;

    List<VacancyDTO> filterResults(String location, String skill, LocalDate date) throws DatabaseException;

    List<DTO> liveFilterCombobox(String searchTerm) throws DatabaseException;
}
