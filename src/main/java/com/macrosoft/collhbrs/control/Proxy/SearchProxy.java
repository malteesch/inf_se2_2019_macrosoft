package com.macrosoft.collhbrs.control.Proxy;

import com.macrosoft.collhbrs.control.SearchControl;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.DTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;

import java.time.LocalDate;
import java.util.List;

public class SearchProxy implements SearchInterface {

    private SearchControl search;

    public SearchProxy(){
        search = new SearchControl();
    }

    @Override
    public void search(String s) throws DatabaseException {
        search.search(s);
    }

    @Override
    public List<VacancyDTO> getCompanyVacancies(CompanyDTO company) throws DatabaseException {
         return search.getCompanyVacancies(company);
    }

    @Override
    public List<CompanyDTO> filterCompanies(String searchTerm) throws DatabaseException {
        return search.filterCompanies(searchTerm);
    }

    @Override
    public List<VacancyDTO> filterVacancies(String searchTerm) throws DatabaseException {
        return search.filterVacancies(searchTerm);
    }

    @Override
    public List<VacancyDTO> filterResults(String location, String skill, LocalDate date) throws DatabaseException {
        return search.filterResults(location,skill,date);
    }

    @Override
    public List<DTO> liveFilterCombobox(String searchTerm) throws DatabaseException {
        return search.liveFilterCombobox(searchTerm);
    }
}
