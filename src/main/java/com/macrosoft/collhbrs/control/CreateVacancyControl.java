package com.macrosoft.collhbrs.control;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.model.daos.VacancyDAO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;

public class CreateVacancyControl {

    public static void publishVacancy(VacancyDTO dto) throws DatabaseException {
        new VacancyDAO().createOne(dto);
    }
}
