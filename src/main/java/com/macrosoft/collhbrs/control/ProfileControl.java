/**
 * @author Platzhalter 
 * @version 1.0 
 */
package com.macrosoft.collhbrs.control;

import com.macrosoft.collhbrs.gui.views.ProfileView;
import com.macrosoft.collhbrs.model.daos.CompanyDAO;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.shared.Position;
import com.vaadin.ui.UI;

public class ProfileControl {

    public static void showCompany() {
        UserDTO user = Session.getCurrentUser();
        if (user == null) {
            new Notifier().createErrorNotification("Der gewünschte User konnte nicht gefunden werden!")
                    .at(Position.TOP_CENTER)
                    .show();
        }
        try {
            if (ProfileView.getMyProfile()) {
                CompanyDAO dao = new CompanyDAO();
                CompanyDTO dto = dao.getOne(user.getUsername());
                ProfileView.setCompany(dto);
            }
            UI.getCurrent().getNavigator().navigateTo(Config.Views.PROFILE);
        } catch (Exception e) {
            new Notifier().createErrorNotification("Das gewünschte Unternehmen konnte nicht gefunden werden!")
                    .at(Position.TOP_CENTER)
                    .show();
            e.printStackTrace();
        }
    }

    public static void showStudent() {
        UserDTO user = Session.getCurrentUser();
        if (user == null) {
            new Notifier().createErrorNotification("Der gewünschte User konnte nicht gefunden werden!")
                    .at(Position.TOP_CENTER)
                    .show();
        }

        try {
            if (ProfileView.getMyProfile()) {
                StudentDAO dao = new StudentDAO();
                StudentDTO dto = dao.getOne(user.getEmail());
                ProfileView.setStudent(dto);
            }
            UI.getCurrent().getNavigator().navigateTo(Config.Views.PROFILE);
        } catch (Exception e) {
            new Notifier().createErrorNotification("Der gewünschte Student konnte nicht gefunden werden!")
                    .at(Position.TOP_CENTER)
                    .show();
            e.printStackTrace();
        }
    }
}