/**
 * @author Platzhalter
 * @version 1.0
 * <p>
 * Weist die Klasse dem Control-Package zu
 */

/**
 * Weist die Klasse dem Control-Package zu
 */
package com.macrosoft.collhbrs.control;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.InvalidCredentialsException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.model.daos.UserDAO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.ui.UI;

/**
 * Die Klasse regelt die Registrierungsfunktion der Webanwendung
 */
public class RegisterControl {

    /**
     * Die Methode dient zur Umsetzung der Registrierungsfunktion.
     * Hierbei kontrolliert sie die vom Benutzer gemachten Angaben auf Ihre
     * Einzigartigkeit(noch kein Konto auf diesen Namen)
     */
    public static void registerUser(UserDTO dto) throws UserException, DatabaseException {

        UserDAO dao = new UserDAO();
        UserDTO user;

        try {
            dao.getOne(dto.getUsername());
        }
        catch (InvalidCredentialsException e) {

            user = dao.createOne(dto);
            Session.setCurrentUser(user);

            UI.getCurrent().getNavigator().navigateTo(Config.Views.MAIN);
        }
        // throw new UserExistsException();
    }
}
