package com.macrosoft.collhbrs.control;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.daos.VacancyDAO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Session;
import com.macrosoft.collhbrs.services.utils.Utils;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ApplicationControl {

    public static ArrayList<VacancyDTO> getAllfavoredApplications() throws DatabaseException, UserException, SQLException {
        StudentDAO studentDAO = new StudentDAO();
        StudentDTO studentDTO = studentDAO.getOne(Objects.requireNonNull(Session.getCurrentUser()).getEmail());
        return getApplications("SELECT * FROM \"macrosoft_collHBRS\".vacancy_student WHERE studentid = " + studentDTO.getStudentid() + ";");

    }

    public static ArrayList<VacancyDTO> getApplications(String query) throws DatabaseException, SQLException {
        ArrayList<VacancyDTO> arr = new ArrayList<>();
        ResultSet set = Utils.freeDBCall(query);
        List<Integer> list = new ArrayList<>();
        while (set.next()) {
            list.add(set.getInt("vacancyid"));
        }
        VacancyDAO dao = new VacancyDAO();
        List<VacancyDTO> dtos = dao.getAll();
        for (VacancyDTO dto : dtos) {
            if (list.contains(dto.getVacancyId())) {
                arr.add(dto);
            }
        }
        return arr;
    }

    public static ArrayList<VacancyDTO> getAllApplications() throws DatabaseException, UserException, SQLException {
        StudentDAO studentDAO = new StudentDAO();
        StudentDTO studentDTO = studentDAO.getOne(Objects.requireNonNull(Session.getCurrentUser()).getEmail());
        return getApplications("SELECT * FROM \"macrosoft_collHBRS\".application_student WHERE studentid = " + studentDTO.getStudentid() + ";");
    }


    public static ArrayList<VacancyDTO> getVacanciesForCompany(int companyid) throws DatabaseException, SQLException {
        if (companyid <= 0) {
            return null;
        }
        ArrayList<VacancyDTO> arr = new ArrayList<>();
        VacancyDAO vacancyDAO = new VacancyDAO();
        List<VacancyDTO> list = vacancyDAO.getAll();
        for (VacancyDTO dto : list) {
            if (dto.getCompany().getCompanyId() == companyid) {
                arr.add(dto);
            }
        }
        return arr;
    }


}
