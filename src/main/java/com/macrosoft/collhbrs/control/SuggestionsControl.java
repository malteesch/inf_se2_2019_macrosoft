/**
 * @author Platzhalter
 * @version 1.0
 * <p>
 * Weist die Klasse dem Control-Package zu
 * <p>
 * Weist die Klasse dem Control-Package zu
 */

/**
 * Weist die Klasse dem Control-Package zu
 */

package com.macrosoft.collhbrs.control;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.model.daos.CompanyDAO;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.daos.VacancyDAO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.db.JDBCConnection;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Session;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Diese Klasse behandelt die Funktionalität der Zusammenstellung von Stellenvorschlägen an die Studenten
 */
public class SuggestionsControl {
    /**
     * Diese Methode erstellt eine Liste an Stellenvorschlägen für registrierte und angemeldete Studenten basierend
     * auf deren angegebenen Skills und Interessen. Hierbei gleicht das System alle vom Studenten gemachten Angaben dieser
     * beiden Kategorien mit den im System hinterlegten Stellen ab und fügt gegebene Treffer einer Liste hinzu, die dem
     * Studenten zurückgegeben wird
     */
    public static List<VacancyDTO> getVacancySuggestions() throws DatabaseException, UserException {
        UserDTO currentUser = Session.getCurrentUser();
        List<VacancyDTO> suggestions = new ArrayList<>();
        if (currentUser != null && Session.getCurrentRole().equals(Config.Role.STUDENT)) {
            StudentDTO student = new StudentDAO().getOne(currentUser.getUsername());
            List<String> skills = student.getSkills();
            List<String> interests = student.getInterests();
            new VacancyDAO().getAll().stream()
                    .filter(element -> element.getTags().stream().anyMatch(tag -> {
                        if (skills != null) {
                            for (String skill : skills) {
                                if (skill.toLowerCase().equals(tag.toLowerCase())) return true;
                            }
                        }
                        if (interests != null) {
                            for (String interest : interests) {
                                if (interest.toLowerCase().equals(tag.toLowerCase())) return true;
                            }
                        }
                        return false;
                    }))
                    .forEach(suggestions::add);
        }
        return suggestions;
    }

    public static List<StudentDTO> getStudentSuggestions() throws DatabaseException {
        UserDTO currentUser = Session.getCurrentUser();
        List<StudentDTO> suggestions = new ArrayList<>();
        if (currentUser != null && Session.getCurrentRole().equals(Config.Role.COMPANY)) {
            int companyId = new CompanyDAO().getOne(currentUser.getUsername()).getCompanyId();
            List<Integer> hiredStudentIds = new ArrayList<>();
            //language=PostgreSQL
            final String query = "SELECT DISTINCT studentid AS id\n" +
                    "FROM \"macrosoft_collHBRS\".application_student\n" +
                    "JOIN \"macrosoft_collHBRS\".vacancy ON application_student.vacancyid = vacancy.vacancyid\n" +
                    "JOIN \"macrosoft_collHBRS\".company ON vacancy.companyid = company.companyid\n" +
                    "WHERE company.companyid = ?\n" +
                    "AND hired = ?;";
            try {

                PreparedStatement pst = JDBCConnection.getInstance().getPreparedStatement(query);
                pst.setInt(1, companyId);
                pst.setBoolean(2, true);
                ResultSet result = pst.executeQuery();
                while (result.next()) {
                    hiredStudentIds.add(result.getInt("id"));
                }

                List<StudentDTO> allStudents = new StudentDAO().getAll();
                List<StudentDTO> hiredStudents = new ArrayList<>();
                allStudents.stream().filter(student -> {
                    for (Integer id : hiredStudentIds) {
                        if (student.getStudentid() == id) {
                            return true;
                        }
                    }
                    return false;
                }).forEach(hiredStudents::add);
                allStudents.stream().filter(student -> {

                    for (StudentDTO hiredStudent : hiredStudents) {
                        if (hiredStudent.getStudentid() == student.getStudentid())
                            return false;
                        if (student.compareTo(hiredStudent) == 0 && !hiredStudentIds.contains(student.getStudentid()))
                            return true;
                    }
                    return false;
                }).forEach(suggestions::add);
            } catch (SQLException e) {
                throw new DatabaseException("[" + SuggestionsControl.class.toString() + "] Error while getting hired students.");
            }
        }
        return suggestions;
    }
}
