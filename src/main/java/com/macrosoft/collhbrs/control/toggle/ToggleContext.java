package com.macrosoft.collhbrs.control.toggle;

import com.macrosoft.collhbrs.services.utils.Session;

class ToggleContext {
    static boolean hasRole(String role) {
        return Session.getCurrentRole().equals(role);
    }
}
