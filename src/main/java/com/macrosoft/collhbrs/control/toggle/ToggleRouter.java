package com.macrosoft.collhbrs.control.toggle;

import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Utils;

public class ToggleRouter {

    public static boolean featureIsEnabled(String feature) {
        if (!ToggleContext.hasRole(Config.Role.STUDENT) && !ToggleContext.hasRole(Config.Role.COMPANY)) return false;
        return Utils.getFeatureStatus(feature);
    }
}
