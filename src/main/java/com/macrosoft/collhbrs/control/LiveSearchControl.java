package com.macrosoft.collhbrs.control;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.Proxy.SearchInterface;
import com.macrosoft.collhbrs.control.Proxy.SearchProxy;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.gui.views.SearchView;
import com.macrosoft.collhbrs.model.daos.VacancyDAO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.DTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Notifier;
import com.vaadin.shared.Position;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.UI;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LiveSearchControl {

    private static SearchInterface searchControl = new SearchProxy();

    public static ComboBox<String> createLiveSearch() {
        ComboBox<String> searchbox = new ComboBox<>();
        searchbox.setEmptySelectionAllowed(false);
        searchbox.setPlaceholder("Bitte Suchbegriff eingeben...");
        searchbox.setWidth("100%");
        searchbox.addStyleName(MaterialTheme.COMBOBOX_CUSTOM);
        searchbox.setTextInputAllowed(true);
        searchbox.setEmptySelectionAllowed(false);

        List<DTO> filtered = null;
        try {
            filtered = searchControl.liveFilterCombobox("");
        } catch (DatabaseException e) {
            new Notifier().createErrorNotification("Die Suche konnte nicht durchgeführt werden. Bitte kontaktieren Sie den Administrator.")
                    .at(Position.TOP_CENTER)
                    .show();
            e.printStackTrace();
        }
        List<String> titles = new ArrayList<>();
        if (filtered != null) {
            for (DTO dto : filtered) {
                if (dto instanceof VacancyDTO) {
                    VacancyDTO v = (VacancyDTO) dto;
                    titles.add(v.getTitle());
                } else if (dto instanceof CompanyDTO) {
                    CompanyDTO c = (CompanyDTO) dto;
                    titles.add(c.getName());
                }
            }
        }

        searchbox.setItems(titles);
        searchbox.setNewItemProvider(input -> {
            comboboxSearch(input);
            return Optional.empty();
        });

        searchbox.addValueChangeListener(e -> comboboxSearch(searchbox.getValue()));
        return searchbox;
    }

    public static void comboboxSearch(String searchTerm) {
        try {
            if (!searchTerm.isEmpty()) {
                searchControl.search(searchTerm);
            } else {
                SearchView.setCurrentResults(new VacancyDAO().getAll());
                SearchView.setHeaderCompany(new CompanyDTO());
                UI.getCurrent().getNavigator().navigateTo(Config.Views.SEARCH);
            }
        } catch (DatabaseException e) {
            new Notifier().createErrorNotification("Die Suche konnte nicht durchgeführt werden. Bitte kontaktieren Sie den Administrator.")
                    .at(Position.TOP_CENTER)
                    .show();
            e.printStackTrace();
        }
    }
}
