package com.macrosoft.collhbrs.control.exceptions;

public class UserExistsException extends UserException {
    public UserExistsException() {
        super("User already exists. Please log in.");
    }
}
