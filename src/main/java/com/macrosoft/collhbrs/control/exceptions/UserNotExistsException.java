package com.macrosoft.collhbrs.control.exceptions;

public class UserNotExistsException extends UserException {

    public UserNotExistsException() {
        super("User does not exist. Please register first!");
    }
}
