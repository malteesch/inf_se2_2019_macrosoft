package com.macrosoft.collhbrs.control.exceptions;

public class InvalidCredentialsException extends UserException {

    public InvalidCredentialsException() {
        super("Ihre Login Daten stimmen sind nicht korrekt. Bitte versuchen Sie es erneut.");
    }
}
