
package com.macrosoft.collhbrs.control;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.gui.views.VacancyDetailsView;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Session;
import com.macrosoft.collhbrs.services.utils.Utils;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class VacancyDetailsControl {

    public static void showVacancy(VacancyDTO vDTO) throws Exception {
        VacancyDetailsView.setCurrentVacancy(vDTO);
        UI.getCurrent().getNavigator().navigateTo(Config.Views.VACANCY_DETAILS);
    }


    public static boolean addToFavorites(int id, int studentid) throws DatabaseException, SQLException {
        if (isFavorite(id, studentid)) {
            return false;
        } else {
            //language=PostgreSQL
            String sql = "INSERT INTO \"macrosoft_collHBRS\".vacancy_student (studentid, vacancyid) values (" + studentid + ", " + id + ") RETURNING *;";
            Utils.freeDBCall(sql);
            return true;
        }
    }


    public static boolean removeFromFavorites(int vacancyId, int studentid) throws DatabaseException, SQLException {
        if (isFavorite(vacancyId, studentid)) {
            //language=PostgreSQL
            String sql = "DELETE FROM \"macrosoft_collHBRS\".vacancy_student WHERE studentid = " + studentid + " and vacancyid = " + vacancyId + " RETURNING *;";
            Utils.freeDBCall(sql);
            return true;
        }
        return false;
    }

    public static boolean isFavorite(int vacancyId, int studentid) throws DatabaseException, SQLException {
        if (vacancyId > 0 && studentid > 0) {
            //language=PostgreSQL
            String sql = "SELECT * FROM \"macrosoft_collHBRS\".vacancy_student " +
                    "WHERE studentid = " + studentid + " AND vacancyid =  " + vacancyId;
            ResultSet set = Utils.freeDBCall(sql);
            if (set.next()) {
                return true;
            }
        }
        return false;

    }

    public static Button createFavButton(VacancyDTO dto) throws DatabaseException, UserException, SQLException {
        StudentDAO dao = new StudentDAO();
        StudentDTO studentDTO = dao.getOne(Objects.requireNonNull(Session.getCurrentUser()).getEmail());
        // true, wenn es geklappt hat, false, wenn schon favorit war
        Button fav = new Button();
        boolean x = VacancyDetailsControl.isFavorite(dto.getVacancyId(), studentDTO.getStudentid());
        if (x) {
            fav.setIcon(VaadinIcons.HEART);

        } else {
            fav.setIcon(VaadinIcons.HEART_O);

        }
        fav.addStyleName(MaterialTheme.BUTTON_BORDERLESS);
        fav.addClickListener((Button.ClickListener) event -> {
            try {
                if (fav.getIcon() == VaadinIcons.HEART_O) {
                    addToFavorites(dto.getVacancyId(), studentDTO.getStudentid());
                    fav.setIcon(VaadinIcons.HEART);
                } else {
                    VacancyDetailsControl.removeFromFavorites(dto.getVacancyId(), studentDTO.getStudentid());
                    fav.setIcon(VaadinIcons.HEART_O);
                }
            } catch (DatabaseException | SQLException e) {
                e.printStackTrace();
            }

        });
        return fav;
    }

    public static boolean createOneClickApplication(int vacancyId, int studentId) throws DatabaseException, SQLException {

        if (vacancyId > 0 && studentId > 0 && !alreadyApplied(vacancyId, studentId)) {

            String query = "INSERT INTO \"macrosoft_collHBRS\".application_student (studentid, vacancyid, applicationdate) VALUES (" + studentId + ", " + vacancyId + ", NOW()) RETURNING *;";
            ResultSet set = Utils.freeDBCall(query);
            return set.next();
        } else {
            return false;
        }
    }

    public static boolean alreadyApplied(int vacancyId, int studentId) throws DatabaseException, SQLException {
        if (vacancyId > 0 && studentId > 0) {
            String query = "SELECT * FROM \"macrosoft_collHBRS\".application_student WHERE studentid = " + studentId + " and vacancyid = " + vacancyId + ";";
            ResultSet set = Utils.freeDBCall(query);
            return set.next();
        } else {
            return false;
        }
    }
}
