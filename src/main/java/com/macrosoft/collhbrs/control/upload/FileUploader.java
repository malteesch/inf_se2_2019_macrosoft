package com.macrosoft.collhbrs.control.upload;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.gui.windows.EditStudentWindow;
import com.macrosoft.collhbrs.model.daos.DocumentDAO;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.daos.UserDAO;
import com.macrosoft.collhbrs.model.dtos.DocumentDTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Session;
import com.macrosoft.collhbrs.services.utils.Utils;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload;

import java.io.*;
import java.util.Objects;

import static com.macrosoft.collhbrs.services.utils.Utils.toByteArray;

public class FileUploader implements Upload.Receiver, Upload.SucceededListener {

    public File file;
    private String mimeType;
    private String filename;

    @Override
    public OutputStream receiveUpload(String filename, String mimeType) {

        try {
            this.file = File.createTempFile("tmp-", "." + mimeType.split("/")[1]);
            this.mimeType = mimeType;
            this.filename = filename;

            return new FileOutputStream(file);
        } catch (IOException e) {
            e.printStackTrace(); //TODO
        }
        return null;
    }

    @Override
    public void uploadSucceeded(Upload.SucceededEvent event) {

        Notification notify = new Notification("Information");
        FileInputStream fis = null;

        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace(); //TODO
        }

        if(this.mimeType.contains("pdf")) {
            try {
                    DocumentDTO doc = new DocumentDTO();
                    assert fis != null;
                    doc.setFile(toByteArray(fis));
                    doc.setUserid(Objects.requireNonNull(Session.getCurrentUser()).getUserid());
                    doc.setDocumentdescription("leer");
                    doc.setVisibility(true);
                    doc.setDocumenttitle(filename); // Todo: File extension aus filename entfernen

                    DocumentDAO doc_dao = new DocumentDAO();
                    doc_dao.createOne(doc);

                    notify.setDescription("Lebenslauf hochgeladen. Bitte speichern nicht vergessen.");

            } catch (Exception e) {
                e.printStackTrace(); // Todo
            }

        }else if(this.mimeType.contains("image")){
            try {
                assert fis != null;
                Objects.requireNonNull(Session.getCurrentUser()).setImage(toByteArray(fis));
                notify.setDescription("Profilbild hochgeladen. Bitte speichern nicht vergessen.");

                if (Session.getCurrentRole().equals(Config.Role.STUDENT)) {
                    EditStudentWindow.refreshProfilePic(Utils.convertToImage(Session.getCurrentUser().getImage()));
                }

            } catch (IOException e) {
                e.printStackTrace(); //TODO
                notify.setDescription("Fehler");
            }
        }else{
            notify.setDescription("Dieser Datentyp wird nicht akzeptiert.");
        }

        notify.setDelayMsec(20000);
        notify.setPosition(Position.BOTTOM_RIGHT);
        notify.show(Page.getCurrent());
    }

}