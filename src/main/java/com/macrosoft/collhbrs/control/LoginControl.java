/**
 * @author Platzhalter
 * @version 1.0
 * <p>
 * Weist die Klasse dem Control-Package zu
 */

/**
 * Weist die Klasse dem Control-Package zu
 */
package com.macrosoft.collhbrs.control;

import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.InvalidCredentialsException;
import com.macrosoft.collhbrs.control.exceptions.UserException;
import com.macrosoft.collhbrs.model.daos.CompanyDAO;
import com.macrosoft.collhbrs.model.daos.StudentDAO;
import com.macrosoft.collhbrs.model.daos.UserDAO;
import com.macrosoft.collhbrs.model.dtos.UserDTO;
import com.macrosoft.collhbrs.model.dtos.UserLoginDTO;
import com.macrosoft.collhbrs.services.utils.BCryptUtil;
import com.macrosoft.collhbrs.services.utils.Config;
import com.macrosoft.collhbrs.services.utils.Session;
import com.vaadin.ui.UI;

/**
 * Diese Control-Klasse ermöglicht die LogIn-Funktion und die LogOut-Funktion der Webanwendung
 */
public class LoginControl {

    /**
     * Die Methode checkAuthentication regelt die LogIn-Funktion der Webanwendung, indem sie die vom Benutzer in die LogIn-UI eingegebenen Daten mit den bereits in der Datenbank 
     * abgespeicherten Datensätzen abgleicht, sowie kontrolliert ob ggf. durch einen Fehler keine Daten in die Maske eingetragen wurden
     */
    public static void checkAuthentication(UserLoginDTO dto) throws InvalidCredentialsException, DatabaseException {
        UserDTO user = new UserDAO().getOne(dto.getLogin());
        if (BCryptUtil.check(dto.getPassword(), user.getPassword())) {
            Session.setCurrentUser(user);
            Session.setCurrentRole(checkRole(dto));
            UI.getCurrent().getNavigator().navigateTo(Config.Views.MAIN);
        } else {
            throw new InvalidCredentialsException();
        }
    }

    /**
     * Die Methode regelt die LogOut-Funktion der Webanwendung
     */
    public static void logoutUser() {
        Session.quit();
        UI.getCurrent().getPage().setLocation("/collhbrs-1.0-SNAPSHOT");
    }

    static String checkRole(UserLoginDTO dto) throws DatabaseException {
        try {
            new StudentDAO().getOne(dto.getLogin());
            return Config.Role.STUDENT;
        } catch (DatabaseException | UserException e) {
            try {
                new CompanyDAO().getOne(dto.getLogin());
                return Config.Role.COMPANY;
            } catch (DatabaseException f) {
                f.printStackTrace(); //TODO
            }
        }
        throw new DatabaseException("[" + LoginControl.class.toString() + "] error checkRole lauft nicht");
    }
}
