/**
 * @author Platzhalter
 * @version 1.0
 */

/**
 * Weist die Klasse der Control-Package zu
 */
package com.macrosoft.collhbrs.control;

import com.macrosoft.collhbrs.control.Proxy.SearchInterface;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.gui.views.SearchView;
import com.macrosoft.collhbrs.model.daos.CompanyDAO;
import com.macrosoft.collhbrs.model.daos.VacancyDAO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.DTO;
import com.macrosoft.collhbrs.model.dtos.VacancyDTO;
import com.macrosoft.collhbrs.services.utils.Config;
import com.vaadin.ui.UI;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


/**
 * Die Klasse dient zur Navigation zur Search-Funktion der Webanwendung(noch :D)
 */
public class SearchControl implements SearchInterface {

    /**
     * Die Methode öffnet die Search-UI für den Nutzer
     */
    public void search(String s) throws DatabaseException {

        List<CompanyDTO> filteredCompanies = filterCompanies(s);
        List<VacancyDTO> filteredVacancies;

        if (filteredCompanies.size() >= 1) {
            filteredVacancies = getCompanyVacancies(filteredCompanies.get(0));
            SearchView.setHeaderCompany(filteredCompanies.get(0));
        } else {
            filteredVacancies = filterVacancies(s);
            SearchView.setHeaderCompany(new CompanyDTO());
        }

        SearchView.setCurrentResults(filteredVacancies);
        SearchView.setAllVacancies(false);
        UI.getCurrent().getNavigator().navigateTo(Config.Views.SEARCH);
    }

    public List<VacancyDTO> getCompanyVacancies(CompanyDTO company) throws DatabaseException {
        List<VacancyDTO> vacancies = new VacancyDAO().getAll();
        List<VacancyDTO> filtered = new ArrayList<>();
        vacancies.stream()
                .filter(vacancy -> vacancy.getCompany().getName().equals(company.getName()))
                .forEach(filtered::add);
        return filtered;
    }

    public List<CompanyDTO> filterCompanies(String searchTerm) throws DatabaseException {
        List<CompanyDTO> companies = new CompanyDAO().getAll();
        List<CompanyDTO> filtered = new ArrayList<>();
        companies.stream()
                .filter(company -> company.getName().toLowerCase().contains(searchTerm.toLowerCase()))
                .forEach(filtered::add);
        return filtered;
    }

    public List<VacancyDTO> filterVacancies(String searchTerm) throws DatabaseException {
        List<VacancyDTO> vacancies = new VacancyDAO().getAll();
        List<VacancyDTO> filtered = new ArrayList<>();
        vacancies.stream()
                .filter(vacancy -> vacancy.getTitle().toLowerCase().contains(searchTerm.toLowerCase()))
                .forEach(filtered::add);
        return filtered;
    }

    public List<DTO> liveFilterCombobox(String searchTerm) throws DatabaseException {
        List<VacancyDTO> vacancies = new VacancyDAO().getAll();
        List<CompanyDTO> companies = new CompanyDAO().getAll();
        List<DTO> filtered = new ArrayList<>();
        vacancies.stream()
                .filter(vacancy -> vacancy.getTitle().toLowerCase().contains(searchTerm.toLowerCase()))
                .forEach(filtered::add);

        companies.stream()
                .filter(company -> company.getName().toLowerCase().contains(searchTerm.toLowerCase()))
                .forEach(filtered::add);
        return filtered;
    }

    public List<VacancyDTO> filterResults(String location, String skill, LocalDate date) {
        List<VacancyDTO> vacancies = SearchView.getCurrentResults();
        List<VacancyDTO> filtered = new ArrayList<>();

        vacancies.stream()
                .filter(vacancy -> {
                    if (location != null) {
                        return vacancy.getCompany().getAddress().getCity().toLowerCase().contains(location.toLowerCase());
                    }
                    return true;
                })
                .filter(vacancy -> vacancy.getTags().stream().anyMatch(tag -> {
                    if (skill != null) {
                        return tag.toLowerCase().contains(skill.toLowerCase());
                    }
                    return true;
                }))
                .filter(vacancy -> {
                    if (date != null) {
                        return vacancy.getDate().compareTo(date) > 0;
                    }
                    return true;
                })
                .forEach(filtered::add);
        return filtered;
    }
}
