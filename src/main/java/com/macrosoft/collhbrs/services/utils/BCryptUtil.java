package com.macrosoft.collhbrs.services.utils;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class BCryptUtil {

    private static String salt = BCrypt.gensalt(Config.BCrypt.SALT_STRENGTH);

    public static String hash(String plaintext) {
        return BCrypt.hashpw(plaintext, salt);
    }

    public static boolean check(String plaintext, String hash) {
        return BCrypt.checkpw(plaintext, hash);
    }
}
