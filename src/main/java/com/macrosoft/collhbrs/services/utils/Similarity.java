package com.macrosoft.collhbrs.services.utils;

import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Similarity implements Comparator<StudentDTO> {

    private static Similarity instance = new Similarity();
    private DefaultUndirectedWeightedGraph<String, DefaultWeightedEdge> graph = new DefaultUndirectedWeightedGraph<>(DefaultWeightedEdge.class);

    private Similarity() {
        populateGraph();
    }

    public static synchronized Similarity getInstance() {
        if (instance == null)
            instance = new Similarity();
        return instance;
    }

    private void populateGraph() {
        // languages
        graph.addVertex("java");
        graph.addVertex("php");
        graph.addVertex("kotlin");
        graph.addVertex("scala");
        graph.addVertex("javascript");
        graph.addVertex("typescript");
        graph.addVertex("python");
        graph.addVertex("html");
        graph.addVertex("css");
        graph.addVertex("scss");
        graph.addVertex("swift");
        graph.addVertex("objective-c");
        graph.addVertex("c++");
        graph.addVertex("c#");
        graph.addVertex("mysql");
        graph.addVertex("postgresql");
        graph.addVertex("sql");

        // java
        DefaultWeightedEdge java_php = graph.addEdge("java", "php");
        graph.setEdgeWeight(java_php, 4.0);
        DefaultWeightedEdge java_kotlin = graph.addEdge("java", "kotlin");
        graph.setEdgeWeight(java_kotlin, 2.0);
        DefaultWeightedEdge java_scala = graph.addEdge("java", "scala");
        graph.setEdgeWeight(java_scala, 2.0);
        DefaultWeightedEdge java_javascript = graph.addEdge("java", "javascript");
        graph.setEdgeWeight(java_javascript, 6.0);
        DefaultWeightedEdge java_typescript = graph.addEdge("java", "typescript");
        graph.setEdgeWeight(java_typescript, 5.0);
        DefaultWeightedEdge java_python = graph.addEdge("java", "python");
        graph.setEdgeWeight(java_python, 6.0);
        DefaultWeightedEdge java_html = graph.addEdge("java", "html");
        graph.setEdgeWeight(java_html, 10.0);
        DefaultWeightedEdge java_css = graph.addEdge("java", "css");
        graph.setEdgeWeight(java_css, 10.0);
        DefaultWeightedEdge java_scss = graph.addEdge("java", "scss");
        graph.setEdgeWeight(java_scss, 10.0);
        DefaultWeightedEdge java_swift = graph.addEdge("java", "swift");
        graph.setEdgeWeight(java_swift, 7.0);
        DefaultWeightedEdge java_objectivec = graph.addEdge("java", "objective-c");
        graph.setEdgeWeight(java_objectivec, 6.0);
        DefaultWeightedEdge java_cpp = graph.addEdge("java", "c++");
        graph.setEdgeWeight(java_cpp, 5.0);
        DefaultWeightedEdge java_csharp = graph.addEdge("java", "c#");
        graph.setEdgeWeight(java_csharp, 1.0);
        DefaultWeightedEdge java_mysql = graph.addEdge("java", "mysql");
        graph.setEdgeWeight(java_mysql, 10.0);
        DefaultWeightedEdge java_postgresql = graph.addEdge("java", "postgresql");
        graph.setEdgeWeight(java_postgresql, 10.0);
        DefaultWeightedEdge java_sql = graph.addEdge("java", "sql");
        graph.setEdgeWeight(java_sql, 10.0);

        // php
        DefaultWeightedEdge php_kotlin = graph.addEdge("php", "kotlin");
        graph.setEdgeWeight(php_kotlin, 5.0);
        DefaultWeightedEdge php_html = graph.addEdge("php", "html");
        graph.setEdgeWeight(php_html, 3.0);


        // javascript
        DefaultWeightedEdge javascript_html = graph.addEdge("javascript", "html");
        graph.setEdgeWeight(javascript_html, 3.0);

    }

    private double getShortestDistance(String skill1, String skill2) {
        return new DijkstraShortestPath<>(graph).getPathWeight(skill1, skill2);
    }

    @Override
    public int compare(StudentDTO student1, StudentDTO student2) {
        double shortestDistance = Double.MAX_VALUE;
        List<String> skills1 = student1.getSkills() != null ? student1.getSkills() : new ArrayList<>();
        List<String> skills2 = student2.getSkills() != null ? student2.getSkills() : new ArrayList<>();
        for (String skill1 : skills1) {
            for (String skill2 : skills2) {
                if (graph.containsVertex(skill1.toLowerCase()) && graph.containsVertex(skill2.toLowerCase())) {
                    shortestDistance = Math.min(shortestDistance, getShortestDistance(skill1.toLowerCase(), skill2.toLowerCase()));
                }
            }
        }
        return shortestDistance <= Config.SIMILARITY_DISTANCE ? 0 : -1;
    }
}
