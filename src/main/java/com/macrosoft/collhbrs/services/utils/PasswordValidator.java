package com.macrosoft.collhbrs.services.utils;

import com.vaadin.data.ValidationResult;
import com.vaadin.data.ValueContext;
import com.vaadin.data.validator.StringLengthValidator;

public class PasswordValidator extends StringLengthValidator {

    public PasswordValidator() {
        super("", 6, 50);
    }

    @Override
    public ValidationResult apply(String value, ValueContext context) {
        ValidationResult result = super.apply(value, context);
        if (result.isError()) {
            return ValidationResult
                    .error("Das Passwort sollte mindestens 6 Zeichen lang sein.");
        } else if (!hasDigit(value) || !hasLetter(value)) {
            return ValidationResult
                    .error("Das Passwort sollte mindestens einen Buchstaben und eine Zahl enthalten.");
        }
        return result;
    }

    private boolean hasDigit(String pwd) {
        return pwd.chars().anyMatch(Character::isDigit);
    }

    private boolean hasLetter(String pwd) {
        return pwd.chars().anyMatch(Character::isLetter);
    }
}
