package com.macrosoft.collhbrs.services.utils;

public class Config {

    public static final double SIMILARITY_DISTANCE = 9;

    public static class Views {
        public static final String MAIN = "main";
        public static final String LOGIN = "login";
        public static final String REGISTER = "register";
        public static final String SEARCH = "search";
        public static final String PROFILE = "profile";
        public static final String CREATE_VACANCY = "createVacancy";
        public static final String VACANCY_DETAILS = "vacancyDetails";
        public static final String APPLICATION_VIEW = "applications";
        public static final String HELP_VIEW = "help";

    }

    static class BCrypt {
        static final int SALT_STRENGTH = 12;
    }

    public static class DB {
        public static final String DB_USER = "ttomcz2s";
        public static final String DB_PASSWORD = "ttomcz2s";
        public static final String DB_CONNECTION_STRING = "jdbc:postgresql://dumbo.inf.h-brs.de/ttomcz2s";
    }

    public static class Role {
        public static final String STUDENT = "student";
        public static final String COMPANY = "company";
    }

    public static class Features {
        // constants have to match a name in the feature table in the database
        public static final String APPLY = "applyForVacancy";
        public static final String VACANCY_SUGGESTIONS = "suggestVacancies";
        public static final String STUDENT_SUGGESTIONS = "suggestStudents";
    }
}
