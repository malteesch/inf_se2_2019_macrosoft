package com.macrosoft.collhbrs.services.utils;

import com.github.appreciated.material.MaterialTheme;
import com.macrosoft.collhbrs.control.exceptions.DatabaseException;
import com.macrosoft.collhbrs.control.exceptions.NotRatedException;
import com.macrosoft.collhbrs.model.dtos.AddressDTO;
import com.macrosoft.collhbrs.model.dtos.CompanyDTO;
import com.macrosoft.collhbrs.model.dtos.DTO;
import com.macrosoft.collhbrs.model.dtos.StudentDTO;
import com.macrosoft.collhbrs.services.db.JDBCConnection;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import org.json.JSONArray;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Utils {

    public static <T> ArrayList<T> transform(T[] arr) {
        if (arr.length > 0) {

            return new ArrayList<>(Arrays.asList(arr));
        }
        return new ArrayList<>();
    }

    @SuppressWarnings("unchecked")
    public static <T> ArrayList<T> transform(Array arr) throws SQLException {
        T[] tmp = (T[]) arr.getArray();
        return transform(tmp);
    }

    public static Image convertToImage(final byte[] imageData) {
        StreamResource.StreamSource streamSource = (StreamResource.StreamSource) () -> (imageData == null) ? null : new ByteArrayInputStream(
                imageData);
        return new Image(null, new StreamResource(streamSource, "streamedSourceFromByteArray"));
    }

    public static String convertDateToString(LocalDate date) {
        return dateToString(date);
    }

    private static String dateToString(Object date) {
        if (date == null) {
            return "";
        }
        String[] arr = date.toString().split("-");
        return arr[2] + "." + arr[1] + "." + arr[0];
    }

    public static StreamResource convertByteArrayToPdf(byte[] bArray, String title) {
        StreamResource.StreamSource streamSource = (StreamResource.StreamSource) () -> (bArray == null) ? null : new ByteArrayInputStream(
                bArray);
        return new StreamResource(streamSource, title + ".pdf");
    }

    public static byte[] toByteArray(FileInputStream fis) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = fis.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        return bos.toByteArray();
    }

    public static ResultSet freeDBCall(String query) throws SQLException, DatabaseException {
        JDBCConnection jdbc = JDBCConnection.getInstance();
        PreparedStatement pst = jdbc.getPreparedStatement(query);
        return pst.executeQuery();
    }

    public static boolean getFeatureStatus(String feature) {
        //language=PostgreSQL
        final String query = "SELECT status\n" +
                "FROM \"macrosoft_collHBRS\".feature\n" +
                "WHERE name = ?;";
        try {
            JDBCConnection conn = JDBCConnection.getInstance();
            PreparedStatement pst = conn.getPreparedStatement(query);
            pst.setString(1, feature);
            ResultSet result = pst.executeQuery();
            if (result.next()) {
                return result.getBoolean("status");
            } else return false;
        } catch (DatabaseException | SQLException e) {
            return false;
        }
    }

    public static CompanyDTO setRating(CompanyDTO dto)throws NotRatedException {
        //language=PostgreSQL
        String query = "SELECT avg(rating) FROM \"macrosoft_collHBRS\".\"student_company_rating\" WHERE Company = " + dto.getCompanyId() + ";";
        ResultSet res;
        try {
            JDBCConnection conn = JDBCConnection.getInstance();
            Statement st = conn.getStatement();
            res = st.executeQuery(query);

            res.next();
            double rating = res.getDouble(1);
            if(res.wasNull()){
                throw new NotRatedException();
            }
            dto.setRating(rating);
            return dto;

        }
            catch (DatabaseException | SQLException e) {
                e.printStackTrace();
                return null;
            }

    }
    public static void rate(int i,CompanyDTO dto){
        int user = Session.getCurrentUser().getUserid();
        int company = dto.getCompanyId();
        //language=PostgreSQL
        String query1 = "select 1 from \"macrosoft_collHBRS\".student_company_rating WHERE userid="+ user +" AND company = " + company;
        String query2 = "UPDATE \"macrosoft_collHBRS\".student_company_rating SET rating = " + i + " WHERE userid = " + user + " AND company = " + company;
        String query3 = "INSERT INTO \"macrosoft_collHBRS\".student_company_rating VALUES (" + user + ", " + company + ", " + i + ") Returning *";
        try {
            JDBCConnection conn = JDBCConnection.getInstance();
            Statement pst = conn.getStatement();
            pst.execute(query1);

            ResultSet set =  pst.getResultSet();
            if(set.next()){
                Statement pst2 = conn.getStatement();
                pst2.execute(query2);
            }
            else{
                Statement pst3 = conn.getStatement();
                pst3.executeQuery(query3);
            }

        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static <T extends DTO> JSONArray listToJSONArray(List<T> list) {
        JSONArray arr = new JSONArray();
        if (list != null)
            list.forEach(element -> arr.put(element.toJSON()));
        return arr;
    }

    public static JSONArray stringListToJSONArray(List<String> list) {
        JSONArray arr = new JSONArray();
        list.forEach(arr::put);
        return arr;
    }

    public static HorizontalLayout createNoResultsWarning(String s) {
        HorizontalLayout noResultsLayout = new HorizontalLayout();
        noResultsLayout.setWidth("100%");
        noResultsLayout.setHeight("147px");
        noResultsLayout.addStyleName("search-components");

        Panel noResultsPanel = new Panel();
        noResultsPanel.setWidth("80%");
        noResultsPanel.setHeight("107px");
        noResultsLayout.addComponent(noResultsPanel);
        noResultsLayout.setComponentAlignment(noResultsPanel, Alignment.MIDDLE_CENTER);
        HorizontalLayout panelContent = new HorizontalLayout();
        panelContent.addStyleName("noresults-layout");
        panelContent.setSizeFull();
        Label noResultsLabel = new Label(VaadinIcons.INFO_CIRCLE_O.getHtml() + s, ContentMode.HTML);
        noResultsLabel.setWidth("100%");
        noResultsLabel.addStyleName(MaterialTheme.LABEL_BOLD);
        panelContent.addComponent(noResultsLabel);
        panelContent.setComponentAlignment(noResultsLabel, Alignment.MIDDLE_LEFT);
        noResultsPanel.setContent(panelContent);
        return noResultsLayout;

    }

    public static int getNumberApplicationToVacancy(int i) throws DatabaseException {
        JDBCConnection connection = JDBCConnection.getInstance();
        String countQuery = "SELECT COUNT(*) FROM \"macrosoft_collHBRS\".application_student WHERE vacancyid = '" + i + "';";
        int result = 0;
        try {
            PreparedStatement pst = connection.getPreparedStatement(countQuery);
            ResultSet set = pst.executeQuery();
            if (set.next()) {
                result = set.getInt(1);
            }
        } catch (SQLException e) {
            throw new DatabaseException();
        } finally {
            connection.closeConnection();
        }
        return result;
    }

    public static ArrayList<StudentDTO> getStudentsApplicationToVacancy(int i) throws DatabaseException, SQLException {

        JDBCConnection connection = JDBCConnection.getInstance();
        //language=PostgreSQL
        String query = "SELECT * FROM \"macrosoft_collHBRS\".application_student, \"macrosoft_collHBRS\".student, \"macrosoft_collHBRS\".\"user\", \"macrosoft_collHBRS\".address\n" +
                "WHERE application_student.studentid = student.studentid\n" +
                "AND student.userid = \"user\".userid\n" +
                "AND \"user\".address = address.addressid\n" +
                "AND vacancyid = '" + i + "';";

        PreparedStatement pst = connection.getPreparedStatement(query);
        ResultSet set = pst.executeQuery();
        ArrayList<StudentDTO> list = new ArrayList<>();

        while (set.next()) {
            StudentDTO dto = new StudentDTO();
            AddressDTO aDto = new AddressDTO();
            dto.setImage(set.getBytes("photo"));
            dto.setName(set.getString("name"));
            dto.setFamilyname(set.getString("familyname"));
            dto.setMajor(set.getString("major"));
            dto.setSpecalization(set.getString("schwerpunkt"));
            dto.setBirthdate(new java.sql.Date(set.getDate("birthdaydate").getTime()).toLocalDate());
            dto.setEmail(set.getString("email"));
            aDto.setStreet(set.getString("street"));
            aDto.setHousenumber(set.getString("housenumber"));
            aDto.setCity(set.getString("city"));
            aDto.setPostalcode(set.getString("postalcode"));
            aDto.setCountry(set.getString("country"));
            dto.setAddress(aDto);
            list.add(dto);
        }
        return list;
    }
}
