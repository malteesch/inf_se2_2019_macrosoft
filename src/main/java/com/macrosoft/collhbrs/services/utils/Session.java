package com.macrosoft.collhbrs.services.utils;

import com.macrosoft.collhbrs.model.dtos.UserDTO;
import com.vaadin.server.VaadinSession;

public class Session {

    public static class Attributes {
        static final String CURRENT_USER = "currentUser";
        static final String ROLE = "role";
    }

    public static boolean isLoggedIn() {
        return VaadinSession.getCurrent().getAttribute(Attributes.CURRENT_USER) != null;
    }

    public static void setCurrentUser(UserDTO user) {
        VaadinSession.getCurrent().setAttribute(Attributes.CURRENT_USER, user);
    }

    public static UserDTO getCurrentUser() {
        if (!isLoggedIn()) {
            return null;
        }
        return (UserDTO) VaadinSession.getCurrent().getAttribute(Attributes.CURRENT_USER);
    }

    public static String getCurrentRole() {
        return (String) VaadinSession.getCurrent().getAttribute(Attributes.ROLE);
    }

    public static void setCurrentRole(String role) {
        VaadinSession.getCurrent().setAttribute(Attributes.ROLE, role);
    }

    public static void quit() {
        VaadinSession.getCurrent().close();
    }
}
